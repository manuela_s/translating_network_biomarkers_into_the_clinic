# A machine learning platform to optimize the translation of personalized 
# network models to the clinic
---------------------------------------------------------------------------

Datasets and source-code underlying the computational analyses included in
the manuscript "*A machine learning platform to optimize the translation of
personalized network models to the clinic*" submitted to *JCO Clinical Cancer
Informatics* and authored by Manuela Salvucci, Arman Rahman, Alexa Resler,
Girish Mallya Udupi, Deborah A. McNamara, Elaine W. Kay, Pierre Laurent-Puig,
Daniel B. Longley, Patrick G. Johnston, Mark Lawler, Richard Wilson, Manuel
Salto-Tellez, Sandra Van Schaeybroeck, Mairin Rafferty, William Gallagher, 
Markus Rehm, and Jochen H. M. Prehn.


## Requirements

- MATLAB (release 2014b) with:
    - Parallel Computing toolbox;
    - Statistics toolbox;
- Graphviz (version 3.28);
- R (release 3.4.3) with:
    - optparse package (version 1.4.4);
    - data.table package (version 1.10.4-3);
    - sva package (version 3.26.0) from Bioconductor (version 3.6, BiocInstaller 1.28.0);
    - survival package (2.41-3);
    - car package (2.1-5).

Graphviz and R binary Rscript must be listed in PATH.


## Directory structure

- **data:** clinical and proteomic datasets;
- **+cohort_utilities:** utility package to setup, run and post-process
    simulations for the APOPTO-CELL model;
- **+matlab_utilities:** utility package to handle and plot data. Additionally,
    the package includes third-party source code retrieved from [MATLAB file exchange](https://uk.mathworks.com/matlabcentral/fileexchange/)
    with source and licencing information detailed in the corresponding source.md
    and licence.txt files;
- **+decision_tree:** utility package to build classification decision trees
    with custom cost functions for minimal number of protein inputs;
- **+get:** utility package to get input datasets;
- **+run:** utility package to run APOPTO-CELL simulations, build classification
    decision trees and perform statistical analyses;
- **+make:** utility package to make figures and tables.

Detailed information are included within each file:

- doc string for each .m and .R file;
- code-book for each .csv file.


## Usage

From MATLAB, run the following functions:

1. `GenerateInputsFile()` to generate the .mat file with the inputs required
   by the subsequent computational analyses (stored in the folder **outputs**);
2. `RunSimulations()` to generate the .mat files with simulation results for the
   APOPTO-CELL and corresponding minimal models (stored in the folder **outputs**);
3. `ComputeCIndicesStats()` to compute the *c*-index metric from univariate Cox
   regression hazards models for disease-free survival and overall survival
   where patients were categorized by APOPTO-CELL derived signatures (stored
   in the folder **outputs**);
4. `run.TreePredictions()` to generate a .mat file with the predictions from
   classification decision trees (stored in the folder **outputs**);
5. `MakeFiguresAndTables()` to generate figures, supplemental figures, supplemental
   tables and supplemental file sets (stored in the folder **figures_and_tables**).


## Citation

Please cite https://doi.org/10.5281/zenodo.1162683 and the corresponding paper,
if using these datasets and/or source code.


## Contact information

- Prof. Jochen H. M. Prehn (JPrehn@rcsi.ie);
- Manuela Salvucci (manuelasalvucci@rcsi.ie).


## Funding

This work was supported by generous funding from:

- The European Union Framework Programme 7 (FP7 APO-DECIDE, contract No. 306021);
- Science Foundation Ireland Investigator Award (13/IA/1881);
- The Irish Cancer Society BreastPredict Collaborative Research Centre (CCRC13GAL);
- Science Foundation Ireland/Department of Enterprise and Learning Partnership Award  (14/IA/2582). 
