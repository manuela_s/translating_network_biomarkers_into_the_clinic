function ComputeCIndicesStats()
% Compute c-index metric for DFS and OS from univariate Cox regression hazards
% models required for the analyses presented in the paper.

% Figure 5:
% - Generate 'rppa_combos_ensembles_cox.mat'
run.RPPACombosEnsemblesCox();

% Figures S6:
% - Panel D: generate 'permuted_tma_cox.mat'
run.PermutedTMACox();
end
