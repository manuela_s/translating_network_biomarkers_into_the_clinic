function FigureA1()
% Auxilliary figure 1.
% Correlation analysis between protein concentrations (in arbitrary units)
% measured by RPPA (golden standard) and TMA (median batch-corrected).

% Get TMA data
tma = get.BatchCorrectedTMA();
tma = grpstats(tma, {'universal_patient_id', 'protein'}, @nanmedian,...
    'DataVars', 'hscore',...
    'VarNames', {'universal_patient_id', 'protein', 'GroupCount', 'tma'});

% Get RPPA data
rppa = get.RawRPPA();
rppa.Properties.VariableNames{'concentration'} = 'rppa';

data = innerjoin(tma, rppa,...
    'keys', {'universal_patient_id', 'protein'},...
    'LeftVariables', {'universal_patient_id', 'protein', 'tma'});

% Make figure
data.protein = renamecats(data.protein,...
    {'caspase3', 'caspase9', 'smac', 'xiap'},...
    {'PC3', 'PC9', 'SMAC', 'XIAP'});
proteins = categories(removecats(data.protein));

figure();
p = matlab_utilities.panel.panel();
p.pack('h', numel(proteins));

for i = 1:numel(proteins)
    p(i).select();
    matlab_utilities.MakeCorrelationPlot(...
        data.rppa(data.protein == proteins{i}),...
        data.tma(data.protein == proteins{i}),...
        'marker_size', 3,...
        'has_fitting_line', false);
    title(p(i),...
        sprintf('%s (n=%d)', proteins{i}, sum(data.protein==proteins{i})));
    axis tight;
    set(gca, 'YLim', [0 300]);
end

xlabel(p, 'RPPA [a.u.]');
ylabel(p, 'IHC [a.u.]');

set(findobj(gcf, 'type', 'text'),...
    'fontname', 'arial',...
    'fontsize', 6);

p.fontname='arial';
p.fontsize=8;
p.children.margin=8;

p.export(...
    fullfile('figures_and_tables', 'FigureA1.pdf'),...
    '-w186',...
    '-h60',...
    '-rx');
end
