function FigureS5()
% Supplementary figure 5.
% Best trade-off tree for APOPTO-CELL predictions based solely on proteins
% measured by TMA ('caspase3' and 'xiap') and corresponding Kaplan-Meier
% plots and univariate Cox proportional hazard models for DFS and OS.

cli = get.Clinical();

figure();
p = matlab_utilities.panel.panel();

% Left TMA tree, right KM plots
p.pack('h', {[], {27}});

% Tree
PlotTMABestTree(p(1));

% KMs
p(2).pack('v', {{7}, []});
make.helpers.TreeKMPlot(...
    p(2,2),...
    cli,...
    'tma',...
    'Best tree',....
    'FigureS5', 'B', 'C');

p.margin = 5;
p(2).margin = [35, 15, 5, 5];

p.fontname = 'arial';
p.fontsize = 8;

p.export(...
    fullfile('figures_and_tables', 'FigureS5.pdf'),...
    '-w186',...
    '-h151',...
    '-rx');
end

function PlotTMABestTree(p)
% Identify and plot best prediction tree based on TMA measurements of
% 'caspase3' and 'xiap'. Plots tree into panel, and saves graphviz version
% to svg file.
% Input argument:
% - p: instance of panel class, panel to plot in.

trees = load(fullfile('outputs', 'optimized_tree_building_results_2inputs.mat'));
idx_best_tree = decision_tree.FindBestTree(...
    trees.accuracy, trees.average_proteins_measured);
tree = trees.tree{idx_best_tree};

p.select();
tree.plot();

make.helpers.ApoptoCellTreeGraphvizExport(tree, 'FigureS5_A', [0, 0, 0]);
end
