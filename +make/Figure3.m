function Figure3()
% Figure 3.
% Visualize decision trees to predict APOPTO-CELL output from protein inputs
% and corresponding statistics:
% - panel A: decision tree for APOPTO-CELL built using GINI index as metric
%   for splitting nodes;
% - panel B: decision tree for APOPTO-CELL built using GINI index factoring
%   in a penalty per any extra required protein as metric for splitting nodes; 
% - panel C: plot tree-size vs. accuracy for trees built with GINI index
%   and penalized GINI index;
% - panel D: plot accuracy vs. number of required proteins for:
%   - trees built with GINI index and penalized GINI index;
%   - ensembles (see Figure 2);
% - panel E: plot protein requirements (and corresponding APOPTO-CELL
%   predictions).

% Get trees as generated by 'run.OptimizeTreeBuilding5Inputs'
trees = load(fullfile('outputs', 'optimized_tree_building_results_5inputs.mat'));
% Find best tree
idx_best_tree = decision_tree.FindBestTree(...
    trees.accuracy, trees.average_proteins_measured);

% Get data from different protein combination ensembles 
combos = make.helpers.GetPermutationData();

% Get colors
colors = make.helpers.GetColors();

% Make figure
figure();
p = matlab_utilities.panel.panel();
% Split in one row for trees (panels A-B) and one for tree statistics
% (panels C-E)
p.pack('v', {[] {70}});

% Split for panel A and B with trees built with GINI index and penalized
% GINI index, respectively
p(1).pack('h', 2);

% Plot trees with confidence threshold 0.74 for visualization purposes
% (best tree has higher confidence threshold, but is too large for the
% figure)
p(1,1).select();
PlotTree(...
    trees.tree{trees.confidence_thresholds==0.74 & trees.costs_per_new_protein==0},...
    colors('gini_index'),...
    'A');

p(1,2).select();
PlotTree(...
    trees.tree{trees.confidence_thresholds==0.74 & trees.costs_per_new_protein==0.5},...
    colors('penalized_gini_index'),...
    'B');

% Split second row in two (plots and legends), and in 3 columns (panels C-E) 
p(2).pack({{40}, []}, 3);

p(2,1,1).select();
TreesComplexityPlot(trees, colors);
p(2,2,1).select(legend());

p(2,1,2).select();
NumProteinsPlot(combos, trees, colors);
p(2,2,2).select(legend());

p(2,1,3).select();
ProteinCombinationBarPlot(trees.tree{idx_best_tree}, trees.t, colors);

p.fontname = 'arial';
p.fontsize = 8;

p.marginbottom = 5;
p(1).margin = 5;
p(2).marginbottom = 10;

p.export(...
    fullfile('figures_and_tables', 'Figure3_B_C_D.pdf'),...
    '-rx',...
    '-w172',...
    '-h198');
end

function PlotTree(tree, color, panel)
% Panel A or B, plot decision tree for APOPTO-CELL.
% Plot in-place for matlab figure and export graphviz tree to svg file.
% Input arguments:
% - tree: instance of 'decision_tree.Tree' class, tree to plot;
% - color: double vector, RGB color for tree branches;
% - panel: char, panel label to use for file export name.

tree.plot();

make.helpers.ApoptoCellTreeGraphvizExport(...
    tree,...
    sprintf('Figure3_%s', panel),...
    color);
end

function TreesComplexityPlot(trees, colors)
% Panel C, plot tree size vs accuracy (agreement with ground-truth
% APOPTO-CELL).
% Input arguments:
% - trees: struct, as generated by 'run.OptimizeTreeBuilding5Inputs';
% - colors: map, mapping plot labels to RGB colors, as returned by
%   'make.helpers.GetColors'.

set(gca, 'NextPlot', 'add');

% Plot for trees built with GINI index cost function
plot(...
    trees.accuracy(trees.costs_per_new_protein==0),...
    trees.tree_size(trees.costs_per_new_protein==0),...
    'DisplayName', 'Tree with GINI index',...
    'Marker', 's', 'MarkerSize', 2,...
    'MarkerFaceColor', colors('gini_index'),...
    'MarkerEdgeColor', colors('gini_index'),...
    'color', colors('gini_index'),...
    'linewidth', 0.25);

% Plot for trees built with penalized GINI index cost function
plot(...
    trees.accuracy(trees.costs_per_new_protein==0.5),...
    trees.tree_size(trees.costs_per_new_protein==0.5),...
    '-r.',...
    'DisplayName', 'Tree with GINI index and per protein penalty',...
    'Marker', 's',...
    'MarkerSize', 2,...
    'MarkerFaceColor', colors('penalized_gini_index'),...
    'MarkerEdgeColor', colors('penalized_gini_index'),...
    'color', colors('penalized_gini_index'),...
    'linewidth', 0.25);

xlabel('Accuracy');
ylabel('Tree size');

legend('show');

set(gca,...
    'TickDir', 'out',...
    'XLim', [0.6 1],...
    'XTick', 0:0.1:1,...
    'YScale', 'log',...
    'YLim', [1 100000],...
    'YTick', [1, 10, 100, 1000, 10000, 100000],...
    'YMinorTick', 'on',...
    'XColor', 'k',...
    'YColor', 'k',...
    'box', 'on',...
    'linewidth', 1);
end

function NumProteinsPlot(combos, trees, colors)
% Panel D, plot accuracy (agreement with ground-truth APOPTO-CELL) vs.
% number of required proteins for the Tree and Ensemble approaches.
% Input arguments:
% - combos: table, one row per ensemble combination, as returned by
%   'make.helpers.GetPermutationData';
% - trees: struct, as generated by 'run.OptimizeTreeBuilding5Inputs';
% - colors: map, mapping plot labels to RGB colors, as returned by
%   'make.helpers.GetColors'.

combos.num_proteins = sum(combos{:, 1:5}, 2);

set(gca, 'NextPlot', 'add');

% Plot for trees built with GINI index cost function
plot(...
    trees.average_proteins_measured(trees.costs_per_new_protein==0),...
    trees.accuracy(trees.costs_per_new_protein==0).*100,...
    'DisplayName', 'Tree with GINI index',...
    'Marker', 's',...
    'MarkerSize', 2,...
    'MarkerFaceColor', colors('gini_index'),...
    'MarkerEdgeColor', colors('gini_index'),...
    'color', colors('gini_index'),...
    'linewidth', 0.25);

% Plot for trees built with penalized GINI index cost function
plot(...
    trees.average_proteins_measured(trees.costs_per_new_protein==0.5),...
    trees.accuracy(trees.costs_per_new_protein==0.5).*100,...
    'DisplayName', 'Tree with GINI index & per protein penalty',...
    'Marker', 's',...
    'MarkerSize', 2,...
    'MarkerFaceColor', colors('penalized_gini_index'),...
    'MarkerEdgeColor', colors('penalized_gini_index'),...
    'color', colors('penalized_gini_index'),...
    'linewidth', 0.25);

% Highlight best tree and plot trade-off line
OptimalTreePlot(trees.accuracy, trees.average_proteins_measured, colors);

% Plot for ensembles with different combination of protein inputs
plot(...
    combos.num_proteins,...
    combos.fraction_matches_full_apoptocell.*100,...
    's',...
    'DisplayName', 'Ensemble',...
    'Marker', 's',...
    'MarkerSize', 2,...
    'MarkerFaceColor', colors('ensembles'),...
    'MarkerEdgeColor', colors('ensembles'));

% Plot line between ensembles with best accuracy for each number of
% proteins
stats = grpstats(combos, 'num_proteins', @max, 'DataVars', 'fraction_matches_full_apoptocell');
plot(...
    stats.num_proteins,...
    stats.max_fraction_matches_full_apoptocell.*100,...
    'DisplayName', 'Best ensemble',...
    'Marker', 's',...
    'MarkerSize', 2,...
    'MarkerFaceColor', colors('ensembles'),...
    'MarkerEdgeColor', colors('ensembles'),...
    'Color', colors('ensembles'));

legend('show');
set(gca,...
    'XLim', [0 5],...
    'XTick', 0:5,...
    'YLim', [60 101],...
    'YTick', 60:10:100,...
    'TickDir', 'out',...
    'YMinorTick', 'on',...
    'XColor', 'k',...
    'YColor', 'k',...
    'box', 'on',...
    'linewidth', 0.25);

xlabel('Number of proteins');
ylabel('Accuracy');
end

function OptimalTreePlot(accuracy, average_proteins_measured, colors)
% Highlight best tree and plot trade-off line.
% Input arguments
% - accuracy: double matrix, accuracy [0, 1] for each candidate tree;
% - average_proteins_measured: double matrix with same shape as accuracy,
%   average number of proteins required to make predictions for each 
%   candidate tree; 
% - colors: map, mapping plot labels to RGB colors, as returned by 
%   'make.helpers.GetColors'.

% Identify the best tree and get the per_protein_penalty used for trade-off
[idx, per_protein_penalty] = decision_tree.FindBestTree(...
    accuracy, average_proteins_measured);

% High-light best tree
plot(...
    average_proteins_measured(idx),...
    accuracy(idx) .* 100,...
    'o',...
    'DisplayName', 'Best trade-off tree',...
    'MarkerSize', 4,...
    'MarkerEdgeColor', colors('tradeoff'));

% Plot trade-off line where trees with equivalent score for accuracy vs.
% number of required proteins lie for the selected per-protein penalty
l = refline(...
    per_protein_penalty,...
    accuracy(idx).*100-average_proteins_measured(idx).*per_protein_penalty);
set(l,...
    'color', colors('tradeoff'),...
    'linestyle', '--',...
    'linewidth', 1,...
    'DisplayName', 'Trade-off line');
end

function ProteinCombinationBarPlot(tree, t, colors)
% Panel E, bar plot with fraction of virtual patients that require
% different combinations of input proteins for the best tree.
% Input arguments:
% - tree: instance of 'decision_tree.Tree' class, tree to plot;
% - t: table, tree inputs for virtual patient as generated by 
%   'decision_tree.GetTreeTrainingData';
% - colors: map, mapping plot labels to RGB colors, as returned by
%   'make.helpers.GetColors'.

% Get tree predictions for the virtual patient cohort
predictions = tree.Predict(t);
predictions.num_proteins_measured = cellfun(@numel, predictions.proteins_measured);
predictions.proteins_measured_str = categorical(cellfun(@(x) strjoin(x, ' + '),...
    predictions.proteins_measured, 'UniformOutput', false));

% Group virtual patients based on ground-truth APOPTO-CELL signature (sc_class)
% and predicted class by Tree (predicted_class)
groups = grpstats(predictions,...
    {'num_proteins_measured', 'proteins_measured_str', 'sc_class', 'predicted_class'},...
    [], 'DataVars', false);
groups.percentage = 100 .* groups.GroupCount ./ sum(groups.GroupCount);
groups.group = categorical(groups.sc_class) .* groups.predicted_class;
% Merge the two groups where ground-truth APOPTO-CELL signature and Tree
% predictions disagree into a single group 'misclassified'
groups.group = mergecats(groups.group, {'SC<=25% SC>25%', 'SC>25% SC<=25%'}, 'misclassified');
groups.group = renamecats(groups.group, {'SC<=25% SC<=25%', 'SC>25% SC>25%'}, {'SC<=25%', 'SC>25%'});

% Unstack to get one row per combination of measured proteins and one
% column per outcome ('SC<=25%', 'SC>25%' or 'misclassified')
groups_w = unstack(groups, 'percentage', 'group',...
    'GroupingVariables', {'num_proteins_measured', 'proteins_measured_str'});

hb = bar(groups_w{:, 3:width(groups_w)}, 'stacked');
set(hb,...
    {'FaceColor'}, colors.values(categories(removecats(groups.group))),...
    {'DisplayName'}, categories(removecats(groups.group)));
ylabel('Virtual patients [%]');
legend('show', 'Location', 'NorthEast');

set(gca,...
    'XLim', [0.5 height(groups_w)+0.5],...
    'XTick', 1:height(groups_w),...
    'XTickLabel', cellstr(groups_w.proteins_measured_str),...
    'XTickLabelRotation', 30,...
    'TickDir', 'out',...
    'YMinorTick', 'on',...
    'XColor', 'k',...
    'YColor', 'k',...
    'box', 'on',...
    'linewidth', 1);
end
