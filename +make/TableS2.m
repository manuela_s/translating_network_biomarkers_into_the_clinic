function TableS2()
% Supplementary table 2.
% Univariate and multivariate Cox regression models corresponding to each
% Kaplan-Meier plot presented in the paper.
% Two multivariate models were fitted:
% - model 1: adjusted by T stage and lymphovascular invasion;
% - model 2: adjusted by T stage, lymphovascular invasion, age, sex and
%   tumour location.

cli = get.Clinical();

% Remove tumour_sites with low number of samples:
cli.tumour_site = removecats(cli.tumour_site, {'appendix', 'others'});

% Make table with survival information. One row per patient per
% survival_type
cli_per_survival = [...
    GetSingleSurvivalCli(cli, 'dfs');...
    GetSingleSurvivalCli(cli, 'os')];

% KMPair.ExportForCox writes one .mat file with data per KM pair (DFS and
% OS) in the figure. Run Cox models for each dataset
dir_name = fullfile('outputs', 'cox_models');
files = dir(dir_name);
files = {files(3:end).name}; % Skip . and ..
for i=1:numel(files)
    tmp = load(fullfile('outputs', 'cox_models', files{i}));
    block{i} = RunCoxForKMPair(cli_per_survival, tmp.data, tmp.metadata); %#ok<AGROW>
end

block = vertcat(block{:});

writetable(block, fullfile('figures_and_tables', 'TableS2.csv'));
end

function block = RunCoxForKMPair(cli_per_survival, data, metadata)
% Run univariate and multivariate Cox models for one dataset
% Input arguments:
% - cli_per_survival: table, clinical data (as returned by 'get.Clinical'),
%   but per survival_type ('dfs' and 'os');
% - data: table, one row per patient. Columns for universal_patient_id and
%   for predictor, as exported by KMPair.ExportForCox;
% - metadata: table, a single row with columns figure_label, dfs_panel,
%   os_panel and predictor_name, as exported by KMPair.ExportForCox;
% Output:
% block: table, results from uni/multivariate Cox regression models for
% export to csv file.

j = innerjoin(cli_per_survival, data, 'key', 'universal_patient_id');
j.T = removecats(mergecats(j.T, {'2', '3'}, '2_or_3'));

predictor_name = metadata.predictor_name{1};
uni_block = GetStatsPerCoxType(j, predictor_name, 'uni', {});
mv1_block = GetStatsPerCoxType(j, predictor_name, 'mv1', {'T', 'lymphovascular_invasion'});
mv2_block = GetStatsPerCoxType(j, predictor_name, 'mv2', {'T', 'lymphovascular_invasion',...
    'age', 'sex', 'tumour_site'});

metadata_block = sortrows(repmat(stack(metadata, {'dfs_panel', 'os_panel'},...
    'IndexVariableName', 'survival_type', 'NewDataVariableName', 'panel'),...
    height(uni_block)/2, 1), 'survival_type');
metadata_block.survival_type = renamecats(metadata_block.survival_type,...
    {'dfs_panel', 'os_panel'},...
    {'dfs', 'os'});

block = [...
    metadata_block(:, {'figure_label', 'panel', 'survival_type'}),...
    uni_block(:, 2:end),...
    mv1_block(:, 3:end),...
    mv2_block(:, 3:end)];
end

function block_stats = GetStatsPerCoxType(j, predictor_name, colname_prefix, additional_terms)
% Run univariate or multivariate Cox regression model and format output for
% csv file.
% Input arguments:
% - j: table, one row per patient/survival_type and columns for 'universal_patient_id',
%   survival and predictors (see 'get.Clinical');
% - predictor_name: string, name of column in table j with primary predictor;
% - colname_prefix: string, prefix to use in output column names. Either
%   'uni', 'mv1' or 'mv2';
% - additional_terms: cell array, names of columns in table j to use as
%   additional terms to adjust multivariate Cox models by;
% Output:
% - block_stats: table, for each survival_type ('dfs' or 'os') rows for:
%   - overall model
%   - primary predictor
%   - for categorical predictors, one row per category other than the
%     reference level.
%   and columns:
%   - 'survival_type': string, type of survival. Either 'dfs' or 'os';
%   - 'predictor': string, formatted name of the primary predictor. Either:
%      - empty string for overall model;
%      - name of primary predictor optionally concatenated with reference
%        level for categorical predictor;
%      - predictor levels (other than reference) for categorical predictor;
%   - 'hr_ci': hazard ratio and 95% confidence interval for predictor;
%   - 'lrt_p': likelihood ratio test p-value from Cox model;
%   - 'c_index': c-index metric from Cox model.
% See help in 'matlab_utilities.RunMultipleCoxModelsR'.

[full, per_term, per_term_level] =...
    matlab_utilities.RunMultipleCoxModelsR(...
    j.survival_type,...
    j.survival,...
    j.is_censored=='no',...
    j(:,[{predictor_name}, additional_terms]));

% Get overall metrics for the predictor in overall_stats.
% Construct predictor string for cox terms:
per_term.predictor = rowfun(...
    @(cox_term, ref_level) sprintf('%s (ref. %s)', char(cox_term), char(ref_level)),...
    per_term,...
    'InputVariables', {'cox_term', 'ref_level'},...
    'OutputFormat', 'cell');
per_term_level.predictor = cellfun(@(p) sprintf('    %s', p),...
    per_term_level.cox_term_level,...
    'UniformOutput', false);

% Combine Cox stats for overall model and term- and term-level-stats for
% primary predictor:
block_stats = matlab_utilities.ConcatenateTables(...
    {full,...
    per_term(categorical(per_term.cox_term) == predictor_name,:),...
    per_term_level(categorical(per_term_level.cox_term) == predictor_name,:)});
block_stats = sortrows(block_stats, 'sample_set');

% Format content:
block_stats.lrt_p = regexprep(...
    arrayfun(@matlab_utilities.FormatPValue, block_stats.lrt_p, 'UniformOutput', false),...
    '(p=?|NaN)', '');
block_stats.hr_ci = regexprep(...
    arrayfun(@(hr, ci1, ci2) sprintf('%.2f (%.2f-%.2f)', hr, ci1, ci2),...
    block_stats.hr, block_stats.low_ci, block_stats.high_ci,...
    'UniformOutput', false),...
    'NaN.*', '');
block_stats.c_index =  regexprep(...
    arrayfun(@(x) num2str(x, '%.2f'), block_stats.c_index, 'UniformOutput', false),...
    'NaN', '');

block_stats.Properties.VariableNames{'sample_set'} = 'survival_type';
block_stats = block_stats(:, ...
    {'survival_type', 'predictor', 'hr_ci', 'lrt_p', 'c_index'});

% Prepend colname_prefix on all uni/multivariate specific columns:
block_stats.Properties.VariableNames(3:end) =...
    strcat(colname_prefix, '_', block_stats.Properties.VariableNames(3:end));
end

function cli = GetSingleSurvivalCli(cli, survival_type)
% Get clinical data for a given survival type.
% Input arguments:
% - cli: table, clinical data, as returned by 'get.Clinical'
% - survival_type: string, 'dfs' or 'os'
% Outputs:
% - cli: table, clinical data, but with survival data only for given
%   survival type in columns 'survival' and 'is_censored'.

switch survival_type
    case 'dfs'
        cli.survival = cli.disease_free_survival_months;
        cli.is_censored = cli.is_censored_disease_free_survival;
    case 'os'
        cli.survival = cli.overall_survival_months;
        cli.is_censored = cli.is_censored_overall_survival;
    otherwise
        error('Unsupported survival_type: %s', survival_type);
end

cli.survival_type = categorical(repmat({survival_type}, height(cli), 1));
cli(:, {'disease_free_survival_months', 'overall_survival_months',...
    'is_censored_disease_free_survival', 'is_censored_overall_survival'}) = [];
end
