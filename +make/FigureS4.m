function FigureS4()
% Supplementary figure 4.
% - panels A-D: Kaplan-Meier plots and univariate Cox regression estimates
%   for DFS and OS to evaluate the prognostic value of single proteins
%   measured by TMA (panels A-B: 'caspase3'; panels C-D: 'xiap').

% Get Clinical data
cli = get.Clinical();

% Get TMA-based single protein measurements
inputs = load(fullfile('outputs', 'inputs.mat'), 'tma');

% Join clinical and molecular data
t = innerjoin(cli, inputs.tma, 'keys', 'universal_patient_id');

% Dichotomize patients in groups using the median concentration as cut-off
t.caspase3_class = categorical(...
    matlab_utilities.iif(t.caspase3 > median(t.caspase3), '>median', '<=median'));
t.caspase3_class = reordercats(t.caspase3_class, {'>median', '<=median'});

t.xiap_class = categorical(...
    matlab_utilities.iif(t.xiap > median(t.xiap), '>median', '<=median'));
t.xiap_class = reordercats(t.xiap_class, {'>median', '<=median'});

proteins = {'caspase3_class', 'xiap_class'};
pretty_proteins = {'PC3', 'XIAP'};

figure();
p = matlab_utilities.panel.panel();
p.pack('h', 2); % One panel for each protein

for i=1:numel(proteins)
    p(i).pack('v', {[], {10}}); % Set aside 10mm at the bottom for legend
    km = make.helpers.KMPair(t, proteins{i});
    km.ExportForCox('FigureS4', char(63+2*i), char(64+2*i));
    % Split for DFS/OS, and set aside 7mm at the bottom on each side for risk table
    p(i,1).pack('h', 2, {[], {7}}); 
    p(i,1).margin = [15, 5, 5, 5];
    p(i,1,1,1).select(); % For DFS
    p(i,1,1,1).marginbottom = 10;
    p(i,1,1,2).select(); % For DFS risk table
    p(i,1,1,2).margin = 0;
    axes(p(i,1,1,1).object); %#ok<LAXES>
    km.DFS('risk_table_axes', p(i,1,1,2).object);
    lh{1} = legend();
    p(i,1,2,1).select(); % For OS
    p(i,1,2,1).marginbottom = 10;
    p(i,1,2,2).select(); % For OS risk table
    p(i,1,2,2).margin = 0;
    axes(p(i,1,2,1).object); %#ok<LAXES>
    km.OS('risk_table_axes', p(i,1,2,2).object);
    lh{2} = legend();
    title(p(i), pretty_proteins{i});
    matlab_utilities.CombineLegends(p(i,2), lh);
    p(i,2).margin = 0;
    p(i).marginleft = 20;
end

% Set fontname and size for the text in the risk table
set(findobj(gcf, 'type', 'text', 'tag', 'risk_table'),...
    'fontname', 'arial',...
    'fontsize', 8);

p.margin = [15, 5, 5, 10];
p.fontname = 'arial';
p.fontsize = 8;

p.export(...
    fullfile('figures_and_tables', 'FigureS4.pdf'),...
    '-w178',...
    '-h74',...
    '-rx');
end
