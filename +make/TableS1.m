function TableS1()
% Supplementary table 1.
% Table with baseline clinical charcateristics for the in-house cohort broken
% down for patients with protein measurements by RPPA and RPPA+TMA.

% Read in clinical data and table with patients included in simulations
cli = get.Clinical();
inputs = load(fullfile('outputs', 'inputs.mat'));

% Get clinical characteristics for patients with protein measurements by
% RPPA and RPPA and TMA
rppa = AnalyzePatientsForMeasurementType(cli, inputs.rppa, 'RPPA');
rppa_and_tma = AnalyzePatientsForMeasurementType(cli, inputs.rppa_and_tma, 'RPPA+TMA');

% Combine results from both measurement types
assert(isequal(rppa.description, rppa_and_tma.description));
j = table(rppa.description, rppa.values, rppa_and_tma.values);

% Export table as csv file
writetable(j,...
    fullfile('figures_and_tables', 'TableS1.csv'),...
    'QuoteStrings', true,...
    'WriteVariableNames', false);
end

function t = AnalyzePatientsForMeasurementType(cli, data, measurement_type)
% Generate output for table S1 for one measurement type
% Input arguments:
% - cli: table, clinical data as returned by 'get.Clinical';
% - data: table, protein measurements for this measurement type;
% - measurement_type: string, name of measurement type.

% Subset clinical data to the patients that we ran simulations for
data = innerjoin(cli, data, 'key', 'universal_patient_id', 'RightVariables', {});

% Overall patient stats
patients_stats = table(...
    {''},...
    {sprintf('%s (n=%d)', measurement_type, height(data))},...
    'VariableNames', {'description', 'values'});
    
% Stats for each key variable
cols = {'age', 'sex', 'T', 'N', 'M', 'tumour_site',...
    'lymphovascular_invasion', 'KRAS_status', 'KRAS_mutation_type'};
for i=1:numel(cols)
    per_variable{i} = AnalyzeCliVariable(data, cols{i}); %#ok<AGROW>
end

% Stats for follow-up:
survival_stats = FollowupStats(data);

% Combine headings, body of the table and progression-free survival median table 
t = [patients_stats; vertcat(per_variable{:}); survival_stats;];
end

function surv_stats = FollowupStats(cli)
% Computes median follow-up
% Input argument:
% - cli: table, clinical data as returned by 'get.Clinical' for the patients
%   to compute follow-up stats for;
% Output:
% - surv_stats: table, one row per survival_type ('dfs' or 'os') and columns
%   'description', and 'values'.

surv_stats = matlab_utilities.ComputeMedianSurvivalR(...
    reshape(categorical(repmat({'dfs', 'os'}, height(cli), 1)), [], 1),...
    [cli.disease_free_survival_months; cli.overall_survival_months],...
    [cli.is_censored_disease_free_survival=='yes'; cli.is_censored_overall_survival=='yes']);
surv_stats.description = cellfun(...
    @(s) sprintf('%s follow-up (median, 95%% CI) [months]', upper(s)),...
    surv_stats.sample_set,...
    'UniformOutput', false);
surv_stats.values = arrayfun(...
    @(m,lci,hci) sprintf('%.0f (%.0f-%.0f)',m,lci,hci),...
    surv_stats.median_survival, surv_stats.low_ci, surv_stats.high_ci,...
    'UniformOutput', false);
surv_stats = surv_stats(:, {'description', 'values'});
end

function t_out = AnalyzeCliVariable(cli, col_name)
% Analyze one variable of clinical data
% Input arguments:
% - cli: table, as returned by 'get.Clinical';
% - col_name: string, name of column in cli to process
% Returns a table with colums:
% - description: description of the predictor/category level.
% - values: median/range for continuous predictors and n/% for
%   categorical predictors.

values = cli.(col_name);
pretty_col_name = regexprep(strcat(upper(col_name(1)), col_name(2:end)), '_', ' ');

if iscategorical(cli.(col_name))
    values = removecats(values);
    if any(isundefined(values))
        values(isundefined(values)) = 'not available';
    end
    stats = tabulate(values);
    description = [{pretty_col_name};...
        cellfun(@(s) sprintf('    %s', s), regexprep(categories(values), '_', ' '), 'UniformOutput', false)];
    values = [{''};...
        cellfun(@(n, p) sprintf('%d (%.0f%%)', n, p), stats(:,2), stats(:,3), 'UniformOutput', false)];
else
    description = {sprintf('%s (median, range)', pretty_col_name)};
    values = {sprintf('%.0f (%.0f-%.0f)', nanmedian(values), nanmin(values), nanmax(values))};    
    % Include units in description when appropriate
    if strcmp(col_name, 'age')
        description{1} = sprintf('%s [years]', description{1});
    end
end
    t_out = table(description, values);
end
