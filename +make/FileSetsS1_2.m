function FileSetsS1_2()
% Create supplementary file sets 1 and 2.
% Generate SVG files for decision tree built to encode APOPTO-CELL using 5 
% protein inputs as predictors.
% - file set S1: decision tree built with GINI index as cost function,
% - file set S2: decision tree built with penalized GINI index as cost
%   function with best trade-off between accuracy and number of required proteins.

trees = load(fullfile('outputs', 'optimized_tree_building_results_5inputs.mat'));

idx_best_tree = decision_tree.FindBestTree(...
    trees.accuracy, trees.average_proteins_measured);

% Get colors
colors = make.helpers.GetColors();

% Supplementary File Set 1
make.helpers.ApoptoCellTreeGraphvizExport(...
    trees.tree{trees.confidence_thresholds==1 & trees.costs_per_new_protein==0},...
    'FileSetS1',...
    colors('gini_index'));

% Supplementary File Set 2
make.helpers.ApoptoCellTreeGraphvizExport(...
    trees.tree{idx_best_tree},...
    'FileSetS2',...
    colors('penalized_gini_index'));
end
