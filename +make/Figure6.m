function Figure6()
% Figure 6.
% Plots for TMA-based quantification for stage III patients of the
% validation cohort.
% Generate 2 sets of plots:
% - top section: add place-holders for representative immunohistochemistry
%   images and plot corresponding dot/boxplot for 'caspase3' and 'xiap';
% - bottom section: 8 Kaplan-Meier plots and univariate Cox regression
%   models organised in a 2 x 4 grid. The 2 rows include plots for DFS and
%   OS. Columns include plots based on signatures derived from:
%   - 'apoptotic score' from combining 'caspase3' and 'xiap' expressions;
%   - simulations with a 'naive' approach (patient-specific protein expression
%     for 'caspase3' and 'xiap' and median expression for 'caspase9', 'smac'
%     and 'xiap');
%   - Ensemble simulations with patient-specific quantifications for 'caspase3'
%     and 'xiap' and clinically-relevant range for 'apaf1', 'caspase9' and 
%     'smac';
%   - predictions based on best Tree built using up to 4 protein inputs
%     measured by RPPA ('caspase9' and 'smac') and TMA ('caspase3' and 'xiap');

% Get clinical data (for MedianOrEnsembleKMPlot and TreeKMPlot)
cli = get.Clinical();

% Get TMA data (for protein distributions)
tma = load(fullfile('outputs', 'inputs.mat'), 'tma');
tma = tma.tma;

% Derive 'apoptotic score' signature by combining expression of 'caspase3' and 'xiap':
tma.caspase3_xiap = categorical(...
    matlab_utilities.iif(...
    tma.caspase3 > median(tma.caspase3) & tma.xiap <= median(tma.xiap), 'high-apoptosis',...
    matlab_utilities.iif(...
    tma.caspase3 <= median(tma.caspase3) & tma.xiap > median(tma.xiap), 'low-apoptosis',...
    'intermediate-apoptosis')));
tma.caspase3_xiap = reordercats(tma.caspase3_xiap,...
    {'high-apoptosis', 'intermediate-apoptosis', 'low-apoptosis'});

% Join clinical and molecular data (for apoptotic score plot)
j = innerjoin(cli, tma, 'keys', 'universal_patient_id');

% Make figure
figure();
p = matlab_utilities.panel.panel();

% Pack 2 panels for top and bottom section, respectively
p.pack('v', {{58}, {119}});

% Top section
p(1).margin = [7, 15, 7, 5];
p(1).pack(2,4);
p(1,1).marginbottom = 4;
p(1,2).margintop = 4;
p(1,1,4).marginleft = 20;
p(1,2,4).marginleft = 20;

AddEmptyPlot(p(1,1,1));
AddEmptyPlot(p(1,1,2));
AddEmptyPlot(p(1,1,3));
p(1,1,4).select();
PlotProteinHelper(p(1,1,4), tma.caspase3);

AddEmptyPlot(p(1,2,1));
AddEmptyPlot(p(1,2,2));
AddEmptyPlot(p(1,2,3));
p(1,2,4).select();
PlotProteinHelper(p(1,2,4), tma.xiap);

% Bottom section
p(2).pack('h', 4);

% First column: apoptotic score
km = make.helpers.KMPair(j, 'caspase3_xiap');
km.VerticalLayout(p(2,1));
title(p(2,1), 'Apoptosis score');
km.ExportForCox('Figure6', 'C', 'G');

% Second column: APOPTO-CELL simulations with 'naive' approach
make.helpers.MedianOrEnsembleKMPlot(...
    p(2,2),...
    cli,...
    'tma_2proteins_medians_simulations.mat',...
    'Naive approach',...
    'Figure6', 'D', 'H');

% Third column: APOPTO-CELL simulations with ensemble with patient-specific
% measurements for 'caspase3' and 'xiap'
make.helpers.MedianOrEnsembleKMPlot(...
    p(2,3),...
    cli,...
    'tma_2proteins_ensemble_simulations.mat',...
    'Best 4 Ps',...
    'Figure6', 'E', 'I');

% Forth column: APOPTO-CELL predictions with optimal Tree built with
% protein inputs measured by RPPA ('caspase9' and 'smac') and TMA
% ('caspase3' and 'xiap')
make.helpers.TreeKMPlot(...
    p(2,4),...
    cli,...
    'rppa_and_tma',...
    'Best tree',....
    'Figure6', 'F', 'J');

p(2).margin = [12, 20, 5, 5];
p.margin = [28, 5, 7, 5];

p.fontname = 'arial';
p.fontsize = 8;

p.export(...
    fullfile('figures_and_tables', 'Figure6.pdf'),...
    '-w179',...
    '-h202',...
    '-rx');
end

function PlotProteinHelper(p, protein_values)
% Make combined boxplot and dot plot for a protein.
% Input arguments:
% - p: instance of panel class, panel to plot in;
% - protein_values: vector, protein values.

p.select();
matlab_utilities.MakeCombinedBoxplotSpreadPlot(...
    protein_values,...
    {repmat({'default'}, numel(protein_values), 1)},...
    'colors', 'k', 'offset', 0.4, 'marker_size', 3, 'marker_type', '.');

ylabel('Concentration [\\muM]');

matlab_utilities.ticklabelformat(gca, 'y', '%.2f');

set(gca,...
    'XLim', [0.9 1.7],...
    'XTickLabel', {},...
    'YLim', [-0.01 0.6],...
    'YTick', 0:0.2:0.6,...
    'YMinorTick', 'on',...
    'TickDir', 'out',...
    'box', 'on',...
    'linewidth', 1,...
    'XColor', 'k',...
    'YColor', 'k');
end

function AddEmptyPlot(p)
% Add place holder plot for representative images of TMA staining for
% 'caspase3' and 'xiap'.
% Input argument:
% - p: instance of panel class, panel to plot in.

p.select();
set(gca,...
    'XTick', [],...
    'YTick', [],...
    'box', 'on',...
    'linewidth', 1,...
    'XColor', 'k',...
    'YColor', 'k');
end
