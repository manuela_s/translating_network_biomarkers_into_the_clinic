function FigureS3()
% Supplementary figure 3.
% Plot protein expression in tumor cores assessed by TMA before and after 
% batch correction.

% Get raw protein expression
raw = get.RawTMA();

% Get batch-corrected protein expression
corrected = get.BatchCorrectedTMA();

% Combine raw and batch-corrected protein expression
tma = innerjoin(raw, corrected,...
    'keys', {'universal_patient_id', 'sub_cohort', 'core', 'protein'});
    
colors = make.helpers.GetColors();
colors = cell2mat(colors.values(categories(tma.core .* tma.sub_cohort)));

tma.protein = renamecats(tma.protein,...
    {'caspase3', 'caspase9', 'smac', 'xiap'},...
    {'PC3', 'PC9', 'SMAC', 'XIAP'});
proteins = categories(tma.protein);

% Make figure
figure();
p = matlab_utilities.panel.panel();
% Pack panel for plots and legend, respectively
p.pack('v', {[], {5}});

p(1).pack(3, numel(proteins));
for i=1:numel(proteins)
    t = tma(tma.protein==proteins{i}, :);
    p(1,1,i).select();
    title(p(1,1,i), proteins{i});
    MakeBatchEffectBoxplot(t, 'hscore_raw', colors);
    p(1,2,i).select();
    MakeBatchEffectScatterPlot(t, colors);
    p(1,3,i).select();
    MakeBatchEffectBoxplot(t, 'hscore_corrected', colors);
end

hy_1_1 = ylabel(p(1,1),{'H-score before', 'ComBat batch correction'});

hx_1_2 = xlabel(p(1,2), {'H-score before', 'ComBat batch correction'});
hy_1_2 = ylabel(p(1,2), {'H-score after', 'ComBat batch correction'});

hy_1_3 = ylabel(p(1,3), {'H-score after', 'ComBat batch correction'});

set([hy_1_1, hx_1_2, hy_1_2, hy_1_3], 'Color', 'k');

legend_h = findobj(gcf, 'type', 'legend');
set(legend_h, 'Orientation', 'horizontal');
matlab_utilities.CombineLegends(p(2), num2cell(legend_h));

p(1).margin = [10, 15, 5, 5];
p(2).margin = 5;
p.margin = [15, 5, 5, 5];

p.fontname = 'arial';
p.fontsize = 8;

p.export(...
    fullfile('figures_and_tables', 'FigureS3.pdf'),...
    '-w168',...
    '-h148',...
    '-rx');
end

function MakeBatchEffectBoxplot(single_protein_tab, col, colors)
% Make boxplot for batch effects for one protein.
% Input arguments:
% - single_protein_tab: table, data for the protein to plot;
% - col: string, column name for protein concentrations;
% - colors: matrix, RGB color values for each batch.

boxplot(...
    single_protein_tab.(col),...
    {single_protein_tab.core, single_protein_tab.sub_cohort},...
    'plotstyle', 'compact',...
    'factorseparator', 1,...
    'colors', colors,...
    'colorgroup', {single_protein_tab.core, single_protein_tab.sub_cohort},...
    'symbol', '.',...
    'labelverbosity', 'all',...
    'medianstyle', 'line');

set(gca,...
    'XColor', 'k',...
    'YColor', 'k',...
    'box', 'on',...
    'linewidth', 1,...
    'YMinorTick', 'on',...
    'TickDir', 'out');

ylim([0 300]);
end

function MakeBatchEffectScatterPlot(t, colors)
% Make scatter plot for TMA expression for one protein before and
% after batch correction.
% Input arguments:
% - t: table, one row per patient/core. Columns include:
%   - hscore_raw: double, protein concentration before batch correction;
%   - hscore_corrected: double, protein concentration before after correction;
%   - t.core: categorical, identifier of the core ('A', 'B' or 'C');
%   - t.sub_cohort: categorical, identifier of the sub_cohort the patient
%     belonged to ('1' or '2');
% - colors: matrix, RGB color values for each batch.

h = gscatter(...
    t.hscore_raw,...
    t.hscore_corrected,...
    t.core .* t.sub_cohort,...
    colors,...
    '.',...
    4);

set(h, {'MarkerFaceColor'}, num2cell(colors, 2));

set(gca,...
    'XColor', 'k',...
    'YColor', 'k',...
    'box', 'on',...
    'linewidth', 1,...
    'XMinortick', 'on',...
    'YMinorTick', 'on',...
    'TickDir', 'out');

xlim([0 300]);
ylim([0 300]);
end
