function Figure2()
% Figure 2, panels A-C.

% Panel A
make.Figure2PanelAScheme();
% Panel B
make.Figure2PanelB();
% Panel C
make.Figure2PanelC();
end
