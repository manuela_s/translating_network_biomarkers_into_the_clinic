function colors = GetColors()
% Get colors for manuscript.
% Output:
% - colors: map, group names to RGB values.

medium_blue = [0, 115, 194]./255; % #0073C2
yellow = [239, 192, 0]./255; % #EFC000
light_gray = [134, 134, 134]./255; % #868686 
light_red = [206, 83, 76]./255; % #CD534C 
light_blue = [122, 165, 215]./255; % #7AA6DC 
dark_blue = [19, 61, 102]./255; % #003C67 
dark_yellow = [142, 120, 46]./255; % #8F7700 
dark_gray = [59, 59, 59]./255; % #3B3B3B
dark_red = [167, 49, 48]./255; % #A73030 
medium_blue_violet = [74, 105, 144]./255; % #4A6990

% Other colors
white = [255, 255, 255]./255; % #FFFFFF

colors = containers.Map();

colors('SC>25%') = medium_blue;
colors('SC<=25%') = yellow;
colors('SC>25% misclassified') = dark_blue;
colors('SC<=25% misclassified') = dark_yellow;
colors('misclassified') = light_gray;
colors('inconclusive') = light_gray;
colors('discordant') = light_gray;

colors('high-apoptosis') = dark_blue;
colors('intermediate-apoptosis') = medium_blue_violet;
colors('low-apoptosis') = light_blue;

colors('low-risk') = medium_blue;
colors('medium-risk') = light_gray;
colors('high-risk') = yellow;

colors('>median') = dark_gray;
colors('<=median') = light_gray;

colors('dfs') = light_blue;
colors('os') = dark_blue;

% Colors for core .* sub_cohort batches:
colors('A 1') = light_blue;
colors('A 2') =  dark_blue;
colors('B 1') =  yellow;
colors('B 2') =  dark_yellow;
colors('C 1') =  light_red;
colors('C 2') =  dark_red;

% Colors for Figure 2B:
colors('available') = light_gray;
colors('not_available') = white;

% Colors for Figure 3:
colors('gini_index') = light_red;
colors('penalized_gini_index') = dark_red;
colors('ensembles') = light_gray;
colors('tradeoff') = medium_blue_violet;

% Colors for Figure S1:
colors('included') = light_blue;
colors('excluded') = light_red;
end
