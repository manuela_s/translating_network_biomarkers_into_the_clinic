function MedianOrEnsembleKMPlot(p, j, filename, title_str, figure_label, dfs_panel, os_panel)
% Make Kaplan-Meier plots for simulations made with 'run.SingleSimulationPerPatient'
% or 'run.EnsembleSimulations'.
% Input arguments:
% - p: instance of panel class, panel to plot in;
% - j: table with clinical data. One row per patient. Required columns:
%   - universal_patient_id;
%   - disease_free_survival;
%   - is_censored_disease_free_survival;
%   - overall_survival_months;
%   - is_censored_overall_survival;
% - filename: string, name of mat file created by 'run.SingleSimulationPerPatient'
%   or run.EnsembleSimulations;
% - title_str: string, title for the panel;
% - figure_label: string, label used by 'ExportForCox' method from 'KMPair' class
%   to cross-reference this figure in Cox model stats in table S2;
% - dfs_label: char, label used by 'ExportForCox' method from 'KMPair' class
%   to cross-reference the DFS panel in Cox model stats in table S2;
% - os_label: char, label used by 'ExportForCox' method from 'KMPair' class
%   to cross-reference the DFS panel in Cox model stats in table S2.

sims = load(fullfile('outputs', filename));
sims = sims.results;
j2 = innerjoin(j, sims, 'keys', 'universal_patient_id',...
    'RightVariables', 'substrate_cleavage_class');

km = make.helpers.KMPair(j2, 'substrate_cleavage_class');
km.VerticalLayout(p);
title(p, title_str);

km.ExportForCox(figure_label, dfs_panel, os_panel);
end
