classdef KMPair
    % Make paired Kaplan-Maier plot for DFS/OS and exports corresponding
    % data for univariate/multivariate Cox regression models.
    
    properties
        cli; % clinical data;
        predictor_name; % column in cli used for grouping
    end
    
    methods
        function obj = KMPair(cli, predictor_name)
            % Input arguments:
            % - cli: table, clinical data with one row per patient and must
            %   include the following columns: 
            %   - disease_free_survival;
            %   - is_censored_disease_free_survival;
            %   - overall_survival_months;
            %   - is_censored_overall_survival;  
            % - predictor_name: string, name of column in cli with
            %   predictor to group patients by (categorical).
            
            assert(istable(cli));
            assert(ischar(predictor_name));
            assert(iscategorical(cli.(predictor_name)));
            obj.cli = cli;
            obj.predictor_name = predictor_name;
        end
        
        function DFS(obj, varargin)
            % Plots Kaplan-Meier for disease-free survival.
            % Input argument:
            % - varargin: extra argument(s) passed to 'cohort_utilities.MakeKaplanMeierPlot'
            
            assert(~any(isundefined(obj.cli.is_censored_disease_free_survival)));
            obj.KM(...
                obj.cli.disease_free_survival_months,...
                obj.cli.is_censored_disease_free_survival == 'yes',...
                'Time to recurrence [months]',...
                'Disease-free survival',...
                varargin{:});
        end
        
        function OS(obj, varargin)
            % Plots Kaplan-Meier for overall survival.
            % Input argument:
            % - varargin: extra argument(s) passed to 'cohort_utilities.MakeKaplanMeierPlot'
            
            assert(~any(isundefined(obj.cli.is_censored_overall_survival)));
            obj.KM(...
                obj.cli.overall_survival_months,...
                obj.cli.is_censored_overall_survival == 'yes',...
                'Time to death [months]',...
                'Overall survival',...
                varargin{:});
        end
        
        function VerticalLayout(obj, p)
            % Create vertical layout panels with both DFS and OS Kaplan-Meier plots.
            % Input argument:
            % - p: instance of panel class, parent panel for plots.
            
            % Panels for DFS KM, DFS risk-table, OS KM, OS risk-table and legend. 
            p.pack('v', {[], {10}, [], {10}, {15}});
            p(1).marginbottom = 10;
            p(3).marginbottom = 10;
            p(2).margin = 0;
            p(4).margin = 0;
            p(1).select();
            p(2).select();
            axes(p(1).object);
            obj.DFS('risk_table_axes', p(2).object);
            lh{1} = legend();
            p(3).select();
            p(4).select();
            axes(p(3).object);
            obj.OS('risk_table_axes', p(4).object);
            lh{2} = legend();
            
            % Set fontname/size for the text in the risk-table
            set(findobj([p(2).object, p(4).object],...
                'type', 'text', 'tag', 'risk_table'),...
                'fontname', 'arial',...
                'fontsize', 8);
            
            % Set YLim on risk table axis to always make space for
            % 3 groups to make them aligned across panels.
            set([p(2).object, p(4).object], 'YLim', [-3 0]);
            
            matlab_utilities.CombineLegends(p(5), lh, 'create_subpanels', false);
        end
        
        function ExportForCox(obj, figure_label, dfs_panel, os_panel)
            % Export mat file with per-patient predictor information for
            % Cox regression analysis for OS and DFS in Table S2.
            % Input arguments:
            % - figure_label: string, label to use for this figure in table S2;
            % - dfs_panel: char, label used for DFS panel;
            % - os_panel: char, label used for OS panel.
            
            dir_name = fullfile('outputs', 'cox_models');
            if exist(dir_name, 'dir')==0
                mkdir(dir_name);
            end
            filename = fullfile(dir_name,...
                sprintf('%s_%s_%s.mat', figure_label, dfs_panel, os_panel));
            
            data = table(...
                obj.cli.universal_patient_id, obj.cli.(obj.predictor_name),...
                'VariableNames', {'universal_patient_id', obj.predictor_name}); %#ok<NASGU>
            metadata = table({figure_label}, {dfs_panel}, {os_panel}, {obj.predictor_name},...
                'VariableNames', {'figure_label', 'dfs_panel', 'os_panel', 'predictor_name'}); %#ok<NASGU>
            save(filename, 'data', 'metadata');
        end
    end
    
    methods (Access=private)
        function KM(obj, survival, censored, xlab, ylab, varargin)
            % Plot Kaplan-Meier curve to gca.
            % Input arguments:
            % - survival: vector, per-patient survival in months;
            % - censored: logical vector, per-patient censoring information;
            % - xlab: string, xlabel for plot;
            % - ylab: string, ylabel for plot;
            % - varargin: extra argument(s) passed to 'cohort_utilities.MakeKaplanMeierPlot'.

            colors = make.helpers.GetColors();
            groups = obj.cli.(obj.predictor_name);
            cohort_utilities.MakeKaplanMeierPlot(...
                survival,...
                censored,...
                'stats_fontname', 'arial',...
                'stats_fontsize', 4,...
                'print_cox_stats', true,...
                'xlim',[0 120],...
                'xtick', 0:24:120,...
                'xlabel', xlab,...
                'ylabel', ylab,...
                'censoredmarkersize', 1,...
                'kmlinewidth', 1,...
                'has_legend', true,...
                'tick_dir', 'out',...
                'groups', groups,...
                'colors', colors.values(categories(groups)),...
                varargin{:}); 
        end
    end
end
