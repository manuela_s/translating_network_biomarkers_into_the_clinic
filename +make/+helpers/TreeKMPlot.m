function TreeKMPlot(p, j, technique, title_str, figure_label, dfs_panel, os_panel)
% Make Kaplan-Meier plots for predictions made with 'run.TreePredictions'.
% Input arguments:
% - p: instance of panel class, panel to plot in;
% - j: table with clinical data. One row per patient. Required columns:
%   - universal_patient_id;
%   - disease_free_survival;
%   - is_censored_disease_free_survival;
%   - overall_survival_months;
%   - is_censored_overall_survival;
% - technique: string, name of technique used for protein measurements.
%   One of 'rppa', 'tma' or 'rppa_and_tma';
% - title_str: string, title for the panel;
% - figure_label: string, label used by 'ExportForCox' method from 'KMPair' class
%   to cross-reference this figure in Cox model stats in table S2;
% - dfs_label: char, label used by 'ExportForCox' method from 'KMPair' class
%   to cross-reference the DFS panel in Cox model stats in table S2;
% - os_label: char, label used by 'ExportForCox' method from 'KMPair' class
%   to cross-reference the DFS panel in Cox model stats in table S2.

predictions = load(fullfile('outputs', 'tree_predictions.mat'));
predictions = predictions.(technique);
j2 = innerjoin(j, predictions,...
    'keys', 'universal_patient_id',...
    'RightVariables', 'substrate_cleavage_class');

km = make.helpers.KMPair(j2, 'substrate_cleavage_class');
km.VerticalLayout(p);
title(p, title_str);

km.ExportForCox(figure_label, dfs_panel, os_panel);
end
