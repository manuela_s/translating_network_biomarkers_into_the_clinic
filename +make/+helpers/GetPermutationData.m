function combos = GetPermutationData()
% Calculate aggregate statistics for measuring different combination of proteins.
% Output:
% - combos: table with one row per possible combination of proteins. Columns:
%   - PC3,PC9,XIAP,APAF1,SMAC: true if the protein is being measured.
%   - fraction_...: statistics for reduced apoptocell where we decide SC class
%     based on the majority vote for all possible simulations with the
%     non-measured proteins.
%   - label: string, label for this combination of proteins.

% Read APOPTO-CELL simulation results obtained by permuting protein input values.
data = load(fullfile('outputs', 'virtual_patients_cohort_simulations.mat'));

sc_class = data.substrate_cleavage_end > 25;

%Generate the full set of bars 2^5
%Generate a bitmask wih 5 proteins that can be on and off
[caspase3,caspase9,xiap,apaf1,smac] = ndgrid([true false],[true false], [true false], [true false], [true false]);
combos=array2table([caspase3(:),caspase9(:),xiap(:),apaf1(:),smac(:)],...
    'VariableNames', {'PC3', 'PC9', 'XIAP', 'APAF1', 'SMAC'});

combos.fraction_matches_full_apoptocell = nan(height(combos), 1);
combos.fraction_not_confident = nan(height(combos), 1);
combos.fraction_confident_matches_full_apoptocell = nan(height(combos), 1);
combos.fraction_confident_matches_full_apoptocell_high_sc = nan(height(combos), 1);
combos.fraction_confident_matches_full_apoptocell_low_sc = nan(height(combos), 1);
combos.fraction_confident_doesnt_match_full_apoptocell_high_sc = nan(height(combos), 1);
combos.fraction_confident_doesnt_match_full_apoptocell_low_sc = nan(height(combos), 1);

combos.label = repmat({''}, height(combos), 1);
confidence = 0.75;
for i = 1:height(combos)
    bits=combos{i,1:5};
    % Reorder the dimensions of sc_class so that the dimensions
    % representing known proteins come first.
    sc_class_permuted = permute(sc_class, [find(bits) find(~bits)]);
    % Find shape for new array, with 1 dimension for known proteins and one
    % dimension for unknown proteins
    sc_shape = data.steps.^[sum(bits) sum(~bits)];
    % For each possible value of the combination of known proteins: average
    % sc_class for matching simulations. If this is >0.5, we assume high sc
    % for the whole group. If it's < 0.5, we assume low sc for the whole
    % group. Fraction matching full apoptocell becomes 0.5 + abs distance
    % from 0.5.
    sc_xxxxx = mean(reshape(sc_class_permuted, sc_shape), 2);
    combos.fraction_matches_full_apoptocell(i) = mean(0.5 + abs(0.5 - sc_xxxxx));
    combos.fraction_confident_matches_full_apoptocell_high_sc(i) =...
        sum(sc_xxxxx(sc_xxxxx > confidence)) / numel(sc_xxxxx);
    combos.fraction_confident_matches_full_apoptocell_low_sc(i) =...
        sum(1 - sc_xxxxx(sc_xxxxx < (1 - confidence))) / numel(sc_xxxxx);
    combos.fraction_confident_doesnt_match_full_apoptocell_high_sc(i) =...
        sum(1 - sc_xxxxx(sc_xxxxx > confidence)) / numel(sc_xxxxx);
    combos.fraction_confident_doesnt_match_full_apoptocell_low_sc(i) =...
        sum(sc_xxxxx(sc_xxxxx < (1 - confidence))) / numel(sc_xxxxx);
    % What groups of simulations agree with full apoptocell for more than
    % confidence fraction of simulations:
    confident = 0.5 + abs(0.5 - sc_xxxxx) > confidence;
    combos.fraction_not_confident(i) = mean(~confident);
    combos.label{i} = strjoin(combos.Properties.VariableNames(bits==1), ' + ');
end

combos.fraction_confident_matches_full_apoptocell =...
    combos.fraction_confident_matches_full_apoptocell_high_sc +...
    combos.fraction_confident_matches_full_apoptocell_low_sc;

combos = sortrows(combos, 'fraction_confident_matches_full_apoptocell', 'descend');
end
