function ApoptoCellTreeGraphvizExport(tree, fig_name, color)
% Run Graphviz Export of APOPTO-CELL decision trees.
% Input arguments:
% - tree: instance of 'decision_tree.Tree' class, tree to export;
% - fig_name: string, figure name to use in filename;
% - color: double vector, RGB color for tree branches.

tree.ExportGraphviz(...
    fullfile('figures_and_tables', sprintf('%s.svg', fig_name)),...
    'custom_node_option_cb', @NodeOptions,...
    'color', strcat('#', sprintf('%2X', round(color.*255))));
end

function options = NodeOptions(node)
% Callback function to set custom node options.
% Sets background color on leaf decision nodes according to class and
% confidence.
% Input argument:
% - node: instance of 'decision_tree.Node' class, tree node to set options for;
% Output:
% - options: string, graphviz options for the given node.

if isa(node, 'decision_tree.LeafDecisionNode')
    switch node.predicted_class
        case 'SC<=25%'
            % Yellow (#EFC000) for confidence=1, white (#FFFFFF) for confidence=0.5
            fillcolor = round(interp1([1; 0.5],...
                [hex2dec('EF'), hex2dec('C0'), hex2dec('00');...
                hex2dec('FF'), hex2dec('FF'), hex2dec('FF')],...
                node.predicted_confidence));
        case 'SC>25%'
            % Blue (#0073C2) for confidence=1, white (#FFFFFF) for confidence=0.5
            fillcolor = round(interp1([1; 0.5],...
                [hex2dec('00'), hex2dec('73'), hex2dec('C2');...
                hex2dec('FF'), hex2dec('FF'), hex2dec('FF')],...
                node.predicted_confidence));
        otherwise
            error('Unknown predicted_class: %s', node.predicted_class);
    end
    if mean(fillcolor) < (256/2)
        % Dark fillcolor, bright text
        fontcolor = 'white';
    else
        fontcolor = 'black';
    end
    % Override label to format <=
    label = regexprep(strrep(strrep(node.GetLabel(), '<=', '&le;'), '>', '&gt;'), '\n', '<BR/>');
    options = sprintf(['fontcolor=%s,style="rounded,filled",fillcolor="%s",'...
        'color=black,margin="0.03,0.03",label=<%s>'],...
        fontcolor, strcat('#', sprintf('%.2X',fillcolor)), label);
else
    options = '';
end
end
