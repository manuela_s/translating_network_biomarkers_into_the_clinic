function pvalue_string = FormatPValue(pvalue, varargin)
% Format pvalue with appropriate number of digits.
% Input arguments:
% - pvalue: pvalue (double);
% - key/value pairs:
%   - pvalue_suffix: a suffix to add to the p-value string;
% Output:
% - pvalue_string: pvalue formatted as string.

ip = inputParser();
ip.addParameter('pvalue_suffix', '', @ischar);
ip.parse(varargin{:});

if pvalue < 0.0001
    pvalue_string = '<0.0001';
elseif pvalue < 0.001
    pvalue_string = sprintf('=%.4f', pvalue);
elseif pvalue < 0.01
    pvalue_string = sprintf('=%.3f', pvalue);
elseif strcmp(sprintf('%.3f', pvalue), '0.050')
    pvalue_string = sprintf('=%.4f', pvalue);
elseif strcmp(sprintf('%.2f', pvalue), '0.05')
    pvalue_string = sprintf('=%.3f', pvalue);
else 
    pvalue_string = sprintf('=%.2f', pvalue);
end

if ~isempty(ip.Results.pvalue_suffix)
    pvalue_string = sprintf('p_{%s}%s', ip.Results.pvalue_suffix, pvalue_string);
else
    pvalue_string = sprintf('p%s', pvalue_string);
end
end
