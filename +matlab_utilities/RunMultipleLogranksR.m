function pvalues_tab = RunMultipleLogranksR(sample_set, survival, has_event, grouping)
% Compute multiple logrank pvalues, one per sample_set.
% Input arguments:
% - sample_set: categorical column vector, identifier for each logrank pvalue
%   to compute;
% - survival: column vector, time;
% - has_event: logical column vector, true if event has occured, false if 
%   event has not occurred;
% - grouping: categorical column vector, group label for each patient;
% Output:
% - pvalues_tab: table, one row per sample_set with columns: 'sample_set'
%   and 'logrank_p'.

t = table(sample_set, survival, has_event, grouping);
input_filename = [tempname(), '.csv'];
writetable(t, input_filename);
output_filename = [tempname(), '.csv'];
R_script_name = fullfile(fileparts(mfilename('fullpath')), 'R_helpers',...
    'RunMultipleLogranks.R');
% Rscript must be in PATH (enviromental variable set)
cmd_line = sprintf('Rscript %s --input_filename %s --output_filename %s',...
    R_script_name, input_filename, output_filename);
[status, output] = system(cmd_line);

if status == 0
    pvalues_tab = readtable(output_filename);
    if numel(output)
        warning('RunMultipleLogranksR:RWarning', '%s', output);
    end
else
    error('error occured while running RunMultipleLogranks.R: %s', output);
end

delete(input_filename);
delete(output_filename)
end
