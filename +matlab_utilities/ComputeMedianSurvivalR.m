function tab_out = ComputeMedianSurvivalR(sample_set, survival, has_event)
% Computes median survival time with 95% confidence interval.
% Input arguments:
% - sample_set: vector with grouping variable;
% - survival: vector of known survival times;
% - has_event: vector with 1 when event has occured and 0 for censored;
% Output:
% - tab_out: table with as many rows as levels in sample_set and columns:
%   - sample_set: grouping variable
%   - median_survival: scalar, median survival;
%   - low_ci: scalar, lower 95% confidence interval;
%   - high_ci: scalar, higher 95% confidence interval.

t = table(sample_set, survival, has_event);
input_filename = [tempname(), '.csv'];
output_filename = [tempname(), '.csv'];
writetable(t, input_filename);
R_script_name = fullfile(fileparts(mfilename('fullpath')), 'R_helpers', 'ComputeMedianSurvival.R');
% Rscript must be in PATH (enviromental variable set)
cmd_line = sprintf('Rscript %s -i %s -o %s', R_script_name, input_filename, output_filename);
[status, output] = system(cmd_line);
if status == 0
    tab_out = readtable(output_filename, 'TreatAsEmpty', 'NA');
else
    error('error occured while running ComputeMedianSurvival.R: %s', output);
end
delete(input_filename);
delete(output_filename);
end
