function legend_handles = MultiLegendGScatter(x, y, varargin)
%% Make gscatter-like plot, grouped by multiple dimensions with separate
% legends for each grouping variable
% Input arguments:
% - x: numeric column vector, x coordinates for data-points;
% - y: numeric column vector, y coordinates for data-points;
% - key/value:
%   - marker_size: scalar, marker size;
%   - color_grouping: categorical, grouping variable for coloring the data-points;
%   - color_grouping_name: char, description for the color_grouping. Used as
%     title in the legend for color_grouping;
%   - colormap: cell array, one cell for each color group;
%   - symbol_grouping: categorical, grouping variable for selecting symbols
%     for the data-points;
%   - symbol_grouping_name: char, description for the symbol_grouping. Used
%     as title in the legend for symbol_grouping;
%   - size_grouping: categorical, grouping variable for selecting sizes for
%     the data-points;
%   - size_grouping_name: char, description for the size_grouping. Used as
%     title in the legend for size_grouping;
%   - filled_grouping: categorical, grouping variable for selecting filling
%     for the data-points;
%   - filled_grouping_name: char, description for the filled_grouping. Used
%     as title in the legend for filled_grouping;
%   - filled_colors: cell array, fill color for each filling group. If fill
%     color is 'k', the marker is filled with the color from the color
%     grouping;
%   - has_fitting_line: logical, draw fitting (least-square regression) line;
%   - fitting_line_style: style of fitting line, see LineSpec;
%   - fitting_line_color: color of fitting line, see LineSpec;
%   - fitting_line_width: width of fitting line, see LineSpec.

% Example of use:
% MultiLegendGScatter(t.x, t.y,...
%     'color_grouping', t.cell_line, 'symbol_grouping', t.treatment)

ip = inputParser();
ip.addParameter('marker_size', 10, @isnumeric); 
ip.addParameter('color_grouping', [], @iscategorical);
ip.addParameter('color_grouping_name', '', @ischar);
ip.addParameter('colormap', [], @iscell);
ip.addParameter('symbol_grouping', [], @iscategorical);
ip.addParameter('symbol_grouping_name', '', @ischar);
ip.addParameter('size_grouping', [], @iscategorical);
ip.addParameter('size_grouping_name', '', @ischar);
ip.addParameter('filled_grouping', [], @iscategorical);
ip.addParameter('filled_grouping_name', '', @ischar);
ip.addParameter('filled_colors', {'none'; 'k'}, @iscell);
ip.addParameter('has_fitting_line', false, @islogical);
ip.addParameter('fitting_line_style', '--');
ip.addParameter('fitting_line_color', 'k');
ip.addParameter('fitting_line_width', 1);
ip.parse(varargin{:});

legend_handles = [];

data = table(x, y);
data.lines = rowfun(@line, data, 'InputVariables', {'x', 'y'}, 'OutputFormat', 'uniform');
set(data.lines, 'Marker', 's', 'MarkerSize', ip.Results.marker_size, 'parent', hggroup());

if ~isempty(ip.Results.color_grouping)
    if ~isempty(ip.Results.colormap)
        cmap = ip.Results.colormap;
    elseif numel(ip.Results.color_grouping) <= 9
        cmap = num2cell(...
            matlab_utilities.brewermap.brewermap(...
            numel(categories(ip.Results.color_grouping)), 'Set1'), 2);
    else
        cmap = num2cell(...
            matlab_utilities.brewermap.brewermap(...
            numel(categories(ip.Results.color_grouping)), 'Set3'), 2);
    end
    legend_handles(end+1) = CreateGroups(...
        data.lines,...
        ip.Results.color_grouping,...
        'Color', cmap,...
        ip.Results.color_grouping_name,...
        'Marker', 's');
end
if ~isempty(ip.Results.symbol_grouping)
    symbols = {'s';'v';'d';'h';'^';'>';'<';'x'};
    symbols = symbols(1:numel(categories(ip.Results.symbol_grouping)));
    legend_handles(end+1) = CreateGroups(...
        data.lines,...
        ip.Results.symbol_grouping,...
        'Marker', symbols,...
        ip.Results.symbol_grouping_name,...
        'Color', 'k');
end
if ~isempty(ip.Results.size_grouping)
    % Pick marker size between 6 and 16 for size_grouping
    sizes = num2cell(linspace(6, 16, numel(categories(ip.Results.size_grouping))))';
    legend_handles(end+1) = CreateGroups(...
        data.lines,...
        ip.Results.size_grouping,...
        'MarkerSize', sizes,...
        ip.Results.size_grouping_name,...
        'Marker', 's',...
        'Color', 'k');
end

if ~isempty(ip.Results.filled_grouping)
    %MarkerFaceColor needs to be set to their respective color for each
    %marker. We first set all the filled ones to 'k' and then replace
    %them with the correct color. 
    legend_handles(end+1) = CreateGroups(...
        data.lines,...
        ip.Results.filled_grouping,...
        'MarkerFaceColor', ip.Results.filled_colors,...
        ip.Results.filled_grouping_name,...
        'Marker', 's',...
        'Color', 'k');
    to_be_filled = findobj(data.lines, 'MarkerFaceColor', 'k');
    set(to_be_filled, {'MarkerFaceColor'}, get(to_be_filled, {'color'}));
end

if ip.Results.has_fitting_line
    CreateFittingLine(x, y, ip);
end

end

function CreateFittingLine(x, y, ip)
%% Draw fitting line and add correlation information.
% Input arguments:
% - x: numeric column vector, x coordinates for data-points;
% - y: numeric column vector, y coordinates for data-points;
% - ip: instance of inputParser, additional arguments to customize line.

[r_p, p_p] = corr(x, y, 'rows', 'complete');
[r_s, p_s] = corr(x, y, 'rows', 'complete', 'type', 'spearman');
% Work around to handle nans for polyfit
k =~(isnan(x) | isnan(y));
[p, S] = polyfit(x(k), y(k), 1);
f = polyval(p, x, S);
r_squared = corr(f, y, 'rows', 'complete') .^ 2;
hline = refline(p(1), p(2));
set(hline, 'linestyle', ip.Results.fitting_line_style,...
    'color', ip.Results.fitting_line_color,...
    'linewidth', ip.Results.fitting_line_width,...
    'displayname', sprintf('linear fit - R^2 = %.4f', r_squared));
text(0.01, 0.99,...
    sprintf('Pearson    r=%.2f %s\nSpearman r=%.2f %s',...
    r_p, matlab_utilities.FormatPValue(p_p),...
    r_s, matlab_utilities.FormatPValue(p_s)),...
    'Units', 'normalized', 'VerticalAlignment', 'top',...
    'tag', 'correlation_info');
end

function legend_handle = CreateGroups(lines, grouping, attribute_name, values, tl, varargin)
%% Helper function to create groups with separate legend
% Input arguments
% - lines: handles to all the lines in the plot;
% - grouping: categorical, group for each element;
% - attribute_name: attribute to change per-group in the lines;
% - values: cell array, one value to use for attribute_name for each group;
% - tl: char, title to use for legend box;
% - varargin: additional arguments to line.

assert(iscell(values) & size(values, 2) == 1, 'Values must be column cell array');
set(lines, {attribute_name}, values(grp2idx(grouping)));
% Create hidden lines to show in legend
legend_lines = cellfun(@(name, attribute_value) line(...
    nan, nan,...
    'LineStyle', 'none',...
    'DisplayName', name,...
    attribute_name, attribute_value,...
    varargin{:}),...
    categories(grouping), values, 'UniformOutput', false);
legend_handle = legend([legend_lines{:}]);
% Remove handle to legend from appdata, so that subsequent legend-calls,
% do not replace this legend box.
rmappdata(gca, 'LegendPeerHandle');
set(legend_handle,...
    'interpreter', 'none',...
    'fontname', 'arial',...
    'fontsize', 8,...
    'tag', 'MultiLegendGScatter');

text('Parent',...
    get(legend_handle, 'DecorationContainer'),...
    'String', tl,...
    'Position', [0.5 1.05, 0],...
    'Units', 'Normalized',...
    'HorizontalAlignment', 'center',...
    'VerticalAlignment', 'bottom',...
    'fontname', 'arial', 'fontsize', 8);
end
