function t = Combat(t, batch_column, correct_columns, covariate_columns)
% Run combat batch correction
% Inputs:
% - t: table with input data. 
% - batch_column: the name of the column in t with the batch information.
% - correct_columns: column identifiers for the columns with data to do
%   batch correction on.
% - covariate_columns: column names for covariate columns
% Outputs:
% - t: table, copy of input variable t, but batch corrected.

extra_args = {};
if nargin > 3
    factors = cellfun(@(c) iscategorical(t.(c)), covariate_columns);
    if any(factors)
        extra_args{end+1} = '--factor_covariate_columns';
        extra_args{end+1} = strjoin(covariate_columns(factors), ',');
    end
    if ~all(factors)
        extra_args{end+1} = '--covariate_columns';
        extra_args{end+1} = strjoin(covariate_columns(~factors), ',');
    end 
else
    covariate_columns = {};
end

edata_filename = [tempname(), '.csv'];
batch_filename = [tempname(), '.csv'];
output_filename = [tempname(), '.csv'];
metadata_columns = horzcat({batch_column}, covariate_columns);
writetable(t(:, metadata_columns), batch_filename);
csvwrite(edata_filename, t{:, correct_columns}');
R_script_name = fullfile(fileparts(mfilename('fullpath')), 'R_helpers', 'RunCombatR.R');
% Rscript must be in PATH (enviromental variable set)
cmd_line = sprintf('Rscript %s --edata_file %s --batch_file %s --batch_column %s --output_file %s %s',...
    R_script_name, edata_filename, batch_filename, batch_column, output_filename,...
    strjoin(extra_args, ' '));
[status, output] = system(cmd_line);
delete(edata_filename);
delete(batch_filename);
if status ~= 0
    error('error occured while running RunCombatR.R: %s', output);
end
combat_edata = csvread(output_filename);
delete(output_filename);
t{:, correct_columns} = combat_edata';
end
