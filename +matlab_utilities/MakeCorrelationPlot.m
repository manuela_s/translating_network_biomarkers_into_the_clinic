function MakeCorrelationPlot(xdata, ydata, varargin)
%% Make a plot of xdata against ydata and add correlation information.
% Input arguments:
% - xdata: numeric array, x coordinates for data-points;
% - ydata: numeric array, y coordinates for data-points;
% - key/value:
%   - xdata_stds: numeric array with the same shape as xdata, standard deviation for xdata;
%   - ydata_stds: numeric array with the same shape as ydata, standard deviation for ydata;
%   - labels: cell array, labels for data-points;
%   - xlab: character array, label for x axis;
%   - ylab: character array, label for y axis;
%   - marker_size: scalar, size of the marker;
%   - markerfacecolor: color for the marker face, see LineSpec;
%   - markeredgecolor: color for the marker edge, see LineSpec;
%   - linewidth: scalar, line width;
%   - has_identity_line: logical, draw identity line;
%   - identity_line_style: style of identity line, see LineSpec;
%   - identity_line_color: color of identity line, see LineSpec;
%   - identity_line_width: width of identity line, see LineSpec;
%   - has_fitting_line: logical, draw fitting (least-square regression) line;
%   - fitting_line_style: style of fitting line, see LineSpec;
%   - fitting_line_color: color of fitting line, see LineSpec;
%   - fitting_line_width: width of fitting line, see LineSpec.

parser = inputParser();
parser.addOptional('xdata_stds', zeros(size(xdata)));
parser.addOptional('ydata_stds', zeros(size(ydata)));
parser.addOptional('labels', {});
parser.addOptional('xlab', '');
parser.addOptional('ylab', '');
parser.addOptional('marker_size', 20);
parser.addOptional('markerfacecolor', [0.75 0.75 0.75]);
parser.addOptional('markeredgecolor', 'k');
parser.addOptional('linewidth', 1);
parser.addOptional('has_identity_line', false);
parser.addOptional('identity_line_style', '-');
parser.addOptional('identity_line_color', 'k');
parser.addOptional('identity_line_width', 1);
parser.addOptional('has_fitting_line', true);
parser.addOptional('fitting_line_style', '--');
parser.addOptional('fitting_line_color', 'k');
parser.addOptional('fitting_line_width', 1);
parser.parse(varargin{:});

% Plot data
hx = matlab_utilities.herrorbar(...
    xdata, ydata, parser.Results.xdata_stds,...
    'ko');
hg = hggroup();
set(hx,...
    'markersize', parser.Results.marker_size,...
    'markerfacecolor', parser.Results.markerfacecolor,...
    'markeredgecolor', parser.Results.markeredgecolor,...
    'linewidth', parser.Results.linewidth, 'parent', hg);
hold on;
errorbar(xdata, ydata, parser.Results.ydata_stds,...
    'ko',...
    'markersize', parser.Results.marker_size,...
    'markerfacecolor', parser.Results.markerfacecolor,...
    'markeredgecolor', parser.Results.markeredgecolor,...
    'linewidth', parser.Results.linewidth, 'parent', hg);
hAnnotation = get(hg, 'Annotation');
hLegendEntry = get(hAnnotation', 'LegendInformation');
set(hLegendEntry, 'IconDisplayStyle', 'off')

if parser.Results.has_identity_line
    href = refline(1, 0);
    set(href,...
        'linestyle', parser.Results.identity_line_style,...
        'color', parser.Results.identity_line_color,...
        'linewidth', parser.Results.identity_line_width,...
        'displayname', 'identity line');
end

if parser.Results.has_fitting_line
    % Work around to handle nans for polyfit
    k =~(isnan(xdata) | isnan(ydata));
    [p, S] = polyfit(xdata(k), ydata(k), 1);
    f = polyval(p, xdata, S);
    r_squared = corr(f, ydata, 'rows', 'complete') .^ 2;
    hline = refline(p(1), p(2));
    set(hline,...
        'linestyle', parser.Results.fitting_line_style,...
        'color', parser.Results.fitting_line_color,...
        'linewidth', parser.Results.fitting_line_width,...
        'displayname', sprintf('linear fit - R^2 = %.4f', r_squared));
    uistack(hline, 'bottom');
    legend('show');
end

% Add data-point labels
for i = 1:numel(parser.Results.labels)
    text(xdata(i), ydata(i), parser.Results.labels{i},...
        'VerticalAlignment', 'bottom', 'interpreter', 'none');
end

% Compute correlations and corresponding p-values
[r_p, p_p] = corr(xdata, ydata, 'rows', 'complete');
[r_s, p_s] = corr(xdata, ydata, 'rows', 'complete', 'type', 'spearman');
text(0.01, 0.99,...
    sprintf('Pearson    r = %.2f %s\nSpearman r = %.2f %s',...
    r_p, matlab_utilities.FormatPValue(p_p), r_s, matlab_utilities.FormatPValue(p_s)),...
    'Units', 'normalized',...
    'VerticalAlignment', 'top',...
    'tag', 'correlation_info');

xlabel(parser.Results.xlab);
ylabel(parser.Results.ylab);
set(gca,...
    'box', 'on',...
    'linewidth', parser.Results.linewidth,...
    'xcolor', 'k', 'ycolor', 'k');
end
