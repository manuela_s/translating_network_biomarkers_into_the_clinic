function CombineLegends(p, legend_handles, varargin)
% Combine legends from multiple subplots into a single shared panel.
% Input arguments:
% - p: panel to put legends in;
% - legend_handles: cell array with one cell per subplot. Each cell can have
%   an array of handles;
% - key/value:
%   - create_subpanels: logical, create separate sub-panels inside p for
%     legend(s);
%   - compare_legends: logical, validate that legends being combines are
%     identical.

ip = inputParser();
ip.addParameter('create_subpanels', true, @islogical);
ip.addParameter('compare_legends', true, @islogical);
ip.parse(varargin{:});

% Size/Position of legends for first subplot in cm:
old_units = get(legend_handles{1}, 'Units');
set(legend_handles{1}, 'Units', 'centimeters');
legend_pos = get(legend_handles{1}, 'Position');
if numel(legend_handles{1}) == 1
    set(legend_handles{1}, 'Units', old_units);
else
    set(legend_handles{1}, {'Units'}, old_units);
end

if iscell(legend_pos)
    % If the first subplot has multiple legend handles, get returns a cell
    % array
    legend_pos = cell2mat(legend_pos);
end

% Create one panel per legend plus a stretchable panel on top and one on the bottom
if ip.Results.create_subpanels
    p.pack('v', [{[]}, num2cell(num2cell(10*legend_pos(:, 4))'), {[]}]);
    for i = 1:numel(legend_handles{1})
        p(i+1).select(legend_handles{1}(i));
    end
else
    assert(numel(legend_handles{1}) == 1,...
        'Can not handle multiple legends per subplot without create_subpanels');
    p.select(legend_handles{1});
end

% Delete legends from remaining subplots:
for i = 2:numel(legend_handles)
    for j = 1:numel(legend_handles{1})
        % Check that legends being deleted are identical to the ones in
        % legend_handles{1}
        if ip.Results.compare_legends
            CompareLegends(handle(legend_handles{1}(j)),...
                handle(legend_handles{i}(j)));
        end
        delete(legend_handles{i}(j));
    end
end
end

function CompareLegends(l1, l2)
% Assert that 2 legends are identical.
% Input arguments:
% - l1: handle for first legend;
% - l2: handle for second legend.

l1_entries = l1.EntryContainer.Children;
l2_entries = l2.EntryContainer.Children;
assert(numel(l1_entries) == numel(l2_entries));
for i = 1:numel(l1_entries)
    CompareLegendEntries(l1_entries(i), l2_entries(i));
end
end

function CompareLegendEntries(le1, le2)
% Assert that 2 legend entries are identical.
% Input arguments:
% - le1: handle to first legend entry;
% - le2: handle for second legend entry.

assert(isequal(le1.Label.String, le2.Label.String));
if isa(le1.Object, 'matlab.graphics.chart.primitive.Histogram')
    assert(isequal(le1.Object.FaceColor, le2.Object.FaceColor));
    assert(isequal(le1.Object.EdgeColor, le2.Object.EdgeColor));
elseif isa(le1.Object, 'matlab.graphics.chart.primitive.Bar')
    assert(isequal(le1.Object.FaceColor, le2.Object.FaceColor));
    assert(isequal(le1.Object.EdgeColor, le2.Object.EdgeColor));
else
    assert(isequal(le1.Object.Color, le2.Object.Color));
    assert(isequal(le1.Object.LineStyle, le2.Object.LineStyle));
    assert(isequal(le1.Object.Marker, le2.Object.Marker));
    assert(isequal(le1.Object.MarkerFaceColor, le2.Object.MarkerFaceColor));
    assert(isequal(le1.Object.MarkerEdgeColor, le2.Object.MarkerEdgeColor));
end
end
