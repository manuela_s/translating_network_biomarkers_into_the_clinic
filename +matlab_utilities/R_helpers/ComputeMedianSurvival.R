suppressPackageStartupMessages(library(optparse))
suppressPackageStartupMessages(library(survival))

option_list <- list(
  make_option(c("-i", "--input_file"),
              help=paste("Name of csv file to read survival data from.",
                         "The csv file should have 3 columns: sample_set, survival and has_event"),
              dest="input_filename"),
  make_option(c("-o", "--output_file"),
              help=paste("Name of csv file to save results to.",
                         "The csv file should have 3 columns: sample_set, median survival, 2.5% CI and 95.5% CI"),
              dest="output_filename"))
opt <- parse_args(OptionParser(option_list=option_list))
if (is.null(opt$input_filename)) {
  stop("No input filename specified")
} else if (is.null(opt$output_filename)) {
  stop("No output filename specified")
}

data = read.csv(opt$input_filename, header = TRUE, sep = ",")
# Check input args
if (is.null(data$sample_set)) {
  stop("No sample_set column found in csv file")
} else if (is.null(data$survival)) {
  stop("No survival column found in csv file")
} else if (is.null(data$has_event)) {
  stop("No has_event column found in csv file")
} 

ComputeMedianSurvival <- function(tab) {
  # Calculate median survival and 95%CI with multiple sets of samples.
  # Input arguments:
  #  tab: table with one row per patient per model columns:
  #   - sample_set: identifier for the sample set (factor). Median and 95% 
  #     CI will be computed for each level of sample set;
  #   - survival: survival times (numeric);
  #   - has_event: status indicator (logical, 0 if censored / 1 if event occurred);
  # Returns:
  #  data frame with one row for each sample_set and columns: sample_set,
  #  median_survival, lowCI, highCi

  iterations = levels(as.factor(tab$sample_set))
  iterations_n <- length(iterations)
  tab_out <- data.frame(sample_set = iterations,
                        median_survival = numeric(iterations_n),
                        low_ci = numeric(iterations_n),
                        high_ci = numeric(iterations_n))
  for (i in 1:iterations_n) {
    sub_tab <- tab[tab$sample_set == iterations[i],]
    ### Median survival (from http://stats.stackexchange.com/questions/19005/finding-median-survival-time-from-survival-function)
    surv_fit_obj <- survfit(Surv(survival,has_event)~1,
                           type = "kaplan-meier",
                           error = "greenwood",
                           conf.type = "log-log",
                           data = sub_tab)
    stats <- read.table(textConnection(capture.output(surv_fit_obj)), skip = 2, header = TRUE)
    tab_out$median_survival[i] <- stats$median
    tab_out$low_ci[i] <- stats$X0.95LCL
    tab_out$high_ci[i] <- stats$X0.95UCL
  }
  return(tab_out)
}

tab_out <- ComputeMedianSurvival(data)

write.csv(tab_out, opt$output_filename, row.names=FALSE)
