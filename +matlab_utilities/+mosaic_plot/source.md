Downloaded on 2015-11-18 from [MATLAB file exchange](http://www.mathworks.com/matlabcentral/fileexchange/47785-mosaic-plot-zip/content/mosaic_plot/mosaic_plot.m), with the following local modifications:
   * Changed to have optional input arguments and to use patch;
   * Added option to choose colors;
   * Added formatting for text.