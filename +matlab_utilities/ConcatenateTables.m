function tables = ConcatenateTables(tables, varargin)
% Concatenates tables vertically and pads columns if missing.
% Input arguments:
% - tables: cell array of tables to concatenate;
% - key/value pairs:
%   - keep_columns: cell array, if set, only keep columns with matching names;
%   - new_column: string, if set, add a new column which identifies the source 
%     table for each row. See new_column_values;
%   - new_column_values: cell array, if set, source name for each input table.
%     See new_column;
% Output:
% - tables: single table with rows from all input tables.

p = inputParser();
p.addParameter('keep_columns', {}, @iscell);
p.addParameter('new_column', '', @ischar);
p.addParameter('new_column_values', {}, @iscell);
p.parse(varargin{:});

if isempty(p.Results.new_column) ~= isempty(p.Results.new_column_values)
   error('Need to specify both new_column and new_column_values or neither'); 
end
if ~isempty(p.Results.new_column_values) &&...
        numel(p.Results.new_column_values) ~= numel(tables)
   error('Need one value in new_column_values for each input table'); 
end

if isempty(p.Results.keep_columns)
    columns = {};
    for i = 1:numel(tables)
        columns = union(columns, tables{i}.Properties.VariableNames, 'stable');
    end
else
    columns = p.Results.keep_columns;
end

% For every column, get a default value to use for tables that are
% missing the column. Use NaN by default, or '' if any of the tables
% have the column as a cell array or undefined if any of the tables have
% the column as a categorical.
default_values = num2cell(nan(size(columns)));
for i = 1:numel(columns)
    types = cellfun(@(t) GetColumnType(t, columns{i}), tables, 'UniformOutput', false);
    if ismember('categorical', types)
        default_values{i} = categorical({''});
    elseif ismember('cell', types)
        default_values{i} = {''};
    end 
end

tables = cellfun(@(t) ConvertTable(t, columns, default_values), tables,...
    'UniformOutput', false);
% Add new column
if ~isempty(p.Results.new_column)
    for i=1:numel(tables)
        tables{i}.(p.Results.new_column) = categorical(...
            repmat(p.Results.new_column_values(i), height(tables{i}), 1));
    end
end

tables = vertcat(tables{:});

end

function type = GetColumnType(t, column_name)
% Get data-type for a column in a table (or '' for missing column).
% Input arguments:
% - t: table, table to assess column type in;
% - column_name: string, name of the column to assess;
% Output:
% - type: string, the name of the class of the column, or '' if the column
%   is missing.

if ~ismember(column_name, t.Properties.VariableNames)
    type = '';
else
    type = class(t.(column_name));
end
end

function new_t = ConvertTable(t, columns, default_values)
% Convert table to have specific set of columns and types.
% Input arguments:
% - t: table, input table;
% - columns: cell array, names of columns to add or change types for;
% - default_values: cell array, default values corresponding to each column
%   in columns. If the column is missing, it is added with this value. If
%   the column is present, its type is forced to be the same as of the
%   default value;
% Output:
% - new_t: table, output table after adding columns / changing types.

new_t = table();
for i=1:numel(columns)
    default_value = default_values{i};
    
    if ismember(columns{i}, t.Properties.VariableNames)
        value = t.(columns{i});
        if iscategorical(default_value)
            % Force to categorical
            value = categorical(value);
        elseif iscell(default_value)
            % Force to cell
            if isnumeric(value)
                value = arrayfun(@num2str, value, 'UniformOutput', false);
            end
        end
    else
        value = repmat(default_value, height(t), 1);
    end
    new_t.(columns{i}) = value;
end
end
