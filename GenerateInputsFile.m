function GenerateInputsFile(varargin)
% Generate 'inputs.mat', required to run simulations.
% Input arguments:
% - key/value pair:
%   - steps: scalar, number of protein concentrations to use when running
%     ensembles of simulations;
%   - combo_steps: scalar, number of protein concentrations to use when
%     running ensemble of simulations for all permutations of known/unknown
%     APOPTO-CELL inputs ('run.RPPACombosEnsemblesSimulations').

ip = inputParser();
ip.addParameter('steps', 20, @isnumeric);
ip.addParameter('combo_steps', 10, @isnumeric);
ip.parse(varargin{:});

% Create folder to store individual simulation results
if exist('outputs', 'dir')==0
    mkdir('outputs');
end

steps = ip.Results.steps;
combo_steps = ip.Results.combo_steps;

percentiles = linspace(1/steps/2, 1-(1/steps/2), steps);
combo_percentiles = linspace(1/combo_steps/2, 1-(1/combo_steps/2), combo_steps);
proteins = ReferenceCohortHelper(percentiles, combo_percentiles); %#ok<NASGU>

abs_proteins = get.AbsProteinConcentrations();
abs_proteins = abs_proteins(ismember(abs_proteins.universal_patient_id,...
    get.PatientIDsMeetingClinicalInclusionCriteria()),:);
[rppa, tma, rppa_and_tma] =...
    get.ProteinsByTechnique(abs_proteins); %#ok<ASGLU>

% Subset TMA to include only patients that also have RPPA to make
% simulation results comparable
tma = tma(ismember(tma.universal_patient_id, rppa_and_tma.universal_patient_id), :); %#ok<NASGU>

save(fullfile('outputs', 'inputs.mat'),...
    'percentiles', 'combo_percentiles', 'proteins',...
    'rppa', 'tma', 'rppa_and_tma');
end

function proteins = ReferenceCohortHelper(percentiles, combo_percentiles)
% Get protein concentrations from CRC reference cohort (Hector et al., GUT, 2012).
% Input arguments:
% - percentiles: vector, percentiles to compute absolute protein
%   concentrations for ensemble simulations except 'run.RPPACombosEnsemblesSimulations';
% - combo_percentiles: vector, percentiles to compute absolute protein
%   concentrations for 'run.RPPACombosEnsemblesSimulations'.
% Output:
% - proteins: table, one row per protein. Columns include:
%   - protein: categorical, name of protein;
%   - nanmedian_concentration: scalar, median concentration for protein;
%   - iqr_concentration: scalar, iqr concentration for protein;
%   - concs: row vector, one protein concentration corresponding to
%     each percentile;
%   - combo_concs: row vector, one protein concentration corresponding to
%     each combo_percentile.

ref_proteins = cohort_utilities.reference_cohorts.CRC();
proteins = ref_proteins.GetProteinStats();

% Find the concentrations at the percentiles specified for each protein
proteins.concs = cell2mat(...
    cellfun(@(x) ref_proteins.GetProteinDistribution(x).icdf(percentiles),...
    cellstr(proteins.protein), 'uniformoutput', false));

% Find the concentrations at the combo_percentiles specified for each protein
proteins.combo_concs = cell2mat(...
    cellfun(@(x) ref_proteins.GetProteinDistribution(x).icdf(combo_percentiles),...
    cellstr(proteins.protein), 'uniformoutput', false));

end
