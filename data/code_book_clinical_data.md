# Code book for clinical_data.csv

- universal_patient_id: unique identifier for each patient;
- resection_margins: status of residual tumor, either
    - 'R0': tumor-free surgical margins;
    - 'not_R0': residual tumor present in surgical margins (either microscopic
      or macroscopic).
- chemotherapy: treatment administered to the patient, either
    - 'chemotherapy', if the patient was treated with 5-FU based chemotherapy;
    - 'no_chemotherapy', if the patient was not treated with 5-FU based chemotherapy;
- stage: colorectal cancer disease stage defined according to the 
  [tumour/node/metastasis (TNM) system](https://cancerstaging.org/references-tools/quickreferences/Documents/ColonMedium.pdf)
  by the American Joint Committee on Cancer (AJCC), either
    - '2', if T3/T4, N0, M0;
    - '3', if any T, N1/N2, M0;
    - '4', if any T, any N, M1;
- T: tumour extent staging according to the [TNM system](https://cancerstaging.org/references-tools/Pages/What-is-Cancer-Staging.aspx) by AJCC, either
    - '1';
    - '2';
    - '3';
    - '4';
    - empty, if information was not available;
- N: lymph nodes staging according to the [TNM system](https://cancerstaging.org/references-tools/Pages/What-is-Cancer-Staging.aspx) by AJCC
    - '0';
    - '1'; 
    - '2';
    - empty, if information was not available;
- M: metastasis staging according to the [TNM system](https://cancerstaging.org/references-tools/Pages/What-is-Cancer-Staging.aspx) by AJCC
    - '0': no metastasis;
    - '1': presence of metastasis; 
- age: patient age, in years;
- sex: patient sex, either
    - 'female';
    - 'male';
- tumour_site: location of primary tumor, either
    - ['distal'](http://www.ncbi.nlm.nih.gov/pubmed/25165475), if from
      'descending_colon', 'sigmoid_colon', 'sigmoid_colon';  
    - ['proximal'](http://www.ncbi.nlm.nih.gov/pubmed/25165475), if from 
      'ascending_colon', 'caecum', 'hepatic flexure', 'transverse colon',
      'splenic flexure';
    - ['rectal'](http://www.ncbi.nlm.nih.gov/pubmed/25165475), if from
      'rectum' or 'rectosigmoid';
    - 'appendix';
    - 'others';
- lymphovascular_invasion: presence of lymphatic invasion, either
    - 'invasion', if lymphovascular invasion was observed;
    - 'no_invasion', if lymphovascular invasion was not observed;
    - empty, if information was not available;
- disease_free_survival_months: time interval elapsed between surgical removal
  of the tumor and tumor relapse, loss to follow-up or study end-date, in months;
- is_censored_disease_free_survival: censoring,
    - 'yes', if patient did not relapse before being lost to follow-up or study
      end-date;
    - 'no', if patient had a documented relapse before being lost to follow-up
      or study end-date;
    - empty, if information was not available;
- overall_survival_months: time interval elapsed between surgical removal
  of the tumor and death from any cause, loss to follow-up or study end-date,
  in months;
- is_censored_overall_survival: censoring,
    - 'yes', if patient was alive before being lost to follow-up or study
      end-date;
    - 'no', if patient died from any cause before being lost to follow-up or
      study end-date;
    - empty, if information was not available;
- KRAS_status: KRAS mutational status assessed by Sequenom's MassARRAY system,
  either
    - 'wild-type', if no point mutation in KRAS was detected;
    - 'mutant', if a point mutation in KRAS was detected;
    - empty, if information was not available;
- KRAS_mutation_type: type of KRAS mutation, either
    - 'wild-type', if no point mutation in KRAS was detected;
    - 'A59T': if [amino acid substitution in position 59 from alanine A to
      threonine T was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=546);
    - 'G12A': if [amino acid substitution in position 12 from glycine G to
      alanine A was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=522);
    - 'G12C': if [amino acid substitution in position 12 from glycine G to
      cysteine C was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=516);
    - 'G12D': if [amino acid substitution in position 12 from glycine G to
      aspartate D was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=521);
    - 'G12R': if [amino acid substitution in position 12 from glycine G to
      arginine R was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=518);
    - 'G12S': if [amino acid substitution in position 12 from glycine G to
      serine S was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=517);
    - 'G12V': if [amino acid substitution in position 12 from glycine G to
      valine V was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=520);
    - 'G13D': if [amino acid substitution in position 13 from glycine G to
      aspartate D was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=532);
    - 'Q61H': if [amino acid substitution in position 61 from glutamine Q to
      histidine H was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=554);
    - 'Q61P': if [amino acid substitution in position 61 from glutamine Q to proline
      P was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=551);
    - 'Q61R': if [amino acid substitution in position 61 from glutamine Q to
      arginine R was detected](https://cancer.sanger.ac.uk/cosmic/mutation/overview?id=552);
    - empty, if information was not available.

For more information, refer to the manuscript.
