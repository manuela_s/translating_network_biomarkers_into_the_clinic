# Code book for rppa_data.csv

Dataset with protein expression measured by Reverse Phase Protein Array (RPPA)
in tumor resections from colorectal cancer patients.

The dataset is organised in a long-table format with one row per patient/protein
measured and columns:

  - universal_patient_id: unique identifier for each patient;
  - protein: name of the protein measured from
    - 'caspase3';
    - 'caspase9';
    - 'smac';
    - 'xiap';
  - concentration: concentration measured by RPPA for the protein in 'protein'.
    Protein concentrations are normalized and expressed in arbitrary units.
    Preparation of tumor samples for RPPA measurements, experimental protocol
    and data processing and normalization have been previously described in
    detail in Supplementary Methods 2 of [A Stepwise Integrated Approach to
    Personalized Risk Predictions in Stage III Colorectal Cancer (Salvucci et
    al., Clin Cancer Res., 2016)](https://www.ncbi.nlm.nih.gov/pubmed/27649552).

For more information, refer to the manuscript.
