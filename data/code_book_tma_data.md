# Code book for tma_data.csv

Dataset with protein expression measured by tissue microarray (TMA) immunohistochemistry (TMA)
in tumor resections from colorectal cancer patients. 

The dataset is organised in a long-table format with one row per patient/core/protein
measured and columns:

  - universal_patient_id: unique identifier for each patient;
  - sub_cohort: identifier for the sub-cohort of the patient. The patients
    were split in two sub-cohorts ('1' or '2'). Each sub-cohort/core/protein
    was assessed in one TMA slide;
  - core: identifier for each core (up to 3, 'A', 'B' or 'C') for the patient;
  - protein: name of the protein measured from
    - 'caspase3';
    - 'caspase9';
    - 'smac';
    - 'xiap';
  - hscore: concentration measured by TMA for the protein in 'protein'.
    Protein concentrations are expressed in arbitrary units in the range [0, 300].
    Preparation of tumor samples for TMA measurements, experimental protocol
    and data processing are described in detail in Methods S2 of the manuscript.

For more information, refer to the manuscript.
