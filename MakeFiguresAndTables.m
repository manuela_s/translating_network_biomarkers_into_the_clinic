function MakeFiguresAndTables()
% Make figures and tables presented in the paper and supplementary materials.

if exist('figures_and_tables', 'dir')==0
    mkdir('figures_and_tables');
end

% Figures:
make.Figure2();
make.Figure3();
make.Figure4();
make.Figure5();
make.Figure6();

% Supplementary figures:
make.FigureS1();
make.FigureS3();
make.FigureS4();
make.FigureS5();
make.FigureS6();

% Supplementary tables:
make.TableS1();
% TableS2 uses inputs for Cox regression models from the figures and supplementary figures,
% and must run last.
make.TableS2();

% Supplementary file sets:
make.FileSetsS1_2();

% Auxiliary figure (not included in the paper):
make.FigureA1();
end
