function tma_combat_corrected = BatchCorrectedTMA()
% Get TMA data batch corrected with ComBat (Leek et al., R package).
% Output:
% - tma_combat_corrected: table, one row per protein per sample. Columns include:
%   - universal_patient_id;
%   - sub_cohort;
%   - core;
%   - protein;
%   - hscore;
%   - batch.

% Get Clinical data
cli = get.Clinical();

% Get TMA patients data
tma = get.RawTMA();

% Join tma and cli to get stage and chemotherapy status for ComBat
tma = innerjoin(tma, cli,...
    'keys', 'universal_patient_id',...
    'rightvariables', {'stage', 'chemotherapy'});

% Run ComBat to remove batch effects
tma.batch = tma.sub_cohort .* tma.core;
tma_w = unstack(tma(:, {'universal_patient_id', 'stage', 'chemotherapy',...
    'batch', 'sub_cohort', 'core', 'protein', 'hscore'}),...
    'hscore', 'protein',...
    'AggregationFunction', @cohort_utilities.DummyAggregator);
% Remove NaNs before running ComBat
cols = {'caspase3', 'caspase9', 'smac', 'xiap'};
tma_w = tma_w(~any(isnan(tma_w{:, cols}), 2), :);
covariates = {'stage', 'chemotherapy'};
tma_combat_corrected_w = matlab_utilities.Combat(...
    tma_w, 'batch', cols, covariates);

tma_combat_corrected = stack(tma_combat_corrected_w,...
    {'caspase3', 'caspase9', 'smac', 'xiap'},...
    'NewDataVariableName', 'hscore', 'IndexVariableName', 'protein');

tma_combat_corrected(:, {'stage', 'chemotherapy'}) = [];
end
