function [rppa, tma, rppa_and_tma] = ProteinsByTechnique(proteins)
% Get patient protein concentrations for cohort measured by different techniques.
% Input argument:
% - proteins: table, as returned by 'get.AbsProteinConcentrations';
% Outputs:
% - rppa: table, one row per patient. Columns for 'universal_patient_id' and
%   protein concentrations for 'caspase3', 'caspase9', 'smac' and 'xiap';
% - tma: table, one row per patient. Columns for 'universal_patient_id' and
%   protein concentrations for 'caspase3' and 'xiap';
% - rppa_and_tma: table, one row per patient. Columns for 'universal_patient_id'
%   and rppa protein concentrations for 'caspase9' and 'smac' and tma 
%   protein concentrations for 'caspase3' and 'xiap';

rppa = unstack(proteins(proteins.technique=='RPPA', :), 'concentration', 'protein');
rppa.technique = [];

tma = unstack(proteins(proteins.technique=='TMA', :), 'concentration', 'protein');
tma.technique = [];

rppa_and_tma = innerjoin(...
    rppa(:, {'universal_patient_id', 'caspase9', 'smac'}),...
    tma,...
    'key', 'universal_patient_id');
rppa_and_tma = rppa_and_tma(:,...
    {'universal_patient_id', 'caspase3', 'caspase9', 'smac', 'xiap'});
end
