function proteins = AbsProteinConcentrations()
% Get protein concentrations converted to absolute units (uM).
% Output:
% - proteins: table, one row per patient/protein/technique with columns:
%   - universal_patient_id: categorical, patient identifier;
%   - protein: categorical, name of protein. One of 'caspase3', 'caspase9',
%     'smac' or 'xiap';
%   - technique: categorical, type of technique used to measure the protein
%     expression. One of 'RPPA' or 'TMA';
%   - concentration: double, protein concentration in uM.

cli = get.Clinical();

rppa = get.RawRPPA();
rppa.Properties.VariableNames{'concentration'} = 'preprocessed_concentration';

tma = get.BatchCorrectedTMA();

% Drop caspase9/smac because of poor agreement with RPPA (see figure A1).
tma(ismember(tma.protein, {'caspase9', 'smac'}), :) = [];

tma = grpstats(tma, {'universal_patient_id', 'protein'}, @median, 'DataVars', 'hscore');
tma.GroupCount = [];
tma.Properties.VariableNames{'median_hscore'} = 'preprocessed_concentration';

proteins = matlab_utilities.ConcatenateTables(...
    {rppa, tma},...
    'new_column', 'technique',...
    'new_column_values', {'RPPA', 'TMA'});

proteins = innerjoin(proteins, cli,...
    'key', 'universal_patient_id',...
    'RightVariables', 'stage');

% Add and rename columns for 'cohort_utilities.ConvertRawToAbs':
proteins.Properties.VariableNames{'protein'} = 'ProteinName';
proteins.SampleType = categorical(repmat({'patient'}, height(proteins), 1));

crc_ref = cohort_utilities.reference_cohorts.CRC();
proteins = cohort_utilities.ConvertRawToAbs(...
    proteins,...
    'ref_cohort_dist_alignment_stage23_only',...
    'grouping_vars', {'ProteinName', 'technique'},...
    'reference', crc_ref);

proteins.Properties.VariableNames{'ProteinName'} = 'protein';
proteins.Properties.VariableNames{'unconverted_storage_abs_concentration'} = 'concentration';
proteins = proteins(:, {'universal_patient_id', 'protein', 'technique', 'concentration'});
end
