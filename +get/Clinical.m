function cli = Clinical()
% Get clinical data.
% Output:
% - cli: table, data from clinical_data.csv

cli = readtable(fullfile('data', 'clinical_data.csv'));

cat_vars = {'universal_patient_id',...
    'resection_margins', 'chemotherapy', 'stage', 'T', 'N', 'M',...
    'sex', 'tumour_site', 'lymphovascular_invasion',...
    'is_censored_disease_free_survival', 'is_censored_overall_survival',...
    'KRAS_status', 'KRAS_mutation_type'};
for c= 1:numel(cat_vars)
    cli.(cat_vars{c}) = categorical(cli.(cat_vars{c}));
end

% Reorder categories for selected variables
cli.tumour_site = reordercats(cli.tumour_site,...
    {'distal', 'proximal', 'rectal', 'appendix', 'others'});
cli.KRAS_status = reordercats(cli.KRAS_status, {'wild-type', 'mutant'});
cli.KRAS_mutation_type = reordercats(cli.KRAS_mutation_type,...
    {'wild-type', 'A59T', 'G12A', 'G12C', 'G12D', 'G12R', 'G12S', 'G12V',...
    'G13D', 'Q61H', 'Q61P', 'Q61R'});
end
