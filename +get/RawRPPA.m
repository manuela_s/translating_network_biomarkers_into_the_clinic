function rppa = RawRPPA()
% Read in the raw RPPA data.
% Output:
% - tma: table, data from rppa_data.csv.

rppa = readtable(fullfile('data', 'rppa_data.csv'));

rppa.universal_patient_id = categorical(rppa.universal_patient_id);
rppa.protein = categorical(rppa.protein);
end
