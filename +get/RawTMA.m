function tma = RawTMA()
% Read in the raw TMA data.
% Output:
% - tma: table, data from tma_data.csv.

tma = readtable(fullfile('data', 'tma_data.csv'));

tma.universal_patient_id = categorical(tma.universal_patient_id);
tma.sub_cohort = categorical(tma.sub_cohort);
tma.core = categorical(tma.core);
tma.protein = categorical(tma.protein);
end
