classdef DistributionAlignment < cohort_utilities.alignment.AbstractAligner
    % DistributionAlignment.
    % Align a new dataset (to_be_matched_dataset) to a reference dataset by
    % transforming its distribution curve to fit the distribution curve of
    % the reference dataset.
    % For more information, see <a href="https://www.ncbi.nlm.nih.gov/pubmed/27649552">Salvucci et al., Clinical Cancer Research, 2016</a>.
    
    properties
        % Probability distribution of the reference dataset
        reference_pd;
        % Probability distribution of the dataset to be aligned to the reference dataset
        to_be_matched_pd;
    end
    
    methods
        function obj = DistributionAlignment(reference_dataset, to_be_matched_dataset)
            % Create instance of DistributionAlignment.
            % Input arguments:
            % - reference_dataset: numeric vector, measurements from the
            %   reference_dataset to align to;
            % - to_be_matched_dataset: numeric vector, measurements from the
            %   new dataset to be aligned to the reference dataset;
            % Output:
            % - obj: instance of DistributionAlignment, can be used to
            %   transform values that are in or aligned with
            %   to_be_matched_dataset to values in the reference_dataset.
            % See also Transform.
            obj.reference_pd = fitdist(reference_dataset(:), 'kernel',...
                'Kernel', 'normal',...
                'support', 'positive');
            obj.to_be_matched_pd = fitdist(to_be_matched_dataset(:), 'kernel',...
                'Kernel', 'normal');
        end
        
        function new_values = Transform(obj, old_values)
            percentiles = cdf(obj.to_be_matched_pd, old_values);
            new_values = icdf(obj.reference_pd, percentiles);
        end
    end
    
    methods (Static)
        function new_values = Align(reference_dataset, to_be_matched_dataset)
            obj = cohort_utilities.alignment.DistributionAlignment(reference_dataset, to_be_matched_dataset);
            new_values = obj.Transform(to_be_matched_dataset);
        end
    end
end
