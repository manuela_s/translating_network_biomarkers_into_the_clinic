function [j, metadata] = ConvertRawToAbs(raw, method, varargin)
% Convert raw concentrations into absolute concentrations in uM.
% Input arguments:
% - raw: table with columns:
%   - ProteinName: categorical, name of the proteins
%     ('apaf1', 'caspase3', 'caspase9', 'smac' or 'xiap');
%   - SampleType: categorical, type of sample ('patient' or 'cell_line');
%   - StorageType: categorical, type of sample storage ('FF' or 'FFPE');
%   - stage: categorical, stage ('1', '2', '3' or '4');
%   - preprocessed_concentration: numeric vector, protein concentration in
%     arbitrary units;
%   - tumour_samples: categorical, type of tumour sample
%     ('newly_diagnosed' or 'recurrent'). Applies only to method
%     'ref_cohort_dist_alignment_GBM_newly_diagnosed_only';
% - method: character array, name of the conversion method to use, one of:
%   - 'ref_cohort_dist_alignment:
%     align distribution to the reference cohort;
%   - 'ref_cohort_dist_alignment_per_storage_type':
%     align samples from each StorageType separatelly to the reference cohort;
%   - 'ref_cohort_dist_alignment_stage23_only':
%     use samples from stage 2 and 3 to find alignment transformation to the
%     reference cohort and apply to all samples;
%   - 'ref_cohort_dist_alignment_stage3_only':
%     use samples from 3 to find alignment transformation to the reference
%     cohort and apply to all samples;
%   - 'ref_cohort_dist_alignment_GBM_newly_diagnosed_only':
%     use samples with 'newly_diagnosed' tumour (i.e. newly diagnosed Glioblastoma,
%     GBM) to find alignment transformation to the reference cohort and
%     apply to all samples;
% - key/value:
%   - grouping_vars: cell array, column names to group samples by.
%     The conversion is applied individually for each group. Defaults to
%     {'cohort', 'ProteinName'};
%   - reference: instance of AbstractReferenceCohort, used when the method
%     requires alignment to a reference cohort.

p = inputParser();
p.addParameter('grouping_vars', {'cohort', 'ProteinName'}, @iscell);
p.addParameter('reference', [], @(ref) isa(ref, 'cohort_utilities.reference_cohorts.AbstractReferenceCohort'));
p.parse(varargin{:});
grouping_vars = p.Results.grouping_vars;
ref = p.Results.reference;

switch method
    case 'ref_cohort_dist_alignment'
        [j, metadata] = cohort_utilities.RefDistributionAlignment(...
            ref, raw, grouping_vars, true(height(raw), 1));
    case 'ref_cohort_dist_alignment_per_storage_type'
        [j, metadata] = cohort_utilities.RefDistributionAlignment(...
            ref, raw, [grouping_vars, 'StorageType'], true(height(raw), 1));
    case 'ref_cohort_dist_alignment_stage23_only'
        [j, metadata] = cohort_utilities.RefDistributionAlignment(...
            ref, raw, grouping_vars, ismember(raw.stage, {'2', '3'}));
    case 'ref_cohort_dist_alignment_stage3_only'
        ref = ref.SubsetStage('3');
        [j, metadata] = cohort_utilities.RefDistributionAlignment(...
            ref, raw, grouping_vars, raw.stage == '3');
    case 'ref_cohort_dist_alignment_GBM_newly_diagnosed_only'
        [j, metadata] = cohort_utilities.RefDistributionAlignment(...
            ref, raw, grouping_vars, raw.cohort == 'GBM' & raw.tumour_samples == 'newly_diagnosed');
    otherwise
        error('Method not supported');
end

% Set negative or zero concentrations to eps.
j.unconverted_storage_abs_concentration(j.unconverted_storage_abs_concentration <= 0) = eps;
end
