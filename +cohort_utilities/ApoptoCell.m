function [traces, header, all_traces] = ApoptoCell(conc, settings, kinetic_par)
% Calculates concentration traces over time for the APOPTO-CELL model.
% See <a href="https://www.ncbi.nlm.nih.gov/pubmed/16932741">Rehm et al., EMBO, 2006</a> for details.
% Input arguments:
% - conc: initial concentrations (in uM) for Procaspase-3, Procaspase9, XIAP,
%   Apaf1, SMAC and smac_mimetics, and optionally initial substrate fraction;
% - settings: struct with duration [min] and stepsize [min];
% - kinetic_par: struct with kinetic parameters. Defaults to 
%   GetDefaultApoptoCellKineticParameters().
% Outputs:
% - traces: matrix with time-courses of key species. See header.
% - header: description of the columns in traces.
% - allTraces: matrix with time-courses of extended set of species.

assert(numel(conc) == 6 || numel(conc) == 7);
if numel(conc) < 7
    conc(7) = 1;
end
if (nargin<3)
    kinetic_par = cohort_utilities.GetDefaultApoptoCellKineticParameters();
end

% Initial conditions
iC = zeros(29, 1);
iC(1) = conc(1);    % C3_, Procaspase-3;
iC(2) = conc(2);    % C9a, Caspase-9 bound to the apoptososme (p35/p12);
iC(3) = 0.0;		% C3a, free active Caspase-3;
iC(4) = 0.0;	    % C9H, Caspase-9 bound to the apoptosome (p35/p10);
iC(5) = conc(3);    % XIAP_, free XIAP;
iC(6) = 0.0;		% [XIAP~C3a], XIAP bound to Caspase-3;
iC(7) = 0.0;		% [XIAP~C9a], XIAP bound to Caspase-9 (p35/p12);
iC(8) = 0.0;	    % [XIAP~C9a~C3a], XIAP bound to Caspase-9 (p35/p12) and Caspase-3;
iC(9) = 0.0;		% [XIAP~{cl-C9a}], XIAP bound to the p2-fragment;
iC(10) = 0.0;		% [[XIAP~{cl-C9a}]~C3a], XIAP bound to the p2-fragment and active Caspase-3;
iC(11) = 0.0;		% BIR12_, XIAP fragment of domains BIR1 and BIR2;
iC(12) = 0.0;		% BIR3R_, XIAP fragment of domains BIR3 and RING;
iC(13) = 0.0;		% [BIR12~C3a], BIR12 bound to Caspase-3;
iC(14) = 0.0;		% [BIR3R~C9a], BIR3R bound to Caspase-9 (p35/p12);
iC(15) = 0.0;		% [BIR3R~{cl-C9a}], BIR3R bound to the p2-fragment;
iC(16) = 0.0;		% SMAC_, free SMAC;
iC(17) = 0.0;		% [XIAP~2SMAC], XIAP bound to SMAC dimer;
iC(18) = 0.0;		% [[XIAP~{cl-C9a}]~SMAC], XIAP bound to the p2-fragment and SMAC;
iC(19) = 0.0;		% [BIR12~SMAC], BIR12 bound to SMAC dimer;
iC(20) = 0.0;		% [BIR3R~SMAC], BIR3R bound to SMAC;
iC(21) = conc(7);   % Substrate (DEVD effector caspase);
iC(22) = conc(6);   % SMAC_mimetic;
iC(23) = 0.0;		% [XIAP~2SMACmimetic], XIAP bound to SMAC_mimetic dimer;
iC(24) = 0.0;		% PAC;
iC(25) = conc(4);	% Apaf-1, free Apaf-1;
iC(26) = conc(5);	% SMACmito, mitochondrial SMAC;
iC(27) = 0.0;		% [BIR12~SMACmimetic], BIR12 bound to SMAC_mimetic dimer;
iC(28) = 0.0;		% [BIR3R~SMACmimetic], BIR3R bound to SMAC_mimetic;
iC(29) = 0.0;       % [[XIAP~{cl-C9a}]~SMACmimetic], XIAP bound to the p2-fragment and SMAC_mimetic.

t12 = 2.3; % apoptosome formation kinetic
t12smac = 7; % SMAC release kinetic

% Bottleneck in apoptosome formation between Apaf-1 and Procaspase-9
if iC(2) < iC(25)
    iC(25) = iC(2);
end;
iC(2) = 0;

% Kinetic parameters:                                     %
% 1 C3_ + 1 C9a => 1 C3a + 1 C9a
k(3) = kinetic_par.k3;
% 1 C9a + 1 C3a => 1 C9H + 1 C3a
k(4) = kinetic_par.k4;
% 1 C3_ + 1 C9H => 1 C3a + 1 C9H
k(5) = 8*k(3);
% 1 C3_ + 1 C3a => 1 C3a + 1 C3a
k(6) = kinetic_par.k6;
% 1 C3a + 1 XIAP_ <=> 1 [XIAP~C3a]
k(7) = kinetic_par.k7; k_(7) = kinetic_par.k_7;
% 1 C3a + 1 BIR12_ <=> 1 [BIR12~C3a]
k(8) = kinetic_par.k8; k_(8) = kinetic_par.k_8;
% 1 XIAP_ + 1 C3a => 1 BIR12_ + 1 BIR3R_ + 1 C3a
k(9) = kinetic_par.k9;
% 1 [XIAP~C9a] + 1 C3a => 1 BIR12_ + 1 [BIR3R~C9a] + 1 C3a
k(10) = kinetic_par.k10;
% 1 [XIAP~C3a] + 1 C3a => 1 BIR3R_ + 1 [BIR12~C3a] + 1 C3a
k(11) = kinetic_par.k11;
% 1 [XIAP~{cl-C9a}] + 1 C3a => 1 BIR12_ + 1 [BIR3R~{cl-C9a}] + 1 C3a
k(12) = kinetic_par.k12;
% 1 [[XIAP~{cl-C9a}]~C3a] + 1 C3a => 1 [BIR12~C3a] + 1 [BIR3R~{cl-C9a}] + 1 C3a
k(13) = kinetic_par.k13;
% 1 [XIAP~C9a~C3a] + 1 C3a => 1 [BIR12~C3a] + 1 [BIR3R~C9a] + 1 C3a
k(14) = kinetic_par.k14;
% 1 [XIAP~2SMAC] + 1 C3a => 1 [BIR12~SMAC] + 1 [BIR3R~SMAC] + 1 C3a
k(15) = kinetic_par.k15;
% 1 [XIAP~C9a~C3a] + 1 C3a => 1 C9H + 1 [[XIAP~{cl-C9a}]~C3a] + 1 C3a
k(16) = kinetic_par.k16;
% 1 [XIAP~C9a] + 1 C3a => 1 C9H + 1 [XIAP~{cl-C9a}] + 1 C3a
k(17) = kinetic_par.k17;
% 1 [BIR3R~C9a] + 1 C3a => 1 [BIR3R~{cl-C9a}] + 1 C9H + 1 C3a
k(18) = kinetic_par.k18;
% 1 C9a + 1 XIAP_ <=> 1 [XIAP~C9a]
k(19) = kinetic_par.k19; k_(19) = kinetic_par.k_19;
% 1 C9a + 1 BIR3R_ <=> 1 [BIR3R~C9a]
k(20) = kinetic_par.k20; k_(20) = kinetic_par.k_20;
% 1 XIAP_ + 1 SMAC_ + 1 SMAC_ <=> 1 [XIAP~2SMAC]
k(21) = kinetic_par.k21; k_(21) = kinetic_par.k_21;
% 1 [XIAP~C9a] + 1 SMAC_ + 1 SMAC_ <=> 1 [XIAP~2SMAC] + 1 C9a
k(22) = kinetic_par.k22; k_(22) = kinetic_par.k_22;
% 1 [XIAP~C3a] + 1 SMAC_ + 1 SMAC_ <=> 1 [XIAP~2SMAC] + 1 C3a
k(23) = kinetic_par.k23; k_(23) = kinetic_par.k_23;
% 1 BIR12_ + 1 SMAC_ <=> 1 [BIR12~SMAC]
k(24) = kinetic_par.k24; k_(24) = kinetic_par.k_24;
% 1 BIR3R_ + 1 SMAC_ <=> 1 [BIR3R~SMAC]
k(25) = kinetic_par.k25; k_(25) = kinetic_par.k_25;
% 1 [BIR12~C3a] + 1 SMAC_ <=> 1 [BIR12~SMAC] + 1 C3a
k(26) = kinetic_par.k26; k_(26) = kinetic_par.k_26;
% 1 [BIR3R~C9a] + 1 SMAC_ <=> 1 [BIR3R~SMAC] + 1 C9a
k(27) = kinetic_par.k27; k_(27) = kinetic_par.k_27;
% 1 [XIAP~{cl-C9a}] + 1 SMAC_ + 1 SMAC_ <=> 1 [[XIAP~{cl-C9a}]~SMAC]
k(28) = kinetic_par.k28; k_(28) = kinetic_par.k_28;
% 1 C9H => out
k(29) = kinetic_par.kout1;
% 1 C9a => out
k(30) = kinetic_par.kout1;
% 1 C3a => out
k(31) = kinetic_par.kout1;
% 1 [XIAP~C3a] => out
k(32) = kinetic_par.kout2;
% 1 [XIAP~C9a~C3a] => out
k(33) = kinetic_par.kout2;
% 1 [XIAP~C9a] => out
k(34) = kinetic_par.kout2;
% 1 [XIAP~{cl-C9a}] => out
k(35) = kinetic_par.kout1;
% 1 [[XIAP~{cl-C9a}]~C3a] => out
k(36) = kinetic_par.kout2;
% 1 [[XIAP~{cl-C9a}]~SMAC] => out
k(37) = kinetic_par.kout2;
% 1 [XIAP~2SMAC] => out
k(38) = kinetic_par.kout2;
% 1 BIR12_ => out
k(39) = kinetic_par.kout1;
% 1 BIR3R_ => out
k(40) = kinetic_par.kout2;
% 1 [BIR12~SMAC] => out
k(41) = kinetic_par.kout1;
% 1 [BIR3R~SMAC] => out
k(42) = kinetic_par.kout2;
% 1 [BIR12~C3a] => out
k(43) = kinetic_par.kout1;
% 1 [BIR3R~C9a] => out
k(44) = kinetic_par.kout1;
% 1 [BIR3R~{cl-C9a}] => out
k(45) = kinetic_par.kout2;
% 1 SMAC_ =>
k(46) = kinetic_par.kout1;
% 1 Substrate + 1 C3a => 1 C3a
k(47) = kinetic_par.k47;
% 1 APAF1 => 1 C9a
k(48) = log(2)/t12;
% 1 SMACmito => 1 SMAC_
k(49) = log(2)/t12smac;
% 1 C3_ => out
k(50) = kinetic_par.k50;
% 1 XIAP_ => out
k(51) = kinetic_par.k51;
% => 1 C3_
k(1) = iC(1)*k(50);
% => 1 XIAP_
k(2) = iC(5)*k(51);
% 1 SMAC_mimetic => out
k(52) = kinetic_par.kout1;
% 1 XIAP_ + 1 SMAC_mimetic + 1 SMAC_mimetic <=> 1 [XIAP~2SMACmimetic]
k(53) = kinetic_par.k53; k_(53) = kinetic_par.k_53;
% 1 [XIAP~C9a] + 1 SMAC_mimetic + 1 SMAC_mimetic <=> 1 [XIAP~2SMACmimetic] + 1 C9a
k(54) = kinetic_par.k54; k_(54) = kinetic_par.k_54;
% 1 [XIAP~C3a] + 1 SMAC_mimetic + 1 SMAC_mimetic <=> 1 [XIAP~2SMACmimetic] + 1 C3a
k(55) = kinetic_par.k55; k_(55) = kinetic_par.k_55;
% 1 BIR12_ + 1 SMAC_mimetic <=> 1 [BIR12~SMACmimetic]
k(56) = kinetic_par.k56; k_(56) = kinetic_par.k_56;
% 1 BIR3R_ + 1 SMAC_mimetic <=> 1 [BIR3R~SMACmimetic]
k(57) = kinetic_par.k57; k_(57) = kinetic_par.k_57;
% 1 [BIR12~C3a] + 1 SMAC_mimetic <=> 1 [BIR12~SMACmimetic] + 1 C3a
k(58) = kinetic_par.k58; k_(58) = kinetic_par.k_58;
% 1 [BIR3R~C9a] + 1 SMAC_mimetic <=> 1 [BIR3R~SMACmimetic] + 1 C9a
k(59) = kinetic_par.k59; k_(59) = kinetic_par.k_59;
% 1 [XIAP~{cl-C9a}] + 1 SMAC_mimetic + 1 SMAC_mimetic <=> 1 [[XIAP~{cl-C9a}]~SMACmimetic]
k(60) = kinetic_par.k60; k_(60) = kinetic_par.k_60;
% 1 PAC + 1 C3_ => 1 C3a + 1 PAC
k(61) = kinetic_par.k61;
% 1 PAC => out
k(62) = kinetic_par.kout1;
% 1 [XIAP~2SMACmimetic] => out
k(63) = kinetic_par.kout2;
% 1 [BIR12~SMACmimetic] => out
k(64) = kinetic_par.kout1;
% 1 [BIR3R~SMACmimetic] => out
k(65) = kinetic_par.kout2;
% 1 [[XIAP~{cl-C9a}]~SMACmimetic] => out
k(66) = kinetic_par.kout2;
% 1 [XIAP~2SMACmimetic] + 1 C3a => 1 [BIR12~SMACmimetic] + 1 [BIR3R~SMACmimetic] + 1 C3a
k(67) = kinetic_par.k67;

% Run simulation
options = odeset('AbsTol', 1e-19, 'RelTol', 1e-08);
t_steps = 0.0:settings.stepsize:settings.duration;
[T, x] = ode15s(@aposim, t_steps, iC, options, k, k_);
if numel(t_steps) == 2
    % ode15s interprets t_steps as an interval if it only has two numbers. 
    % Keep the first and the last entries.
    x = x([1, end], :);
    T = T([1, end]);
end

% Traces for extended set of species
all_traces = [x(:, 1:7), x(:, 9), x(:, 11:21), x(:, 25:26)]; 
% Traces for key species. Note that substrate_cleavage (column 3) is returned as %
traces = [x(:, 2), x(:, 3), (1-x(:, 21))*100]; 
% Headers for key species.
header = {'Casp. 9 [uM]', 'Casp. 3 [uM]', 'cl. Substrate [%]'};  
end

function dC = aposim(t, iC, k, k_)
%% Calculates concentration derivatives at a given time point.
% Input arguments:
% - t: time (ignored);
% - iC: conditions for state variables at time t;
% - k: forward kinetic parameters;
% - k_: backward kinetic parameters.
% Outputs:
% - dC: state variable derivatives at time t.

sbreakdown = 0.01;

% v = k_on*c - k_off*c
var1 = (k(1)- k(50)*iC(1));
% v = k_on*c - k_off*c
var2 = (k(2)- k(51)*iC(5)); 

if iC(21) < sbreakdown
    % Breakdown of protein production
    var1 = 0; 
    var2 = 0;
    % No enforced degradation
    k(32:34) = 0.0058; 
    k(37:38) = 0.0058;
    k(40) = 0.0058;
    k(42) = 0.0058;
    k(44:45) = 0.0058;
    k(63) = 0.0058;
    k(65:66) = 0.0058;
end

% Reaction equations for all components of the apoptosis system
dC = double(iC);
% C3_
dC(1) = var1 - k(3)*iC(1)*iC(2) - k(5)*iC(1)*iC(4)- k(6)*iC(1)*iC(3) - k(61)*iC(24)*iC(1);	
% C9a
dC(2) = - k(4)*iC(2)*iC(3) - k(19)*iC(2)*iC(5) + k_(19)*iC(7) - k(20)*iC(2)*iC(12) +...
    k_(20)*iC(14) + k(22)*iC(7)*iC(16)*iC(16) - k_(22)*iC(17)*iC(2) + k(27)*iC(14)*iC(16) -...
    k_(27)*iC(20)*iC(2) - k(30)*iC(2) + k(48)*iC(25) + k(54)*iC(7)*iC(22)*iC(22)-...
    k_(54)*iC(23)*iC(2) + k(59)*iC(14)*iC(22)- k_(59)*iC(28)*iC(2);	
% C3a
dC(3) = k(3)*iC(1)*iC(2) + k(5)*iC(1)*iC(4) - k(7)*iC(3)*iC(5) + k_(7)*iC(6) -...
    k(8)*iC(3)*iC(11) + k_(8)*iC(13)+ k(23)*iC(6)*iC(16)*iC(16) - k_(23)*iC(17)*iC(3) +...
    k(26)*iC(13)*iC(16) - k_(26)*iC(19)*iC(3) - k(31)*iC(3) + k(55)*iC(6)*iC(22)*iC(22) -...
    k_(55)*iC(23)*iC(3) + k(58)*iC(13)*iC(22) - k_(58)*iC(27)*iC(3) + k(6)*iC(1)*iC(3) + k(61)*iC(1)*iC(24);	
% C9H
dC(4) = k(4)*iC(2)*iC(3) + k(16)*iC(8)*iC(3) + k(17)*iC(7)*iC(3) + k(18)*iC(14)*iC(3) - k(29)*iC(4);	
% XIAP_
dC(5) = var2 - k(7)*iC(3)*iC(5) + k_(7)*iC(6) - k(9)*iC(5)*iC(3) - k(19)*iC(2)*iC(5) +...
    k_(19)*iC(7) - k(21)*iC(5)*iC(16)*iC(16) + k_(21)*iC(17) - k(53)*iC(5)*iC(22)*iC(22) + k_(53)*iC(23);	
% [XIAP~C3a]
dC(6) = k(7)*iC(3)*iC(5) - k_(7)*iC(6) - k(11)*iC(6)*iC(3) - k(23)*iC(6)*iC(16)*iC(16)+...
    k_(23)*iC(17)*iC(3) - k(32)*iC(6) - k(55)*iC(6)*iC(22)*iC(22) + k_(55)*iC(23)*iC(3);	
% [XIAP~C9a]
dC(7) = - k(10)*iC(7)*iC(3) - k(17)*iC(7)*iC(3) + k(19)*iC(2)*iC(5) - k_(19)*iC(7) -...
    k(22)*iC(7)*iC(16)*iC(16) + k_(22)*iC(17)*iC(2) - k(34)*iC(7) - k(54)*iC(7)*iC(22)*iC(22) + k_(54)*iC(23)*iC(2);	
% [XIAP~C9a~C3a]
dC(8) = - k(14)*iC(8)*iC(3) - k(16)*iC(8)*iC(3) - k(33)*iC(8);	
% [XIAP~{cl-C9a}]
dC(9) = - k(12)*iC(9)*iC(3) + k(17)*iC(7)*iC(3) - k(28)*iC(9)*iC(16)*iC(16) +...
    k_(28)*iC(18) - k(35)*iC(9) - k(60)*iC(9)*iC(22)*iC(22) + k_(60)*iC(29);
% [[XIAP~{cl-C9a}]~C3a]
dC(10) = - k(13)*iC(10)*iC(3) + k(16)*iC(8)*iC(3) - k(36)*iC(10);
% BIR12_
dC(11) = - k(8)*iC(3)*iC(11) + k_(8)*iC(13) + k(9)*iC(5)*iC(3) + k(10)*iC(7)*iC(3) +...
    k(12)*iC(9)*iC(3) - k(24)*iC(11)*iC(16) + k_(24)*iC(19) - k(39)*iC(11) - k(56)*iC(11)*iC(22) + k_(56)*iC(27);	
% BIR3R_
dC(12) = k(9)*iC(5)*iC(3) + k(11)*iC(6)*iC(3) - k(20)*iC(2)*iC(12) + k_(20)*iC(14) -...
    k(25)*iC(12)*iC(16) + k_(25)*iC(20) - k(40)*iC(12) - k(57)*iC(12)*iC(22) + k_(57)*iC(28);	
% [BIR12~C3a]
dC(13) = k(8)*iC(3)*iC(11) - k_(8)*iC(13) + k(11)*iC(6)*iC(3) + k(13)*iC(10)*iC(3) +...
    k(14)*iC(8)*iC(3) - k(26)*iC(13)*iC(16) + k_(26)*iC(19)*iC(3) - k(43)*iC(13) -...
    k(58)*iC(13)*iC(22) + k_(58)*iC(27)*iC(3);	
% [BIR3R~C9a]
dC(14) = k(10)*iC(7)*iC(3) + k(14)*iC(8)*iC(3) - k(18)*iC(14)*iC(3) +...
    k(20)*iC(2)*iC(12) - k_(20)*iC(14) - k(27)*iC(14)*iC(16) + k_(27)*iC(20)*iC(2) -...
    k(44)*iC(14) - k(59)*iC(14)*iC(22) + k_(59)*iC(28)*iC(2);	
% [BIR3R~{cl-C9a}]
dC(15) = k(12)*iC(9)*iC(3) + k(13)*iC(10)*iC(3) + k(18)*iC(14)*iC(3) - k(45)*iC(15);	
% SMAC_
dC(16) = - k(21)*iC(5)*iC(16)*iC(16) + k_(21)*iC(17) - k(21)*iC(5)*iC(16)*iC(16) +...
    k_(21)*iC(17) - k(22)*iC(7)*iC(16)*iC(16) + k_(22)*iC(17)*iC(2) - k(22)*iC(7)*iC(16)*iC(16) + ...
    k_(22)*iC(17)*iC(2) - k(23)*iC(6)*iC(16)*iC(16) + k_(23)*iC(17)*iC(3) - k(23)*iC(6)*iC(16)*iC(16) +...
    k_(23)*iC(17)*iC(3) - k(24)*iC(11)*iC(16) + k_(24)*iC(19) - k(25)*iC(12)*iC(16) + k_(25)*iC(20) -...
    k(26)*iC(13)*iC(16) + k_(26)*iC(19)*iC(3) - k(27)*iC(14)*iC(16) + k_(27)*iC(20)*iC(2) -...
    k(28)*iC(9)*iC(16)*iC(16) + k_(28)*iC(18) - k(28)*iC(9)*iC(16)*iC(16) + k_(28)*iC(18) -...
    k(46)*iC(16) + k(49)*iC(26);	
% [XIAP~2SMAC]
dC(17) = - k(15)*iC(17)*iC(3) + k(21)*iC(5)*iC(16)*iC(16) - k_(21)*iC(17) + k(22)*iC(7)*iC(16)*iC(16) -...
    k_(22)*iC(17)*iC(2) + k(23)*iC(6)*iC(16)*iC(16) - k_(23)*iC(17)*iC(3) - k(38)*iC(17);	
% [[XIAP~{cl-C9a}]~SMAC]
dC(18) = k(28)*iC(9)*iC(16)*iC(16) - k_(28)*iC(18) - k(37)*iC(18);	
% [BIR12~SMAC]
dC(19) = k(15)*iC(17)*iC(3) + k(24)*iC(11)*iC(16) - k_(24)*iC(19) + k(26)*iC(13)*iC(16) -...
    k_(26)*iC(19)*iC(3) - k(41)*iC(19);	
% [BIR3R~SMAC]
dC(20) = k(15)*iC(17)*iC(3) + k(25)*iC(12)*iC(16) - k_(25)*iC(20) + k(27)*iC(14)*iC(16) -...
    k_(27)*iC(20)*iC(2) - k(42)*iC(20);	
% Substrate
dC(21) = - k(47)*iC(21)*iC(3);	
% SMAC_mimetic
dC(22) = - k(52)*iC(22) - k(53)*iC(5)*iC(22)*iC(22) + k_(53)*iC(23) - k(53)*iC(5)*iC(22)*iC(22) +...
    k_(53)*iC(23) - k(54)*iC(7)*iC(22)*iC(22) + k_(54)*iC(23)*iC(2) - k(54)*iC(7)*iC(22)*iC(22) +...
    k_(54)*iC(23)*iC(2) - k(55)*iC(6)*iC(22)*iC(22) + k_(55)*iC(23)*iC(3) - k(55)*iC(6)*iC(22)*iC(22) +...
    k_(55)*iC(23)*iC(3) - k(56)*iC(11)*iC(22) + k_(56)*iC(27) - k(57)*iC(12)*iC(22) + k_(57)*iC(28) -...
    k(58)*iC(13)*iC(22) + k_(58)*iC(27)*iC(3) - k(59)*iC(14)*iC(22) + k_(59)*iC(28)*iC(2) -...
    k(60)*iC(9)*iC(22)*iC(22) + k_(60)*iC(29) - k(60)*iC(9)*iC(22)*iC(22) + k_(60)*iC(29);	
% [XIAP~2SMACmimetic]
dC(23) = - k(67)*iC(23)*iC(3) + k(53)*iC(5)*iC(22)*iC(22) - k_(53)*iC(23) +...
    k(54)*iC(7)*iC(22)*iC(22) - k_(54)*iC(23)*iC(2) + k(55)*iC(6)*iC(22)*iC(22) -...
    k_(55)*iC(23)*iC(3) - k(63)*iC(23);	
% PAC
dC(24) = - k(62)*iC(24);
% APAF1
dC(25) = - k(48)*iC(25);
% SMACmito
dC(26) = - k(49)*iC(26);	
% [BIR12~SMACmimetic]
dC(27) = k(67)*iC(23)*iC(3) + k(56)*iC(11)*iC(22) - k_(56)*iC(27) + k(58)*iC(13)*iC(22) -...
    k_(58)*iC(27)*iC(3) - k(64)*iC(27);	
% [BIR3R~SMACmimetic]
dC(28) = k(67)*iC(23)*iC(3) + k(57)*iC(12)*iC(22) - k_(57)*iC(28) + k(59)*iC(14)*iC(22) -...
    k_(59)*iC(28)*iC(2) - k(65)*iC(28);	
% [[XIAP~{cl-C9a}]~SMACmimetic]
dC(29) = k(60)*iC(9)*iC(22)*iC(22) - k_(60)*iC(29) - k(66)*iC(29);	
end
