classdef AbstractReferenceCohort
    % AbstractReferenceCohort
    % Superclass for reference-cohort-classes
    properties (Access = protected)
        % Table with measurements. One row per sample and one column per protein.
        data
    end

    methods
        function protein = GetProtein(obj, protein_name)
            % Get all measurements for the given protein for this cohort.
            % Input argument:
            % - protein_name: character array, name of the protein;
            % Output:
            % - protein: column vector with protein concentrations in uM.
            protein = obj.data.(protein_name);
        end
                
        function d = GetProteinDistribution(obj, protein_name)
            % Get probability distribution for the given protein for this cohort.
            % Input argument:
            % - protein_name: character array, name of the protein;
            % Output:
            % - d: probability distribution.
            % See also fitdist.
            
            protein = GetProtein(obj, protein_name);
            d = fitdist(protein, 'kernel', 'Kernel', 'normal', 'support', 'positive');
        end
        
        function stats = GetProteinStats(obj)
            % Get statistics for protein concentrations in the cohort.
            % Output:
            % - stats: table with one row per protein.
            %   Each row has nanmedian and iqr concentrations for the
            %   protein.

            data_tall = stack(obj.data, {'caspase3', 'caspase9', 'xiap', 'smac', 'apaf1'},...
                'NewDataVariableName', 'concentration', 'IndexVariableName', 'protein');
            stats = grpstats(data_tall(:, {'protein', 'concentration'}),...
                'protein', {@nanmedian, @iqr});
        end
    end
end
