classdef CRC < cohort_utilities.reference_cohorts.AbstractReferenceCohort
    % CRC
    % Protein concentration data (Hector et al., GUT, 2012)
    % Measurements of apaf1, caspase3, caspase9, smac and xiap performed by
    % quantitative Western blotting.
    % For more information, see <a href="https://www.ncbi.nlm.nih.gov/pubmed/22082587">Hector et al., GUT, 2012</a>.

    
    methods
        function obj = CRC()
            % Construct CRC instance, loading protein concentrations from file.
            obj.data = readtable(fullfile('+cohort_utilities', 'data',...
                'crc_reference_cohort.csv'));
        end
        
        function obj = SubsetStage(obj, stage)
            % Subset cohort by stage.
            % Input argument:
            % - stage: character array describing the stage ('2' or '3');
            % Output:
            % - obj: new instance of CRC, containing only samples in the
            %   given stage.

            obj.data = obj.data(categorical(obj.data.stage) == stage, :);
        end
    end
end

