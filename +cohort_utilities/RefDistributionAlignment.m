function [j, metadata] = RefDistributionAlignment(ref, raw, grouping_vars, to_be_matched_rows)
% Align concentrations to reference cohort.
% Input arguments:
% - ref: instance of AbstractReferenceCohort, reference cohort to align to;
% - raw: table, samples to be aligned. See ConvertRawToAbs.
% - grouping_vars: cell array, column names to group samples by;
% - to_be_matched_rows: logical column vector, identifies the rows in raw
%   that should be aligned to the reference cohort. The same transformation
%   is applied to the remaining samples;
% Output:
% - j: table, includes data from raw and adds a column
%   'unconverted_storage_abs_concentration' with the protein concentration
%   in uM;
% - metadata: struct with fields:
%   - groups: table including the proteins and the corresponding instances
%     of the DistributionAlignment class.

raw.to_be_matched = to_be_matched_rows;
patient_raw = raw(raw.SampleType == 'patient' &...
    ismember(raw.ProteinName, {'caspase3', 'caspase9', 'smac', 'xiap', 'apaf1'}), :);
groups = rowfun(...
    @(protein, conc) cohort_utilities.alignment.DistributionAlignment(ref.GetProtein(char(unique(protein))), conc),...
    patient_raw(patient_raw.to_be_matched, :),...
    'GroupingVariables', grouping_vars,...
    'InputVariables', {'ProteinName', 'preprocessed_concentration'},...
    'OutputVariableNames', 'aligner');

for g = 1:height(groups)
    sub_group = innerjoin(patient_raw, groups(g, :), 'keys', grouping_vars,...
        'RightVariables', {});
    
    sub_group.unconverted_storage_abs_concentration = groups.aligner(g).Transform(...
        sub_group.preprocessed_concentration);
    
    sub_groups{g} = sub_group; %#ok<AGROW>
end
j = vertcat(sub_groups{:});
metadata.groups = groups;
j.to_be_matched = [];
end
