function MakeKaplanMeierPlot(y, censored, varargin)
% Make Kaplan-Meier plot.
% Input arguments:
% - y: numeric column vector, time until event/censoring for each sample;
% - censored: logical, column vector with same size as y indicating censored
%   observations (true);
% - key/value pairs:
%   - groups: column vector (same size as y) with groups. Groups can be a
%     categorical, anything else that grp2idx handles or cell array of categoricals
%     for multiple grouping variables;
%   - title: char, title of the plot;
%   - xlabel: char, label for x axis. Defaults to 'Time to recurrence [months]';
%   - ylabel: char, label for y axis. Defaults to 'Disease-free survival';
%   - xlim: numeric array, lower and upper range of the x axis. Defaults to [0 120];
%   - xtick: numeric array, x ticks. Defaults to [0:12:120];
%   - ylim: numeric array, lower and upper range of the y axis. Defaults to [-0.025 1.05];
%   - ytick: numeric array, y ticks. Defaults to [0:0.25:1];
%   - tick_dir: char, direction of the tick marks. 'in' or 'out'. Defaults to 'in';
%   - censoredmarkersize: scalar, size of the marker to indicate censored observations. Defaults to 3;
%   - kmlinewidth: scalar, width of the Kaplan-Meier line. Defaults to 1;
%   - kmlinestyles: cell array, line style for each group (see LineSpec).
%     If the cell array has fewer elements than there are groups, the line
%     styles repeat. Defaults to {'-'};
%   - box_linewidth: scalar, line width of the axes. Defaults to 1;
%   - print_logrank_pvalue: logical, print logrank p-value in the plot.
%     Enabled by default;
%   - print_cox_stats: logical, print overall likelihood ratio test p-value,
%     c-index and hazard ratio(s) with 95% confidence interval(s). Disabled
%     by default;
%   - stats_fontname: char, font name for statitical information. Defaults
%     to 'arial';
%   - stats_fontsize: scalar, font size for statistical information. Defaults to 6;
%   - stats_position: character array, location for statistical infomation.
%     'bottom_left' or 'top_right'. Defaults to 'bottom_left';
%   - stats_box: logical, draw box around statistical information. Disabled
%     by default;
%   - stats_box_color: name of color or array with RGB values, color for the
%     box around the statistical information box (if stats_box is enabled).
%     Defaults to 'w';
%   - risk_table_axes: axes handle, if set, handle to panel to draw risk
%     table in;
%   - has_legend: logical, make legend with names of groups and samples count.
%     Enabled by default;
%   - legend_loc: character array, location for the legend (see legend).
%     Defaults to 'southeast';
%   - colors: cell array, colors for each group. Each cell is either a color
%     name or an array with RGB values. Defaults to {'k', 'b', 'r', 'g', 'm', 'c', 'y'};

p = inputParser();
p.addParameter('groups', categorical(repmat({'all'}, size(y))));
p.addParameter('title', '', @ischar);
p.addParameter('xlabel', 'Time to recurrence [months]', @ischar);
p.addParameter('ylabel', 'Disease-free survival', @ischar);
p.addParameter('xlim', [0 120], @isnumeric);
p.addParameter('xtick', [0:12:120], @isnumeric);
p.addParameter('ylim', [-0.025 1.05], @isnumeric);
p.addParameter('ytick', [0:0.25:1], @isnumeric);
p.addParameter('tick_dir', 'in', @ischar);
p.addParameter('censoredmarkersize', 3, @isnumeric);
p.addParameter('kmlinewidth', 1, @isnumeric);
p.addParameter('kmlinestyles', {'-'}, @iscell);
p.addParameter('box_linewidth', 1, @isnumeric);
p.addParameter('print_logrank_pvalue', true, @islogical);
p.addParameter('print_cox_stats', false, @islogical);
p.addParameter('stats_fontname', 'arial', @ischar);
p.addParameter('stats_fontsize', 6, @isnumeric);
p.addParameter('stats_position', 'bottom_left',...
    @(p_pos) ismember(p_pos, {'bottom_left', 'top_right'}));
p.addParameter('stats_box', false, @islogical);
p.addParameter('stats_box_color', 'w', @isnumeric);
p.addParameter('risk_table_axes', [], @(ax) isa(ax, 'matlab.graphics.axis.Axes'));
p.addParameter('has_legend', true, @islogical);
p.addParameter('legend_loc', 'southeast', @ischar);
p.addParameter('colors', {'k', 'b', 'r', 'g', 'm', 'c', 'y'}, @iscell);
p.parse(varargin{:});

assert(~any(isnan(y)));
assert(islogical(censored));

[new_G, labels, group_t] = ProcessGroups(p.Results.groups);
assert(~(p.Results.print_cox_stats && width(group_t)>1),...
    'Cox stats are only supported when grouping on single variable');

% Cycle through specified colors as needed to produce one color per group:
colors = p.Results.colors(mod(0:numel(labels)-1, numel(p.Results.colors))+1); 

% Create categorical variable with combined groups:
groups = categorical(labels(new_G(~isnan(new_G))), labels);

for i = 1:numel(labels)
    MakeSingleKaplanMaierPlot(...
        y(new_G == i),...
        censored(new_G == i),...
        colors{i},...
        labels{i},...
        p.Results.censoredmarkersize,...
        p.Results.kmlinewidth,...
        p.Results.kmlinestyles{mod(i-1, numel(p.Results.kmlinestyles))+1},...
        p.Results.tick_dir);
end
matlab_utilities.ticklabelformat(gca, 'y', '%.2f');
if p.Results.has_legend
    hl = legend('show');
    set(hl, 'interpreter', 'none', 'Location', p.Results.legend_loc);
end
xlabel(p.Results.xlabel);
ylabel(p.Results.ylabel);
ylim([-0.025 1.05]);
set(gca,...
    'XLim', p.Results.xlim, 'XTick', p.Results.xtick,...
    'YLim', p.Results.ylim, 'YTick', p.Results.ytick,...
    'xcolor', 'k', 'ycolor', 'k',...
    'Box', 'on',...
    'linewidth', p.Results.box_linewidth);
title(p.Results.title, 'interpreter', 'none');
if numel(labels) > 1 &&...
        (p.Results.print_logrank_pvalue || p.Results.print_cox_stats)
    % Make text box with stats
    txt = GetStats(...
        p.Results.print_logrank_pvalue,...
        p.Results.print_cox_stats,...
        y(~isnan(new_G)),...
        censored(~isnan(new_G)),...
        groups);
    % Pad with spaces to avoid overlapping with the axes and ticks.
    txt = cellfun(@(l) [' ', l, ' '], txt, 'UniformOutput', false);
    switch p.Results.stats_position
        case 'bottom_left'
            th = text(0, 0, txt,...
                'units', 'normalized',...
                'HorizontalAlignment', 'left',...
                'VerticalAlignment', 'bottom');
        case 'top_right'
            th = text(1, 1, txt,...
                'units', 'normalized',...
                'HorizontalAlignment', 'right',...
                'VerticalAlignment', 'top');
        otherwise
            error('Unsupported case');
    end
    set(th,...
        'fontname', p.Results.stats_fontname,...
        'fontsize', p.Results.stats_fontsize,...
        'tag', 'logrank_pvalue');
    if p.Results.stats_box
        set(th,...
            'EdgeColor', p.Results.stats_box_color,...
            'linewidth', p.Results.kmlinewidth,...
            'BackgroundColor', p.Results.stats_box_color,...
            'Margin', 0.1);
    end
end
if ~isempty(p.Results.risk_table_axes)
    DrawRiskTable(p.Results.risk_table_axes,...
        y(~isnan(new_G)),...
        groups,...
        p.Results.xtick,...
        colors);
    linkaxes([gca, p.Results.risk_table_axes], 'x');
end
end

function txt = GetStats(print_logrank_pvalue, print_cox_stats,...
    y, censored, groups)
% Helper function to generate text for stats box
% Input arguments:
% - print_logrank_pvalue: Include the overall logrank p-value in stats.
% - print_cox_stats: Include cox stats in stats box.
% - y: numeric column vector, time until event/censoring for each sample;
% - censored: logical, column vector with same size as y indicating censored
%   observations (true);
% - groups: categorical with groups for every patient. When grouping
%   by multiple variables, 'groups' represent the Cartesian product of the
%   groups.

overall_txt = {}; % Elements to go into the first line of the stats box.
additional_lines = {}; % Additional lines for the stats box.

if print_logrank_pvalue
    logrank_stats = OverallAndPairwiseLogrank(y, censored, groups);        
    overall_txt{end+1} = matlab_utilities.FormatPValue(...
        logrank_stats.logrank_p(logrank_stats.sample_set==0),...
        'pvalue_suffix', 'LR');
    if numel(categories(groups)) > 2
        additional_lines = arrayfun(...
            @(r) sprintf('%s{\\it vs} %s: %s',...
            EscapeTex(r.group1), EscapeTex(r.group2),...
            matlab_utilities.FormatPValue(r.logrank_p, 'pvalue_suffix', 'LR')),...
            table2struct(logrank_stats(logrank_stats.sample_set~=0,:)),...
            'UniformOutput', false);
    end
end
if print_cox_stats
    [full_model_stats, per_cox_term_stats, per_cox_term_level_stats] =...
        matlab_utilities.RunMultipleCoxModelsR(...
        categorical(repmat({'default'}, numel(y), 1)),...
        y(:), ~censored(:), table(groups));
    per_group_stats = innerjoin(per_cox_term_stats, per_cox_term_level_stats,...
        'Keys', {'sample_set', 'cox_term'});
    
    overall_txt{end+1} = matlab_utilities.FormatPValue(...
        full_model_stats.lrt_p, 'pvalue_suffix', 'LRT');
    overall_txt{end+1} = sprintf('c_i=%.2f', full_model_stats.c_index);
    
    if print_logrank_pvalue && numel(categories(groups)) > 2
        % Printing both cox stats and logrank pvalues.
        % We include only the logrank pvalues from the pairwise comparison
        % in the cox model.
        combined_stats = innerjoin(per_group_stats, logrank_stats,...
            'LeftKeys', {'ref_level', 'cox_term_level'},...
            'RightKeys', {'group1', 'group2'});
        additional_lines = arrayfun(...
            @(r) sprintf('%s{\\it vs} %s: HR %.2f (%.2f-%.2f), %s',...
            EscapeTex(r.cox_term_level), EscapeTex(r.ref_level),...
            r.hr, r.low_ci, r.high_ci,...
            matlab_utilities.FormatPValue(r.logrank_p, 'pvalue_suffix', 'LR')),...
            table2struct(combined_stats),...
            'UniformOutput', false);
    else
        additional_lines = arrayfun(...
            @(r) sprintf('%s{\\it vs} %s: HR %.2f (%.2f-%.2f)',...
            EscapeTex(r.cox_term_level), EscapeTex(r.ref_level), r.hr, r.low_ci, r.high_ci),...
            table2struct(per_group_stats),...
            'UniformOutput', false);
    end
end

txt = [strjoin(overall_txt, ', ');additional_lines];
end

function logrank_stats = OverallAndPairwiseLogrank(y, censored, groups)
% Compute overall and pairwise logrank pvalues between all groups.
% Input arguments:
% - y: numeric column vector, time until event/censoring for each sample;
% - censored: logical, column vector with same size as y indicating censored
%   observations (true);
% - groups: categorical with groups for every patient. When grouping
%   by multiple variables, 'groups' represent the Cartesian product of the
%   groups.
% Output:
% - logrank_stats: table, 1 row for overall stats and 1 row per pair of
%   groups. Columns: 'group1', 'group2', 'sample_set', 'logrank_p'.
%   sample_set is 0 and group1 and group2 are empty for overall stats.

group_labels = categories(groups);
group_combos = cell2table(...
    nchoosek(group_labels, 2),...
    'VariableNames', {'group1', 'group2'});
group_combos.sample_set = arrayfun(@num2str, 1:height(group_combos), 'UniformOutput', false)';
t = table(y, censored, groups);
pairwise_inputs = rowfun(@(g1, g2) t(ismember(t.groups, [g1, g2]),:),...
    group_combos,...
    'GroupingVariables', 'sample_set', 'OutputFormat', 'cell');
logrank_inputs = matlab_utilities.ConcatenateTables(...
    [{t}; pairwise_inputs],...
    'new_column', 'sample_set',...
    'new_column_values', ['0'; group_combos.sample_set]);
logrank_stats = matlab_utilities.RunMultipleLogranksR(...
    logrank_inputs.sample_set,...
    logrank_inputs.y,...
    ~logrank_inputs.censored,...
    logrank_inputs.groups);
logrank_stats = outerjoin(group_combos, logrank_stats, 'key', 'sample_set');
logrank_stats.sample_set = str2double(logrank_stats.sample_set_logrank_stats);
% Restore original order:
logrank_stats = sortrows(logrank_stats, 'sample_set');
logrank_stats(:,{'sample_set_group_combos','sample_set_logrank_stats'}) = [];
end

function MakeSingleKaplanMaierPlot(y, censored, color, legend_label, markersize, kmlinewidth, kmlinestyle, tick_dir)
% Make a Kaplan-Meier plot for a single group.
% Input arguments:
% - y: numeric column vector, time until event/censoring for each sample;
% - censored: logical, column vector with same size as y indicating censored
%   observations (true);
% - color: color name or an array with RGB values;
% - legend_label: character array, name of the group;
% - markersize: scalar, size of the markers for censoring;
% - kmlinewidth: scalar, line width for the Kaplan-Meier line;
% - kmlinestyle: character array, line style for the group;
% - tick_dir: char, direction of the tick marks. 'in' or 'out';

[f, x] = ecdf(y, 'censoring', censored, 'function', 'survivor');
censored_x = y(censored);
ecdf(y, 'censoring', censored, 'function', 'survivor', 'bounds', 'off');
set(gca, 'TickDir', tick_dir);

% Add manually markers for censored data
h = get(gca, 'children');
h = h(1); % GCA may have multiple children, but the ecdf line will be the first one listed since it was added last
xx = get(h, 'XData');
if (xx(1) > 0)
    set(h, 'XData', [0, get(h, 'XData')]);
    yy = get(h, 'YData');
    set(h, 'YData', [yy(1), yy]);
end
if (xx(end) < max(censored_x))
    set(h, 'XData', [get(h, 'XData'), max(censored_x)]);
    yy = get(h, 'YData');
    set(h, 'YData', [yy, yy(end)]);
end

hold on;

if ~isempty(censored_x)
    for i = 1:numel(censored_x)
        censored_y(i) = f(sum(x <= censored_x(i))); %#ok<AGROW>
    end
    hg = hggroup('Parent', gca);
    plot(hg, censored_x, censored_y, '+',...
        'Color', color, 'MarkerSize', markersize, 'LineWidth', kmlinewidth);
end

set(h,...
    'color', color,...
    'linewidth', kmlinewidth,...
    'linestyle', kmlinestyle,...
    'DisplayName', sprintf('%s (n=%d)', legend_label, numel(y)));
end

function [new_G, labels, group_t] = ProcessGroups(G)
% grp2idx-like function that also handles cell-arrays of multiple grouping
% variables.
% Input arguments:
% - G: categorical-, numeric- or logical- column vector, or a cell array of
%   column vectors, grouping information;
% Outputs:
% - new_G: column vector with group indices for each group;
% - labels: cell array with label to use for each group in new_G;
% - group_t: table with groups. One row per group and one column per
%   group feature.

if iscell(G)
    group_t = table(G{:});
    stats = grpstats(group_t, group_t.Properties.VariableNames, [], 'DataVars', false);
    stats.idx = (1:height(stats))';
    G = join(group_t, stats);
    new_G = G.idx;
    labels = rowfun(@(s) strjoin(cellstr(s), '-'), stats,...
        'InputVariables', group_t.Properties.VariableNames,...
        'SeparateInputs', false, 'OutputFormat', 'cell');
else
    group_t = table(G);
    [new_G, labels] = grp2idx(G);
end
end

function s = EscapeTex(s)
% Replace tex special characters with their escaped version.
% Input arguments:
% - s: character array, string to escape.

s=regexprep(s, '([_{}^\\])', '\\$0');
end

function DrawRiskTable(ax, y, groups, xticks, colors)
% Draw a risk table for the Kaplan Meier plot
% Input arguments:
% - ax: axes handle, axes to draw risk table in;
% - y: numeric column vector, time until event/censoring for each sample;
% - groups: categorical with groups for every patient. When grouping
%   by multiple variables, 'groups' represent the Cartesian product of the
%   groups.
% - xticks: numeric vector, x positions to annotate risks for;
% - colors: cell array, colors for each group. Each cell is either a color
%   name or an array with RGB values.

% For every position in xticks, count how many samples are left for each
% group:
risks = cell2mat(...
    arrayfun(@(xtick) reshape(countcats(groups(y>=xtick)), [], 1), xticks,...
    'UniformOutput', false));
risks_str = arrayfun(@(e) num2str(e), risks, 'UniformOutput', false);

% Add the text:
% Reshape input arguments as column vectors.
% Use y coordinates -0.5,-1.5,... so multiple risk tables with different
% number of groups can be aligned by adjusting ylim.
h = text(...
    reshape(repmat(xticks, size(risks, 1), 1), [], 1),...
    reshape(repmat(-0.5:-1:-size(risks,1), 1, size(risks, 2)), [], 1),...
    reshape(risks_str, [], 1),...
    'parent', ax,...
    'HorizontalAlignment', 'right',...
    'tag', 'risk_table');
set(h, {'Color'}, repmat(colors(:), size(risks, 2), 1));
set(ax,...
    'YLim', [-size(risks, 1), 0],...
    'XColor', 'none',...
    'YColor', 'none',...
    'color', 'none');
end
