# Code book for crc_reference_cohort.csv

Dataset with protein expression measured by quantitative Western blotting (qWB)
in tumor resections from colorectal cancer patients described in [Clinical
application of a systems model of apoptosis execution for the prediction of
colorectal cancer therapy responses and personalisation of therapy, (Hector
et al., GUT, 2012)](https://www.ncbi.nlm.nih.gov/pubmed/22082587).

The dataset has one row per patient and columns:

- universal_patient_id: unique identifier for each patient;
- stage: colorectal cancer disease stage defined according to the 
  [tumour/node/metastasis (TNM) system](https://cancerstaging.org/references-tools/quickreferences/Documents/ColonMedium.pdf)
  by the American Joint Committee on Cancer (AJCC), either
    - '2';
    - '3';
- apaf1: concentration of APAF1 (in uM);
- caspase3: concentration of Procaspase-3 (in uM);
- caspase9: concentration of Procaspase-9 (in uM);
- smac: concentration of SMAC (in uM);
- xiap: concentration of XIAP (in uM);

For more information, refer to the manuscript.
