function a = DummyAggregator(a)
% Aggregation function that checks that a single number is being aggregated.
% Useful when unstacking tables that are supposed to only have unique groups.
% Input argument:
% - a: array with values to aggregate.
% See also unstack.

assert(numel(a) == 1)
end
