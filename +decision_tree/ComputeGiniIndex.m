function gini_index = ComputeGiniIndex(groups, classes)
% Function to calculate GINI index for a possible split.
% Input arguments:
% - groups: logical column vector, group of each sample;
% - classes: logical column vector, class of each sample;
% Output:
% - gini_index: cost, scalar. It is 0 for a perfect split and 0.5 for a
%   random split.

assert(size(groups, 2)==1, 'Groups need to be a column vector');
assert(size(classes, 2)==1, 'Groups need to be a column vector');
assert(islogical(groups));
assert(islogical(classes));

% Inspired by https://machinelearningmastery.com/implement-decision-tree-algorithm-scratch-python/
% gini_index = (1.0 - sum(proportion * proportion)) * (group_size/total_samples)
gini_index = (GiniForGroup(classes(groups==true)) + GiniForGroup(classes(groups==false)))...
    / numel(groups);
end

function sub_gini_index = GiniForGroup(classes_for_group)
% Calculate gini index contribution from one group.
% Input argument:
% - classes_for_group: logical vector, classes for the samples in this group;
% Output argument:
% - sub_gini_index: gini_index contribution from this group.

sub_gini_index = (1-sum(([sum(classes_for_group==true), sum(classes_for_group==false)] ./ numel(classes_for_group)).^2))...
    * numel(classes_for_group);
end
