function ParameterSweep(t, proteins, filename)
% Build trees with different tree-building settings.
% Build one tree for each combination of possible confidence_threshold and
% costs_per_new_protein settings.
% Input arguments:
% - t: table, training data for trees, as retuned by 'decision_tree.GetTreeTrainingData';
% - proteins: cell array of strings, names of proteins to use in decision trees;
% - filename: string, name of matfile to save results in.

% Build one tree for each combination of possible confidence_threshold and
% costs_per_new_protein settings.
confidence_thresholds = 0.6:0.001:1;
costs_per_new_protein = [0, 0.5];
[confidence_thresholds, costs_per_new_protein] = meshgrid(...
    confidence_thresholds, costs_per_new_protein);

% Run simulations in random order to get accurate progress estimates.
idx = randperm(numel(confidence_thresholds));
[~, reverse_idx] = sort(idx);
confidence_thresholds = reshape(confidence_thresholds(idx), size(confidence_thresholds));
costs_per_new_protein = reshape(costs_per_new_protein(idx), size(costs_per_new_protein));

pb = matlab_utilities.ProgressBar(numel(confidence_thresholds),...
    'building_trees', 'enable_gui',false);

accuracy = nan(size(confidence_thresholds));
average_proteins_measured = nan(size(confidence_thresholds));
tree_height = nan(size(confidence_thresholds));
tree_size = nan(size(confidence_thresholds));
parfor i=1:numel(confidence_thresholds)
    tree{i} = decision_tree.BuildTree(t(:,proteins),...
        t.sc_class,...
        'confidence_threshold', confidence_thresholds(i),...
        'cost_per_new_protein', costs_per_new_protein(i)); %#ok<PFBNS>
    predictions = tree{i}.Predict(t);
    accuracy(i) = mean(predictions.predicted_class == categorical(predictions.sc_class));
    average_proteins_measured(i) = mean(cellfun(@numel, predictions.proteins_measured));
    [~, parent_pointers] = tree{i}.root_node.GetTreeLayout();
    [~,~,tree_height(i)] = treelayout(parent_pointers);
    tree_size(i) = numel(parent_pointers);
    pb.update(); %#ok<PFBNS>
end
clear pb;

% Restore index order
confidence_thresholds = reshape(confidence_thresholds(reverse_idx), size(confidence_thresholds));
costs_per_new_protein = reshape(costs_per_new_protein(reverse_idx), size(costs_per_new_protein));
accuracy = reshape(accuracy(reverse_idx), size(accuracy));
average_proteins_measured = reshape(average_proteins_measured(reverse_idx), size(average_proteins_measured));
tree_height = reshape(tree_height(reverse_idx), size(tree_height));
tree_size = reshape(tree_size(reverse_idx), size(tree_size));
tree = reshape(tree(reverse_idx), size(tree));

save(fullfile('outputs', filename),...
    'proteins', 'confidence_thresholds', 'costs_per_new_protein', 'accuracy',...
    'average_proteins_measured', 'tree_height', 'tree_size', 'tree', 't');
end
