classdef Tree
    % Data structure representing a classification decision tree.
    
    properties
        root_node; % Instance of 'decision_tree.Node' class for the root node of the tree;
    end
    
    methods
        
        function obj = Tree(root_node)
            % Constructor for 'decision_tree.Tree'.
            % Input argument:
            % - root_node: instance of 'decision_tree.Node' class, root node
            %   for the tree.
            
            obj.root_node = root_node;
        end
        
        function plot(obj)
            % Plot decision tree visualization in gca.
            % See also treelayout.
            
            [labels, parent_pointers] = obj.root_node.GetTreeLayout();
            treeplot(parent_pointers);
            [x,y] = treelayout(parent_pointers);
            text(x,y,labels);
            set(gca, 'XTick', [], 'YTick', []);
        end
        
        function predictions = Predict(obj, predictors)
            % Predict classification for a set of samples.
            % Input arguments:
            % - predictors: table, one column per predictor and one row per sample
            %   to make predictions for;
            % Output:
            % - predictions: table, predictors with added columns for:
            %   - predicted_class: categorical, predicted class for each sample;
            %   - predicted_confidence: scalar (0-1), confidence in the predicted
            %     class for each sample;
            %   - proteins_measured: cell array of strings, the set of proteins
            %     that needed to be evaluated to make predictions for each sample.
            
            predictions = obj.root_node.Predict(predictors, {});
        end
        
        function ExportGraphviz(obj, plot_filename, varargin)
            % Export visualization of the tree with Graphviz.
            % Input arguments:
            % - plot_filename: string, name of file to write svg file to;
            % - key/value pairs:
            %   - color: string, graphviz color name for tree layout
            %     (https://www.graphviz.org/doc/info/colors.html);
            %   - custom_node_option_cb: function handle, callback function
            %     for setting custom options for each node in the tree.
            %     Function should take single 'decision_tree.Node' input
            %     argument and return string with graphviz options.
            
            ip = inputParser();
            ip.addParameter('color', 'black', @ischar);
            ip.addParameter('custom_node_option_cb', @(n) '', @(v) isa(v, 'function_handle')); 
            ip.parse(varargin{:});
            
            dot_filename = [tempname(), '.dot'];
            file_id = fopen(dot_filename, 'w');
            fprintf(file_id, [...
                'digraph tree {\n' ...
                'newrank=true\n' ...
                'nodesep=0.1\n' ...
                'ranksep=0.1\n' ...
                'node [shape=plaintext,color="%s",fontcolor="%s",fontsize=8,fontname="arial",width=0,height=0,margin=0]\n' ...
                'edge [arrowsize=0.5,color="%s",fontcolor="%s",fontsize=6,fontname="arial",]\n' ...
                'START [shape=Mdiamond]\n'],...
                ip.Results.color, ip.Results.color, ip.Results.color, ip.Results.color);
            obj.root_node.ExportGraphviz(file_id, 'root', ip.Results);
            fprintf(file_id, [...
                'START -> root\n' ...
                '}\n']);
            fclose(file_id);
            [~, ~, ext] = fileparts(plot_filename);
            ext = ext(2:end);
            % dot must be in PATH (enviromental variable set)
            cmd_line = sprintf('dot -o%s -T%s %s', plot_filename, ext, dot_filename);
            [status, output] = system(cmd_line);
            delete(dot_filename);
            if status ~= 0
                error('error occured while running dot: %s', output);
            end
        end
    end
end
