function split_point = FindBestSplit(predictors, classes,...
    proteins_previously_measured, cost_per_new_protein, confidence_threshold, purity_discount)
% Function to identify the best split point.
% Input arguments:
% - predictors: table, one row per sample and one numerical column for each
%   predictor;
% - classes: logical column vector, class of each sample;
% - proteins_previously_measured: cell array of strings, proteins previously
%   measured;
% - cost_per_new_protein: scalar, penalty associated with measuring a new
%   protein;
% - confidence_threshold: scalar (range 0-1), confidence threshold over
%   which we consider the node pure;
% - purity_discount: scalar, reduction in cost for pure splits;
% Output:
% - split_point: instance of class SplitPoint.

    function cost = EvaluateSplit(sp)
        % Function to evaluate a split and assign a cost.
        % Input argument:
        % - sp: instance of 'decision_tree.SplitPoint';
        % Output:
        % - cost: double, lower for better splits.
        
        groups = sp.Split(predictors);
        gini_index = decision_tree.ComputeGiniIndex(groups, classes);
        measurememnt_cost = ~ismember(sp.predictor_name, proteins_previously_measured) .*...
            cost_per_new_protein;
        purity_cost = - PurityFraction(groups, classes, confidence_threshold) .*...
            purity_discount;
        cost = gini_index + measurememnt_cost + purity_cost;
    end

splits = decision_tree.ComputeAllSplits(predictors);
if numel(splits) > 0
    costs = cellfun(@EvaluateSplit, splits);
    [~, idx] = min(costs);
    split_point = splits{idx};
else
    error('decision_tree:NoPossibleSplits', 'Unable to find a split for predictor');
end

end

function purity_fraction = PurityFraction(groups, classes, confidence_threshold)
% Calculate what fraction of the samples go in a group with higher than 
% confidence_threshold purity.
% Input arguments:
% - groups: logical column vector, group of each sample;
% - classes: logical column vector, class of each sample,
% - confidence_threshold: scalar (range 0-1), confidence threshold over
%   which we consider the node pure;
% Output:
% - purity_fraction: scalar, fraction of samples where we have higher than 
%   confidence_threshold purity and predictions match golden APOPTO-CELL.

purity_fraction = (...
    PurityMatchesInGroup(classes(groups==true), confidence_threshold) +...
    PurityMatchesInGroup(classes(groups==false), confidence_threshold)) /...
    numel(classes);
end

function confident_matches = PurityMatchesInGroup(classes_for_group, confidence_threshold)
% Calculate the number of samples in the group if the purity for the group is
% higher than the threshold
% Input arguments:
% - classes_for_group: logical vector, classes for the samples in this group;
% - confidence_threshold: scalar (range 0-1), confidence threshold over
%   which we consider the node pure;
% Output:
% - confident_matches: scalar, the number of samples in pure nodes in the group.

purity_fraction = mean(classes_for_group);
if purity_fraction < 0.5
    purity_fraction = 1 - purity_fraction;
end
if purity_fraction > confidence_threshold
    confident_matches = purity_fraction * numel(classes_for_group);
else
    confident_matches = 0;
end
end
