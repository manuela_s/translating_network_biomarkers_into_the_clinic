classdef InternalNode < decision_tree.Node
    % Class representing an internal node in a classification decision tree.
    
    properties
        split_point; % 'decision_tree.SplitPoint' instance to decide if samples belong in left or right node.
        left_node; % 'decision_tree.Node' instance for the left child, for samples where predictor_name is < split_point;
        right_node; % 'decision_tree.Node' instance for the right child, for samples where predictor_name is >= split_point;
    end
    
    methods
        function obj = InternalNode(split_point, left_node, right_node)
            % Constructor for 'decision_tree.InternalNode'.
            % Input arguments:
            % - split_point: instance of 'decision_tree.SplitPoint' class,
            %   used to decide if samples belong in left or right node;
            % - left_node: instance of 'decision_tree.Node' class,
            %   for samples where predictor_name is < split_point;
            % - right_node: instance of 'decision_tree.Node' class,
            %   for samples where predictor_name is >= split_point.
            
            obj.split_point = split_point;
            obj.left_node = left_node;
            obj.right_node = right_node;
        end
        
        function [labels, parent_pointers] = GetTreeLayout(obj)
            [left_labels, left_pointers] = obj.left_node.GetTreeLayout();
            % Increment pointers by 1, since left_pointers are going to
            % start at position 2.
            left_pointers = left_pointers + 1;
            [right_labels, right_pointers] = obj.right_node.GetTreeLayout();
            % Increment pointers by 1 + number of left pointers.
            right_pointers(right_pointers > 0) = ...
                right_pointers(right_pointers > 0) + 1 + numel(left_pointers);
            right_pointers(right_pointers == 0) = 1;
            
            labels = [{obj.GetLabel()}, left_labels, right_labels];
            parent_pointers = [0, left_pointers, right_pointers];
        end
        
        function label = GetLabel(obj)
            label = obj.split_point.GetLabel();
        end
        
        function predictions = Predict(obj, predictors, proteins_previously_measured)
            proteins_measured = unique([proteins_previously_measured,...
                cellstr(obj.split_point.predictor_name)]);
            on_left = obj.split_point.Split(predictors);
            predictions(on_left,:) =...
                obj.left_node.Predict(predictors(on_left,:), proteins_measured);
            predictions(~on_left,:) =...
                obj.right_node.Predict(predictors(~on_left,:), proteins_measured);
        end
        
        function ExportGraphviz(obj, file_id, node_id, settings)
            fprintf(file_id, '  %s [label="%s",%s]\n', node_id,...
                obj.split_point.predictor_name,...
                settings.custom_node_option_cb(obj));
            obj.left_node.ExportGraphviz(file_id, strcat(node_id, '_l'), settings);
            obj.right_node.ExportGraphviz(file_id, strcat(node_id, '_r'), settings);
            fprintf(file_id, '  %s -> %s [label=<&lt;%d<SUP>th</SUP>>]\n',...
                node_id, strcat(node_id, '_l'),obj.split_point.split_point);
            fprintf(file_id, '  %s -> %s [label=<&ge;%d<SUP>th</SUP>>]\n',...
                node_id, strcat(node_id, '_r'),obj.split_point.split_point);
        end
    end
    
end
