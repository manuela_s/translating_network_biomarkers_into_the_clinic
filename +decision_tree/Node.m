classdef (Abstract) Node
% Abstract class representing a node in a classification decision tree.

methods (Abstract)
    % Get tree layout for a node and sub-nodes in form suitable for plotting.
    % Outputs:
    % - labels: cell array of strings, labels for each node (including children)
    % - parent_pointers: vector of integers, one element per node in tree. Every
    %   element has the index of the parent of that node. See matlab treeplot.
    GetTreeLayout(obj)
    
    % Get a label string for the given node.
    % Outputs:
    % - label: string, label for this node.
    GetLabel(obj)
    
    % Traverse sub-tree and predict classification.
    % Input arguments:
    % - predictors: table, one column per predictor and one row per sample
    %   to make predictions for;
    % - proteins_measured: cell array of strings, proteins previously
    %   measured;
    % Output:
    % - predictions: table, predictors with added columns for:
    %   - predicted_class: categorical, predicted class for each sample; 
    %   - predicted_confidence: scalar (0-1), confidence in the predicted
    %     class for each sample; 
    %   - proteins_measured: cell array of strings, the set of proteins
    %     that needed to be evaluated to make predictions for each sample.
    Predict(obj, predictors, proteins_measured);
    
    % Export visualization of sub-tree with Graphviz.
    % Input arguments:
    % - file_id: file identifier, file handle (as returned by fopen) for
    %   the .dot file to write to;
    % - node_id: string, graphviz node identifier to use for this node;
    % - settings: struct, settings for graphviz export from
    %   'decision_tree.Tree.ExportGraphviz' inputParser.
    ExportGraphviz(obj, file_id, node_id, settings);
end
end

