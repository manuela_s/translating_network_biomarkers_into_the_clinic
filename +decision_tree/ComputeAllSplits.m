function splits = ComputeAllSplits(predictors)
% Function to calculate the split points for all the predictors.
% Input arguments:
% - predictors: table, one row for each sample and one column for each predictor;
% Output:
% - splits: cell array of instances of 'decision_tree.SplitPoint' class.

predictor_names = predictors.Properties.VariableNames;
for p = 1:numel(predictor_names)
    split_points{p} = arrayfun(@(v) decision_tree.SplitPoint(predictor_names{p}, v),...
        decision_tree.ComputeSinglePredictorSplitPoints(...
        predictors.(predictor_names{p})),...
        'UniformOutput', false);  %#ok<AGROW>
end
splits = vertcat(split_points{:});
end
