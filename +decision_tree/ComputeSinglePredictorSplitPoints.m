function split_points = ComputeSinglePredictorSplitPoints(predictor)
% Function to calculate the split points for a single predictor.
% Input arguments:
% - predictor: column vector, numeric predictor values;
% Output:
% - split_points: vector of double, possible split_points for the predictor.

assert(size(predictor,2)==1, 'Predictor must be a column vector');
unique_values = unique(predictor);
if numel(unique_values)>1
    split_points = mean([unique_values(1:end-1), unique_values(2:end)], 2);
else
    split_points = [];
end
end
