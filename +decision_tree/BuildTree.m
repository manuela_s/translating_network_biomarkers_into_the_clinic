function tree = BuildTree(predictors, classes, varargin)
% Function to build a classification decision tree.
% Input arguments:
% - predictors: table, data to build the tree on with one column per predictor;
% - classes: categorical column vector, classes for each sample;
% - key/value pair:
%   - confidence_threshold: scalar, confidence threshold over which we stop
%     measuring additional proteins;
%   - cost_per_new_protein: scalar, penalty associated with measuring a new
%     protein;
%   - purity_discount: scalar, reduction in cost for pure splits;
% Output:
% - tree: instance of Tree class.

ip = inputParser();
ip.addParameter('confidence_threshold', 1, @isnumeric);
ip.addParameter('cost_per_new_protein', 0, @isnumeric);
ip.addParameter('purity_discount', 0, @isnumeric);
ip.parse(varargin{:});

assert(numel(categories(classes))==2, 'Classes need to have exactly two categories');
root_node = BuildNode(predictors, classes, {}, ip.Results);
tree = decision_tree.Tree(root_node);
end

function node = BuildNode(predictors, classes, proteins_previously_measured, options)
% Recursively build node for decision tree.
% Input arguments:
% - predictors: table, data to build the tree on with one column per predictor;
% - classes: categorical column vector, classes for each sample;
% - proteins_previously_measured: cell-array of strings, names of proteins
%   used to split nodes higher up in the decision tree. Splitting the tree
%   based on a protein not already measured incurs an extra cost;
% - options: struct, options for tree building from BuildTree inputParser;
% Output:
% - node: instance of 'decision_tree.Node', the root of the (sub-) tree.

cats = categories(classes);
counts = countcats(classes);
[max_value, max_idx] = max(counts);
confidence = max_value / sum(counts);
predicted_class = cats{max_idx};
if confidence >= options.confidence_threshold
    node = decision_tree.LeafDecisionNode(predicted_class, confidence);
else
    try
        split_point = decision_tree.FindBestSplit(...
            predictors, classes==predicted_class, proteins_previously_measured,...
            options.cost_per_new_protein, options.confidence_threshold,...
            options.purity_discount);
    catch err
        if (strcmp(err.identifier,'decision_tree:NoPossibleSplits'))
            node = decision_tree.LeafDecisionNode(predicted_class, confidence);
            return
        else
            rethrow(err);
        end
    end
    groups = split_point.Split(predictors);
    proteins_measured = unique([proteins_previously_measured, {split_point.predictor_name}]);
    node = decision_tree.InternalNode(...
        split_point,...
        BuildNode(predictors(groups==true, :), classes(groups==true), proteins_measured, options),...
        BuildNode(predictors(groups==false, :), classes(groups==false), proteins_measured, options));
end
end
