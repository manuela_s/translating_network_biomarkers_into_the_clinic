classdef SplitPoint
    % Class representing a split-point for an internal node in a decision
    % tree.
    
    properties
        predictor_name; % String, name of the predictor to split on;
        split_point; % Scalar, value of the predictor to split on;
    end
    
    methods
        function obj = SplitPoint(predictor_name, split_point)
            % Constructor for 'decision_tree.SplitPoint'.
            % Input arguments:
            % - predictor_name: string, name of the predictor to split on;
            % - split_point: scalar, value of the predictor to split on.
            
            obj.predictor_name = predictor_name;
            obj.split_point = split_point;
        end
        
        function label = GetLabel(obj)
            % Get a label string for the split point.
            % Outputs:
            % - label: string, label for the split point.
            
            label = sprintf('%s < %g', obj.predictor_name, obj.split_point);
        end
        
        function groups = Split(obj, predictors)
           % Splits the data in predictors into 2 sets.
           % Input arguments:
           % - predictors: table with one row per sample and one continuous
           %   column per predictor;
           % Output:
           % - groups: column vector of logicals, true for left node and
           %   false for right node.
           
           groups = predictors.(obj.predictor_name) < obj.split_point;
        end
    end
    
end
