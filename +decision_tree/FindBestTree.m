function [idx, per_protein_penalty] = FindBestTree(accuracy, average_proteins_measured)
% Identify tree with best trade-off between accuracy and number of required
% proteins.
% Input arguments:
% - accuracy: double matrix, accuracy [0, 1] for each candidate tree;
% - average_proteins_measured: double matrix with same shape as accuracy,
%   average number of proteins required to make predictions for each 
%   candidate tree; 
% Outputs:
% - idx: scalar, index to optimal tree in 'accuracy' and 'average_proteins_measured';
% - per_protein_penalty: scalar, percentage points of accuracy improvement
%   needed to offset the requirement of one extra protein in average.

% Apply score penalty equivalent to 5% accuracy improvement, for every
% extra protein measured:
per_protein_penalty = 5;

% 'score' increases when accuracy increases and decreases when number of 
% required proteins increases
score = 100 .* accuracy - per_protein_penalty .* average_proteins_measured;
[~, idx] = max(score(:));
end
