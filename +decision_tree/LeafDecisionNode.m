classdef LeafDecisionNode < decision_tree.Node
    % Class representing a leaf node in a classification decision tree.
    
    properties
        predicted_class; % String, name of the predicted class;
        predicted_confidence; % scalar (0-1), confidence in the predicted class;
    end
    
    methods
        function obj = LeafDecisionNode(predicted_class, predicted_confidence)
            % Constructor for 'decision_tree.LeafDecisionNode'.
            % Input arguments:
            % - predicted_class: string, name of the predicted class for
            %   the node;
            % - predicted_confidence: scalar (0-1), confidence in the
            %   predicted class.
            
            assert(ischar(predicted_class));
            obj.predicted_class = predicted_class;
            obj.predicted_confidence = predicted_confidence;
        end
        
        function [labels, parent_pointers] = GetTreeLayout(obj)
            labels = {obj.GetLabel()};
            parent_pointers = 0;
        end
        
        function label = GetLabel(obj)
            label = sprintf('%s\n%.0f%%',...
                obj.predicted_class,...
                100 .* obj.predicted_confidence);
        end
        
        function predictions = Predict(obj, predictors, proteins_measured)
            predictions = predictors;
            predictions.predicted_class = categorical(...
                repmat({obj.predicted_class}, height(predictors), 1));
            predictions.predicted_confidence =...
                repmat(obj.predicted_confidence, height(predictors), 1);
            predictions.proteins_measured =...
                repmat({proteins_measured}, height(predictions), 1);
        end
        
        function ExportGraphviz(obj, file_id, node_id, settings)
            fprintf(file_id, '  %s [label="%s",width=.1,height=.1,shape=rectangle,%s]\n',...
                node_id, obj.GetLabel(), settings.custom_node_option_cb(obj));
        end
    end
end
