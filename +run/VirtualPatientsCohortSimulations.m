function VirtualPatientsCohortSimulations()
% Run APOPTO-CELL simulations for a virtual patients cohort.
% The cohort consists of virtual patients with all combinations of protein
% concentrations for the 5 APOPTO-CELL protein inputs from
% 'inputs.mat'
% Generates virtual_patients_cohort_simulations.mat with fields:
% - 'inputs': struct, as read in from 'inputs.mat';
% - 'settings': struct, settings (duration and stepsize, in min) for 
%    APOPTO-CELL simulations (see 'cohort_utilities.ApoptoCell');
% - 'steps': scalar, number of protein concentration levels to simulate for
%   each protein; 
% - 'num_simulations': scalar, number of simulations (steps^5);
% - 'percentiles': vector of double, protein concentrations in percentiles;
% - 'apaf1', 'caspase3', 'caspase9', 'smac', 'xiap': 5D tensor, protein
%   concentrations in absolute units (uM);
% - 'substrate_cleavage_end': 5D tensor, substrate cleavage predicted by
%   APOPTO-CELL with protein inputs given by 'apaf1', 'caspase3', 'caspase9',
%   'smac', 'xiap'.

% Set simulation settings
settings.duration = 300; %[min]
settings.stepsize = 1;   %[min]

% Get protein concentrations
inputs = load(fullfile('outputs', 'inputs.mat'));
percentiles = inputs.percentiles; %#ok<NASGU>
steps = numel(inputs.percentiles);
proteins = inputs.proteins;

% Construct full combinatorial grid of protein concentrations to use as
% protein inputs to run APOPTO-CELL simulations
[caspase3, caspase9, xiap, apaf1, smac] = ndgrid(...
    proteins{'caspase3', 'concs'},...
    proteins{'caspase9', 'concs'},...
    proteins{'xiap', 'concs'},...
    proteins{'apaf1', 'concs'},...
    proteins{'smac', 'concs'});

% Run simulations
num_simulations = steps^5;
substrate_cleavage_end = zeros(num_simulations, 1);
pb = matlab_utilities.ProgressBar(...
    num_simulations, '5D proteins APOPTO-CELL simulations',...
    'enable_gui', false);
parfor i=1:num_simulations
    concs = [caspase3(i), caspase9(i), xiap(i), apaf1(i), smac(i), 0];
    traces = cohort_utilities.ApoptoCell(concs, settings);
    substrate_cleavage_end(i) = traces(end, 3);
    pb.update(); %#ok<PFBNS>
end
clear pb;

% Reshape substrate_cleavage_end as 5D tensor corresponding to the protein
% variables ('apaf1', 'caspase3', 'caspase9', 'smac' and 'xiap')
substrate_cleavage_end = reshape(...
    substrate_cleavage_end, repmat(steps, 1, 5)); %#ok<NASGU>

% Save results
save(...
    fullfile('outputs', 'virtual_patients_cohort_simulations.mat'),...
    '-v7.3');
end
