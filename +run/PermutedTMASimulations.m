function PermutedTMASimulations(varargin)
% Run APOPTO-CELL simulations with permuted TMA protein concentrations.
% For each repeatition, protein concentrations measured by TMA (caspase3
% and xiap) are randomly assigned to patients, to test the null-hypothesis
% that TMA-based measurements do not contribute to the accuracy of the
% APOPTO-CELL simulation.
% Input argument:
% - key/value pair:
%   - n_repeats: scalar, number of repeatitions to run APOPTO-CELL simulations
%     with permuted TMA protein concentrations.

ip = inputParser();
ip.addParameter('n_repeats', 10000, @isnumeric);
ip.parse(varargin{:});

n_repeats = ip.Results.n_repeats;

inputs = load(fullfile('outputs', 'inputs.mat'), 'proteins', 'rppa_and_tma');
simulations = inputs.rppa_and_tma;
simulations.apaf1 = repmat(...
    inputs.proteins{'apaf1', 'nanmedian_concentration'}, height(simulations), 1);

simulations = PermuteProteins(simulations, {'caspase3','xiap'}, n_repeats);

sims = cohort_utilities.SimulateCohort(simulations,...
    'keep_traces', false,...
    'progressbar_label', 'PermutedTMASimulations');
simulations.substrate_cleavage_end = [sims.results.substrate_cleavage_end]';
simulations.substrate_cleavage_class = categorical(...
    matlab_utilities.iif(simulations.substrate_cleavage_end>25, 'SC>25%', 'SC<=25%'));

% Save simulation inputs and results
save(fullfile('outputs', 'permuted_tma_simulations.mat'),...
    'simulations', '-v7.3');
end

function permuted_simulation_inputs = PermuteProteins(...
    simulation_inputs,protein_names,n_repeats)
%% Randomize protein concentrations between patients for given proteins.
% Input arguments:
% - simulation_inputs: table, inputs for simulations, with one row per
%   patient. Must include one column per protein to permute;
% - protein_names: cell array, names of proteins to permute;
% - n_repeats: scalar, number of repeatitions for permutation process.
% Output:
% - permuted_simulation_inputs: table, simulation_inputs repeated n_repeats
%   times, with permuted protein concentrations. Adds column 'permutation'
%   with index 1:n_repeats.

% Set seed to get reproducible permutations
rng(0);

for i = 1:n_repeats
    sub_t{i} = simulation_inputs; %#ok<AGROW>
    for p = 1:numel(protein_names)  
        randomized_idx = randi(height(sub_t{i}), [height(sub_t{i}), 1]);
        sub_t{i}.(protein_names{p}) = sub_t{i}.(protein_names{p})(randomized_idx);
    end
    sub_t{i}.permutation = categorical(...
        repmat({num2str(i)}, height(sub_t{i}), 1)); %#ok<AGROW>
end

permuted_simulation_inputs = vertcat(sub_t{:});
end
