function SingleSimulationPerPatient(measurement_type, unknown_proteins, filename)
% Run single simulation per patient in cohort.
% Run APOPTO-CELL simulations for a cohort. For proteins that are not known,
% we use the median computed from clinically relevant concentrations
% (Hector et al., GUT, 2012).
% 
% 'inputs.mat' must be generated before calling 
% 'run.SingleSimulationPerPatient'.
%
% Input arguments:
% - measurement_type: string, type of measurement used to quantify APOPTO-CELL
%   protein inputs. One of: 'rppa', 'tma', 'rppa_and_tma'.
% - unknown_proteins: cell array, names of the unknown proteins.
%   Must be a subset of: apaf1, caspase3, caspase9, smac, xiap.
%   Unknown proteins will be simulated with median protein values from
%   reference cohort (Hector et al., GUT, 2012);
% - filename: string, name of mat-file to save results to.

inputs = load(fullfile('outputs', 'inputs.mat'));
simulations = inputs.(measurement_type);
proteins = inputs.proteins;

medians = unstack(...
    proteins(ismember(proteins.protein, unknown_proteins),...
    {'protein', 'nanmedian_concentration'}),...
    'nanmedian_concentration', 'protein');
simulations = [simulations, repmat(medians, height(simulations), 1)];

sims = cohort_utilities.SimulateCohort(simulations, 'keep_traces', false,...
    'progressbar_label', filename);

simulations.substrate_cleavage_end = [sims.results.substrate_cleavage_end]';
simulations.substrate_cleavage_class = categorical(...
    matlab_utilities.iif(simulations.substrate_cleavage_end>25, 'SC>25%', 'SC<=25%'),...
    {'SC>25%', 'SC<=25%'});

% Save simulation results
results = simulations; %#ok<NASGU>
save(...
    fullfile('outputs', filename),...
    'results', '-v7.3');
end
