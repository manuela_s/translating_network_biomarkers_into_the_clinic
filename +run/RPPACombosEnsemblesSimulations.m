function RPPACombosEnsemblesSimulations()
% Run ensemble simulations for each combination of the 4 RPPA-based protein
% inputs to APOPTO-CELL available for the in-house stage 3 patients cohort.

if exist(fullfile('outputs', 'rppa_combos_ensembles_simulations'), 'dir') == 0
    mkdir(fullfile('outputs', 'rppa_combos_ensembles_simulations'));
end

inputs = load(fullfile('outputs', 'inputs.mat'));
steps = numel(inputs.combo_percentiles);

% Generate the full set of combinations with 5 proteins, where each protein
% can be on or off, 2^5 combinations.
proteins = {'caspase3', 'caspase9', 'xiap', 'apaf1', 'smac'};
[caspase3, caspase9, xiap, apaf1, smac] = ndgrid(...
    [true false], [true false], [true false], [true false], [true false]);
combos = array2table([caspase3(:), caspase9(:), xiap(:), apaf1(:), smac(:)],...
    'VariableNames', proteins);
combos.filename = cellfun(@(bits) sprintf('combo_%s.mat',...
    strjoin(proteins(bits), '_')), num2cell(combos{:, 1:5}, 2),...
    'UniformOutput', false);
combos.n_simulations = steps.^sum(~combos{:, 1:5}, 2);

% RPPA measurements for apaf1 are not available, thus all combinations
% requiring availability of apaf1 are excluded
combos(combos.apaf1==1, :) = [];
% Skip running simulations for combination without any known proteins
combos(~any(combos{:, 1:5}, 2), :) = [];

save(fullfile('outputs', 'rppa_combos_ensembles_simulations', 'combos.mat'),...
    'combos');

pb = matlab_utilities.ProgressBar(sum(combos.n_simulations), 'RPPACombosEnsemblesSimulations');
for i = 1:height(combos)
    bits = combos{i, 1:5};
    unknown_proteins = proteins(~bits);
    run.EnsembleSimulations(...
        'rppa',...
        unknown_proteins,...
        fullfile('rppa_combos_ensembles_simulations', combos.filename{i}),...
        'concs_column', 'combo_concs');
    pb.update(combos.n_simulations(i));
end
end
