function EnsembleSimulations(...
    measurement_type, unknown_proteins, filename, varargin)
% Run ensemble simulations for a cohort.
% Run APOPTO-CELL simulations for a cohort where a subset of the
% APOPTO-CELL proteins are known. For proteins that are not known, we run
% multiple simulations with clinically relevant concentrations and
% aggregate the results by majority vote.
% 
% 'inputs.mat' must be generated before calling 'run.EnsembleSimulations'.
%
% Input arguments:
% - measurement_type: string, type of measurement used to quantify APOPTO-CELL
%   protein inputs. One of: 'rppa', 'tma', 'rppa_and_tma'.
% - unknown_proteins: cell array, names of the unknown proteins. Must be a 
%   subset of: apaf1, caspase3, caspase9, smac, xiap.
% - filename: string, name of mat-file to save results to.
% - key/value pair:
%   - concs_column: The name of the column in the simulation_input protein table
%     that holds the protein concentrations to use for unknown proteins.

ip = inputParser();
ip.addParameter('concs_column', 'concs',...
    @(col) ismember(col, {'concs', 'combo_concs'}));
ip.parse(varargin{:});

inputs = load(fullfile('outputs', 'inputs.mat'));
cohort_inputs = inputs.(measurement_type);

simulations = cohort_inputs;
for i=1:numel(unknown_proteins)
    simulations = AddUnknownProtein(...
        simulations,...
        unknown_proteins{i},...
        inputs.proteins{unknown_proteins{i}, ip.Results.concs_column});
end

sims = cohort_utilities.SimulateCohort(simulations, 'keep_traces', false,...
    'progressbar_label', filename);

simulations.substrate_cleavage_end = [sims.results.substrate_cleavage_end]';
sim_stats = rowfun(@(x) mean(x>25), simulations,...
    'InputVariables', 'substrate_cleavage_end',...
    'GroupingVariables', 'universal_patient_id',...
    'OutputVariableNames', 'fraction_high_sc');

results = innerjoin(cohort_inputs, sim_stats);

% Set substrate_cleavage_class based on super majority vote:
results.substrate_cleavage_class = removecats(categorical(...
    matlab_utilities.iif(results.fraction_high_sc >= 0.75, 'SC>25%',...
    matlab_utilities.iif(results.fraction_high_sc <= 0.25, 'SC<=25%', 'inconclusive')),...
    {'SC>25%', 'SC<=25%', 'inconclusive'}));

% Save simulation results
save(fullfile('outputs', filename),...
    'results', '-v7.3');
end

function simulation_inputs = AddUnknownProtein(...
    simulation_inputs, protein_name, concentrations)
% Expands simulation table to include different concentrations of protein
% with unknown concentrations.
% Input arguments:
% - simulation_inputs: table, one row per simulation to run;
% - protein_name: char, the name of the protein to add/replace in
%   simulation_inputs;
% - concentrations: vector, concentrations in uM for added protein per
%   simulation to run;
% Output:
% - simulation_inputs: table, with every row from the input repeated once
%   for every concentration for the added protein.

num_patients = height(simulation_inputs);

% Replicate the data, once for every value of the unknown protein
simulation_inputs = repmat(simulation_inputs, numel(concentrations), 1);

% Reshape concentrations to ensure each patient has a simulation
% for each different concentration in concs
simulation_inputs.(protein_name) = reshape(repmat(concentrations,num_patients,1),[],1);
end
