function TreePredictions()
% Predict substrate_cleavage_class for patients included in the validation
% study with optimal trees with protein inputs determined by 'rppa', 'tma'
% and 'rppa_and_tma'.
% Save predictions to 'tree_predictions.mat'.

cli = get.Clinical();
proteins = get.AbsProteinConcentrations();

data = innerjoin(proteins, cli,...
    'key', 'universal_patient_id',...
    'RightVariables', 'stage'); 

% Convert protein concentrations from absolute units (in uM) to percentiles.
% Given that conversion from raw units to absolute concentrations is based
% on stage 2-3 patients from the reference cohort (method
% 'gut_cohort_stage23_distance_alignment' in 'cohort_utilities.ConvertRawToAbs'),
% we also compute percentiles using stage 2-3 patients only.
data = data(ismember(data.stage, {'2', '3'}), :);
data.stage = [];

% Sort to keep rowfun groups consecutive, so the order in the rowfun output
% matches the order in data:
data = sortrows(data, {'technique', 'protein'});
percentiles = rowfun(@ToPercentiles, data,...
    'InputVariables', 'concentration',...
    'GroupingVariables', {'technique', 'protein'},...
    'OutputVariableNames', 'percentiles');
data.percentiles = percentiles.percentiles;

% Subset to include only patients that meet clinical inclusion criteria and
% have protein inputs quantifications by RPPA
inputs = load(fullfile('outputs', 'inputs.mat'));
data = data(ismember(data.universal_patient_id,...
      inputs.rppa.universal_patient_id), :);

% Split protein percentiles by measurement technique:
data(:, 'concentration') = [];
data.Properties.VariableNames{'percentiles'} = 'concentration';
[rppa, tma, rppa_and_tma] = get.ProteinsByTechnique(data);

rppa = MakeTreePredictions(rppa, 'optimized_tree_building_results_4inputs.mat');
tma = MakeTreePredictions(tma, 'optimized_tree_building_results_2inputs.mat');
rppa_and_tma = MakeTreePredictions(rppa_and_tma, 'optimized_tree_building_results_4inputs.mat');

save(...
    fullfile('outputs', 'tree_predictions.mat'),...
    'rppa', 'tma', 'rppa_and_tma');
end

function percentiles = ToPercentiles(values)
% Convert a set of values to percentiles.
% Input argument:
% - values: numeric vector, values to convert to percentiles;
% Output:
% - percentiles: numeric vector with same shape as values, percentiles
%   corresponding to each value in values.

percentiles = 100 .* cdf(...
    fitdist(values, 'kernel', 'Kernel', 'normal', 'support', 'positive'),...
    values);
end

function predictions = MakeTreePredictions(tree_inputs, filename)
% Make substrate_cleavage predictions for given protein inputs.
% Input arguments:
% - tree_inputs: table, one row per patient. Columns include:
%   - 'universal_patient_id': categorical, identifier for patients;
%   - protein measurements expressed in percentiles. One protein per
%     column. Either {'caspase3', 'caspase9', 'smac', 'xiap'} or
%     {'caspase3', 'xiap'};
% - filename: string, name of mat file saved by 'decision_tree.ParameterSweep'.
%   Either 'optimized_tree_building_results_4inputs.mat' or
%   'optimized_tree_building_results_2inputs.mat';
% Output:
% - predictions: table, one row per patient. Includes tree_inputs and adds
%   the following columns:
%   - 'predicted_class': categorical, substrate_cleavage_class predicted by
%     the tree;
%   - 'predicted_confidence': double, prediction confidence for predicted_class;
%   - 'proteins_measured': cell array, names of proteins used by the tree
%     to make prediction;
%   - 'substrate_cleavage_class': categorical, predicted substrate_cleavage_class
%     for 'predicted_confidence' greater than 'confidence_threshold', otherwise
%     'inconclusive'.

confidence_threshold = 0.75;

% Convert protein names to match protein names in tree implementation:
protein_map = containers.Map(...
    {'caspase3', 'caspase9', 'smac', 'xiap'},...
    {'PC3', 'PC9', 'SMAC', 'XIAP'});
tree_inputs.Properties.VariableNames(2:end) =...
    protein_map.values(tree_inputs.Properties.VariableNames(2:end));

trees = load(fullfile('outputs', filename));

% Identify optimal tree
idx_best_tree = decision_tree.FindBestTree(...
    trees.accuracy, trees.average_proteins_measured);
tree = trees.tree{idx_best_tree};

predictions = tree.Predict(tree_inputs);

predictions.predicted_class(...
    predictions.predicted_confidence < confidence_threshold) = {'inconclusive'};
predictions.substrate_cleavage_class = removecats(categorical(...
    predictions.predicted_class, {'SC>25%', 'SC<=25%', 'inconclusive'}));
end
